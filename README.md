# Replication Package for the SCAM RENE Track paper "An Investigation into the Effect of Control and Data Dependence Paths on Predicate Testability"

## Replicating the Experiments

The testability data is gathered using IGUANA, available
[here](https://github.com/iguanatool/iguana), with the accompanying case studies
directory set up as per the ``expts`` directory of this repository. The subjects
need to be compiled, as per IGUANA instructions, on your machine.

### Input Domains

We used two input domain sizes for each subject, as described in our paper.
These need to be set as per IGUANA instructions using the configuration Java
classes in the ``expts/java`` directory.

For example, changing ``k0/inputspecifications/k0e.java`` function from
100.0 to 1000.0 and -100.0 to -1000.0 in the parameters of the `addDouble` method.

### Example Set-up

In this example we will use the root directory in Linux:

```shell
mkdir test-replication
cd test-replication
git clone https://bitbucket.org/depchaintest/replication-package.git
git clone https://github.com/iguanatool/iguana.git
cd iguana/
export IGUANA_HOME="~/test-replication/iguana"
cd ../replication-package/expts/
export IGUANA_CASESTUDIES_HOME="~/test-replication/replication-package/expts"
./setup.sh
cd ../../iguana/
mvn package
export CLASSPATH="$IGUANA_HOME/target/iguanatool-1.0-jar-with-dependencies.jar":$CLASSPATH
```

To ensure that everything works correctly run the following command in the ``iguana`` directory.

```shell
java org.iguanatool.Run k0 nhc -seed=1
```

### Running the testability experiments and the paper's raw data

We used the ``expts/scripts/expt_run.py`` Python file to run the experiments. To
replicate our work, run ``python3 ../scripts/expt_run.py`` command in the
terminal within the ``iguana/results`` directory.

The paper's and IGUANA's raw output can be found in the ``raw-results``
directory. We converted these into CSV form (data files in the ``CSV`` directory
for each result set) using the ``TSVReader`` class in the ``expts/iguana``
directory.

## Path count data

Code in ``paths/`` generates the path count data from system dependence
graphs in ``paths/SDGs``.  ``make slice`` to build the
``slice`` executable and run with ``./process_all.sh 5 100000``.

## Joining path count data with testing experiment data

We used ``join/process_all.sh`` to join the path count data in
``join/Path_Data`` with the output of the previous section in
``raw-results/{small,large}/CSV``. This script requires a JVM and [q - Text as
Data](http://harelba.github.io/q/) package.  The output is saved as CSV files in
``join/{Smaller,Larger}/{Median,Mean}`` and the starting point for the
statistical analysis is ZIP files created from those directories.


## Analysis of the joined data

Using the joined data, we followed the steps in ``analysis/analysi-steps`` to
build the analysis models and graphs found in the paper.
(While in theory this script can be run straight through, it would be better
to run each step separately in order to consider the results as well as study
the graphs produced.)
This process requires an R interpreter as well as several standard unix tools.

