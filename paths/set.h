/* set.h */

/* copyright (c) The Authors 2002 */

#ifndef _SETS
#define _SETS

typedef struct sSet *tSet;

typedef void (*FN1ARG)(void *);
typedef void (*FN2ARG)(void *, void *);
typedef void (*FN3ARG)(void *, void *, void *);
typedef void (*FN4ARG)(void *, void *, void *, void *);


/* exported set functions */

tSet set_initialize(int (*compare_function)(void *, void*),
    	    int (*hash_function)(void *));
void    set_free(tSet);
int     set_member(tSet, void*);
tSet    set_insert(tSet, void*);
tSet    set_intersection(tSet, tSet);
tSet    set_union(tSet, tSet);
tSet    set_collect(tSet, int (*f)(void *) );
void    set_print(tSet, void (*print_function)());
int     set_size(tSet);
void    set_foreach(tSet, void (*f)(void *));
void    set_foreach1(tSet, void (*f)(void*, void*), void*);
void    set_foreach2(tSet, void (*f)(void *, void *, void *), void *, void *);
void    set_foreach3(tSet, void (*f)(void *, void *,void *, void *), void*, void*, void*);
tSet    set_copy (tSet);
int     set_isempty(tSet);
void    set_remove(tSet, void*);
void*   set_any_member(tSet);

/* other possible functions include 
tSet    set_copy_replicating_elements(tSet, void *(*f)());
void    set_foreach4(tSet, void (*f)(), void*, void*, void*, void*);

void*   set_remove_any(tSet);
int     set_equal(tSet, tSet);
tSet    set_difference(tSet, tSet);
void    set_remove_all_matching(tSet, void*);


*/

#endif
