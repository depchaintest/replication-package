#ifndef __PATH_STATS_H__
#define __PATH_STATS_H__

#include <iostream>
#include <vector>

namespace std
{
  template < class T >
    class PathStatistics
    {
    public:
      PathStatistics();
      PathStatistics(int length, bool estimate);
      virtual ~PathStatistics() {}

      /**
       * Returns the statistics for a set of paths and the given new path.
       */
      PathStatistics< T > operator +(const vector< T >& path) const;

      /**
       * Returns the statistics for the union of two separate sets of
       * paths that end in the same place.
       */
      PathStatistics< T > operator +(const PathStatistics< T >& stats) const;

      /**
       * Returns the statistics for the concatenation of two sets of paths
       */
      PathStatistics< T > operator *(const PathStatistics< T >& stats) const;

      /**
       * Determines if these statistics are all the same as the given ones.
       */
      bool operator ==(const PathStatistics< T >& other) const;

      int getMinimum() const;
      int getMaximum() const;
      long double getCount() const;
      double getAverage() const;
      long double getTotalLength() const;
      bool isEstimated() const;
      static PathStatistics< T > infinity();
      static PathStatistics< T > one();
      static PathStatistics< T > empty();
      static PathStatistics< T > maxForGraph(int n);
      static PathStatistics< T > estimate(int mn, int mx, long double cnt, long double totLen);

      template < class U >
	static PathStatistics< T > copy(const PathStatistics< U >& other);

    private:
      PathStatistics(int, int, long double, long double, bool = false);
      int min;
      int max;
      long double count;
      long double totalLength;
      bool isEstimate;
    };

  template < class T >
    ostream& operator<<(ostream& os, const PathStatistics< T >& stats);
};
#endif
