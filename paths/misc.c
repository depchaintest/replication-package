// misc.c

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "pdg.h"

extern void exit(int);

vertex_kind string_to_kind(char *s)
{
    if (strcmp(s, "entry") == 0) return (ENTRY); 
    if (strcmp(s, "body") == 0) return (BODY); 
    if (strcmp(s, "formal-in") == 0) return (FORMAL_IN); 
    if (strcmp(s, "formal-out") == 0) return (FORMAL_OUT); 
    if (strcmp(s, "global-formal-in") == 0) return (GLOBAL_FORMAL_IN); 
    if (strcmp(s, "global-formal-out") == 0) return (GLOBAL_FORMAL_OUT); 
    if (strcmp(s, "call-site") == 0) return (CALL_SITE); 
    if (strcmp(s, "indirect-call") == 0) return (INDIRECT_CALL); 
    if (strcmp(s, "actual-in") == 0) return (ACTUAL_IN); 
    if (strcmp(s, "actual-out") == 0) return (ACTUAL_OUT); 
    if (strcmp(s, "global-actual-in") == 0) return (GLOBAL_ACTUAL_IN); 
    if (strcmp(s, "global-actual-out") == 0) return (GLOBAL_ACTUAL_OUT); 
    if (strcmp(s, "declaration") == 0) return (DECLARATION); 
    if (strcmp(s,"variable-initialization")==0)return(VARIABLE_INITIALIZATION);
    if (strcmp(s, "auxiliary") == 0) return (AUXILIARY); 
    if (strcmp(s, "ERROR-KIND") == 0) return (ERROR_KIND);
    //if (strcmp(s, "executable-code") == 0) return (EXECUTABLE_CODE); 
    if (strcmp(s, "control-point") == 0) return (CONTROL_POINT); 
    if (strcmp(s, "control-target") == 0) return (CONTROL_TARGET); 
    if (strcmp(s, "expression") == 0) return (EXPRESSION); 
    if (strcmp(s, "return") == 0) return (RETURN_KIND); 
    if (strcmp(s, "other") == 0) return (OTHER);
    if (strcmp(s, "exit") == 0) return (OTHER); // FIX ME
    if (strcmp(s, "switch-case") == 0) return (OTHER); // FIX ME
    if (strcmp(s, "jump") == 0) return (OTHER); // FIX ME
    if (strcmp(s, "label") == 0) return (OTHER); // FIX ME
    printf("string --%s-- has no kind :(\n", s);
    return (ERROR_KIND);
}

char *kind_to_string(vertex_kind k)
{
    static char buff[200];
    buff[0] = '\0';
    if (k & ENTRY)                   strcat(buff, "entry "); 
    if (k & BODY)                    strcat(buff, "body "); 
    if (k & FORMAL_IN)               strcat(buff, "formal-in "); 
    if (k & FORMAL_OUT)              strcat(buff, "formal-out "); 
    if (k & GLOBAL_FORMAL_IN)        strcat(buff, "global-formal-in "); 
    if (k & GLOBAL_FORMAL_OUT)       strcat(buff, "global-formal-out "); 
    if (k & CALL_SITE)               strcat(buff, "call-site "); 
    if (k & INDIRECT_CALL)           strcat(buff, "indirect-call "); 
    if (k & ACTUAL_IN)               strcat(buff, "actual-in "); 
    if (k & ACTUAL_OUT)              strcat(buff, "actual-out "); 
    if (k & GLOBAL_ACTUAL_IN)        strcat(buff, "global-actual-in "); 
    if (k & GLOBAL_ACTUAL_OUT)       strcat(buff, "global-actual-out "); 
    if (k & DECLARATION)             strcat(buff, "declaration "); 
    //if (k & case EXECUTABLE_CODE)  strcat(buff, "executable-code "); 
    if (k & CONTROL_POINT)           strcat(buff, "control-point "); 
    if (k & CONTROL_TARGET)          strcat(buff, "control-target "); 
    if (k & EXPRESSION)              strcat(buff, "expression "); 
    if (k & RETURN_KIND)             strcat(buff, "strcat(buff, "); 
    if (k & VARIABLE_INITIALIZATION) strcat(buff, "variable-initialization "); 
    if (k & OTHER)                   strcat(buff, "other "); 
    if (k & AUXILIARY)               strcat(buff, "auxiliary "); 
    if (k & UNKNOWN_VERTEX_KIND)     strcat(buff, "unknown ");
    if (k & IF)                      strcat(buff, "if ");
    if (k & IF)                      strcat(buff, "while ");

    if (*buff)
        return buff;
    else
        return "ERROR_KIND";
}



void read_fails(char *format, int cnt)
{
    char buf[200];
    int l;

    printf(format, cnt);
    l = fread(buf, 1, 200, stdin);
    buf[l] = '\0';
    printf("next %d bytes of input %s\n", l, buf);
    exit(-1);
}

int __isoc99_fscanf(FILE *stream, const char *format, ...)
{
  va_list argp;
  va_start(argp, format);

  int read =  vfscanf(stream , format, argp);

  va_end(argp);

  return read;
}
