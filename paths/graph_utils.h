#ifndef __GRAPH_UTILS_H__
#define __GRAPH_UTILS_H__

#include <tr1/unordered_set>
#include <tr1/unordered_map>

#include "path_stats.h"
#include "graph.h"

namespace std
{

template< class Key >
void addIfSCCEntry(const Key& v, tr1::unordered_set< Key >& s, const DirectedGraph< Key >& g, const tr1::unordered_map< Key, int >& sccs);

template< class Key >
void addIfSCCExit(const Key& v, tr1::unordered_set< Key >& s, const DirectedGraph< Key >& g, const tr1::unordered_map< Key, int >& sccs);

struct pairhash;

template < class Key >
  tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash > countPaths(const DirectedGraph< Key >& g, const tr1::unordered_set< Key >& starts, const tr1::unordered_set< Key >& ends, int maxDepth, int& deepest);

template < class Key >
class DifferentSCC : public Predicate< Key >
{
 public:
  DifferentSCC(const Key& v, const tr1::unordered_map< Key, int >& s);
  virtual ~DifferentSCC();

  bool operator()(const Key& u) const;

 private:
  int component;
  const tr1::unordered_map< Key, int >& sccs;
};
}
#endif
