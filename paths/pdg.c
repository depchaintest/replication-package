/* pdg.c */

#define DEBUG 0

#include "ht.h"
#include "pdg.h"
#include "misc.h"
#include "malloc.h"
#include "stdio.h"
#include "string.h"

void pdg_vertex_clear_mark(tPdgVertex v)
{
    v->mark = 0;
}

tPdgVertex pdg_vertex_lookup(int name, tHashTable sdg)
{
    struct sPdgVertex look_for;
    tPdgVertex v;

    look_for.name = name; 
    v = ht_lookup(sdg, &look_for);
    if (v == NULL) 
        printf("FIX ME look up failed for %x\n", name);
    return v;
}

void pdg_vertex_print_short(tPdgVertex v)
{
    if (v == NULL) 
       printf("printing NULL VERTEX");
    else
        printf(" %x \"%s\" ", v->name,  v->src);
        //printf(" name %x %s %s\n", v->name, kind_to_string(pdg_vertex_kind(v)), v->src);
}

static void print_name(tPdgVertex *v)
{
    int data = data_edge(*v);
    tPdgVertex x = strip_edge_kind(*v);
    printf("%x (%s) ", x->name, data?"data":"control");
}

void pdg_vertex_print(tPdgVertex v)
{
    tPdgVertex *edges = v->incoming_intra_edges;
    pdg_vertex_print_short(v);
    printf("\n  I (%d): ", v->incoming_intra_edge_count);
    if (edges != NULL)
        while (*edges != NULL)
            print_name(edges++);

    printf("\n  O: ");
    edges = v->outgoing_intra_edges;
    if (edges != NULL)
        while (*edges != NULL)
            print_name(edges++);

    printf("\n  II: ");
    edges = v->incoming_inter_edges;
    if (edges != NULL)
        while (*edges != NULL)
            print_name(edges++);

    printf("\n  IO: ");
    edges = v->outgoing_inter_edges;
    if (edges != NULL)
        while (*edges != NULL)
            print_name(edges++);
    printf("\n");
}

#define MAX  100000     // for sendmamil! #define MAX  35000
static int read_edge_list(tPdgVertex **edges)
{
    char kind_string[100];
    int name;
    int count=0;
    long tmp_edges[MAX];

    int tmp = scanf("%s \"[%*s %x]\",", kind_string, &name);
    while(tmp == 2)
    {
      // printf("READ_EDGE_LIST: %x\n", (int)name);
        int data_edge = strcmp(kind_string, "data") == 0;
//printf("%d '%s' ", data_edge, kind_string);
        if (count >= MAX) printf("OOPS increase MAX to %d\n", count++);
        else tmp_edges[count++] = name | data_edge;
        tmp = scanf("%s \"[%*s %x]\",", kind_string, &name);
//printf("** %x** \n", tmp_edges[count-1]);

    }

    if (count != 0)
      {
        *edges = malloc((count+2)*sizeof(tPdgVertex));
	(*edges[0]) = (tPdgVertex)(tmp_edges[0]); // so we get a warning if the sizes aren't the same
        memcpy(*edges, tmp_edges, (count+1)*sizeof(tPdgVertex));
        (*edges)[count] = 0;

	/*
	printf("READ_EDGE_LIST temp vs edges\n");
	int i;
	for (i = 0; i < count; i++)
	  {
	    printf("%x %x\n", (int)(tmp_edges[i]), (int)((*edges)[i]));
	  }
	*/
    }
    else 
        *edges = 0;
    return count;
}


tPdgVertex pdg_vertex_read(tHashTable sdg)
{
    int name;
    char kind_string[100];
    char filename[1024];
    int number_of_charpos;
    int source_length;
    int i, cnt ;
    tPdgVertex vertex = (tPdgVertex) check_malloc(PDG_VERTEX_SIZE);

    /* FIX ME limit length of input strings */
    cnt = scanf("%s %*s %x%*c %s %d", filename, &name, 
                                      kind_string, &source_length);
    if (cnt != 4)
    {
        if (cnt <= 0) 
            return (NULL);
        else
            read_fails("scanf 1 fails cnt = %d\n", cnt);
    }

    // JRG: added saving filename
    int filename_length = strlen(filename);
    int start = filename_length - 1;
    while (start >= 0 && filename[start] != '/' && filename[start] != '\\')
      {
	start--;
      }
    start++;
    vertex->file = (char *)check_malloc(filename_length - start + 1);
    strcpy(vertex->file, filename + start);

    vertex->name = name;
    vertex->kind = string_to_kind(kind_string);
    vertex->src = NULL;

    getchar(); /* scan off ':' */
    {
        char b[source_length+1+10 ];
        if ((source_length != 0) &&(fread(b, 1, source_length, stdin) == 0))
            read_fails("fread error", 0);
            b[source_length] = '\0';
    vertex->src = strdup(b);
	// FIX ME debug keep all src
if ((strncmp(b, "if ", 3) == 0) || (strncmp(b, "if(", 3) == 0)) 
{
//printf("kind %s src %s\n", kind_string, b);
    //vertex->src = strdup(b);
    vertex->kind = IF | CONTROL_POINT;
}
    if (strncmp(b, "while ", 5) == 0) 
        vertex->kind = WHILE | CONTROL_POINT;

    }

    cnt = scanf("%d", &number_of_charpos);
    if (cnt != 1)
    {
        read_fails("scanf 2 fails cnt = %d\n", cnt);
    }

    vertex->charpos_min = 0;
    vertex->charpos_max = 0;  

    if (number_of_charpos > 0)
    {
        unsigned int start = 0x7fffffff; // MAX INT
        unsigned int stop  = 0;
        for(i=0; i<number_of_charpos; i++)
        {
            int a, b;
            cnt = scanf("%d %d", &a, &b);
            //printf("a %d b %d start %d stop %d\n", a, b, start, stop);
            if (cnt != 2)
                printf("charpos read fails!\n");
            if (a < start)
                start = a;
            if (a+b > stop)
                stop = a+b;
        }
        //printf("start %d stop %d\n", start, stop);
        vertex->charpos_min = start;
        vertex->charpos_max = stop;  
    }

    pdg_vertex_clear_mark(vertex);

    if (strstr(filename, "libc") != NULL)
        vertex->kind |= FROM_LIBC;
    if (number_of_charpos == 0)
        vertex->kind |= NO_SOURCE;

    scanf("%*s");  /* ignore I: */
    vertex->last_average = 0;
    // printf("READ_EDGE_LIST for %x\n", (int)(vertex->name));
    vertex->incoming_intra_edge_count = read_edge_list(&vertex->incoming_intra_edges);
    read_edge_list(&vertex->outgoing_intra_edges);
    read_edge_list(&vertex->incoming_inter_edges);
    read_edge_list(&vertex->outgoing_inter_edges);

    for(i=0; i<6; i++)
        vertex->counts[i] = -1;
    vertex->on_path = 0;
    return vertex;
}

int pdg_vertex_hash(tPdgVertex v)
{
    return(v->name >> 6);
}

// as name are globally unique, files names need not be compared
int pdg_vertex_compare(tPdgVertex v1, tPdgVertex v2)
{
    return (v1->name & ~0x01) == (v2->name & ~0x01);
}


void pdg_vertex_print_short_if(void *x, int (*f)(tPdgVertex v) )
{
    tPdgVertex v = (tPdgVertex) x;
    if (f(x))
    {
        pdg_vertex_print_short(v);
    }
}

void pdg_vertex_print_if(void *x, int (*f)(tPdgVertex v) )
{
    tPdgVertex v = (tPdgVertex) x;
    if (f(x))
    {
        pdg_vertex_print(v);
    }
}
