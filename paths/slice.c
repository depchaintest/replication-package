/* predicate pair compare.c - look for nested predicates that can be combined */


#define OVERLAP_IS_FIN_N_GIN     FALSE, (FORMAL_IN|GLOBAL_FORMAL_IN)
#define OVERLAP_IS_GLOBALS       FALSE, GLOBAL_FORMAL_IN
#define OVERLAP_IS_FORMALS       FALSE, FORMAL_IN
#define OVERLAP_IS_INTRA_SLICE   FALSE, FALSE
#define OVERLAP_IS_INTER_SLICE   TRUE, FALSE
// TRUE, TRUE would count interproceduraly reachable formal parameters


// true if loop-carried and while-while are counted separately
#define SKIPLC 0
#define SKIPCALLWHILE 0

#define DEBUG 0

#include "path.h"
#include "pdg.h"
#include "sdg.h"
#include "stdio.h"
#include "ht.h"
#include "misc.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/times.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>

#include "pd_graph.h"

extern int current_mark;
static int slice_size = 0;

int min_scc_to_process;
int max_scc_to_process;
int scc_processing_timeout;
int scc_break_depth;
int estimation_trials;

void b_slice(tPdgVertex from, long *area, int do_interprocedural, int count_formals_only);
void f_control_slice(tPdgVertex from);

static int bl_slice(tPdgVertex v);

static int _includes_loop_carried = 0;


typedef struct 
{
    int intraprocedurally;
    int interprocedurally;
} transitively_control;

static int loop_carried(tPdgVertex a, tPdgVertex b)
{
    return a->charpos_max < b->charpos_min && a->charpos_max != 0;
}


// rather expensive ... should really add it to the vertex and fillin once
static int same_procedure(tPdgVertex P, tPdgVertex Q)
{
    current_mark++;
    bl_slice(P);
    current_mark++;
    return bl_slice(Q) != 0;
}

static int bl_slice(tPdgVertex v)
{
    int  in_last_slice = 0;
    if (v->mark != current_mark)
    {
        tPdgVertex *edges = v->incoming_intra_edges;
        if (v->mark == current_mark - 1)
            in_last_slice++;

        v->mark = current_mark;

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL) 
            {
                bl_slice(strip_edge_kind(*edges++));
            }
    }

    return in_last_slice;
}


// A is in the forward control slice on v
static int in_fl_control_slice(tPdgVertex A, tPdgVertex v)
{
    // printf("control slice A v %x %x\n", A->name, v->name);
    if (A->name == v->name)
        return TRUE;

    if (v->mark != current_mark)
    {
        tPdgVertex *edges = v->outgoing_intra_edges;
        v->mark = current_mark;

        if (edges != NULL)   
        {
            while (strip_edge_kind(*edges) != NULL)
            {
                if (control_edge(*edges))
                {
                    if (in_fl_control_slice(A, strip_edge_kind(*edges)))
                        return TRUE;
                }
                edges++;
            }
        }
    }

    return FALSE;
}

static int in_data_slice(tPdgVertex A, tPdgVertex v)
{
    if (A->name == v->name)
        return TRUE;

    if (v->mark != current_mark)
    {
        tPdgVertex *edges = v->incoming_intra_edges;
        v->mark = current_mark;

        if (edges != NULL)   // FIX ME data-edges only
        {
            while (strip_edge_kind(*edges) != NULL)
            {
                if (data_edge(*edges))
                {
                    if (loop_carried(v, strip_edge_kind(*edges)))
                        _includes_loop_carried = 1;

                    if (!loop_carried(v, strip_edge_kind(*edges)))
                        if (in_data_slice(A, strip_edge_kind(*edges)))
                            return TRUE;
                }
                edges++;
            }
        }
    }

    return FALSE;
}



static void dump_slice(tPdgVertex v)
{
    if (v->mark == current_mark)
        pdg_vertex_print_short(v);
}

static void transitively_controls(tPdgVertex Q, tPdgVertex P, transitively_control *tc)
{
    //int x;
    current_mark++;

    // intraprocedural version
    tc-> intraprocedurally = in_fl_control_slice(P, Q);
    //printf(" %d ", x);

    // interprocedural version
    f_control_slice(Q);
    tc-> interprocedurally = (current_mark == P->mark);  // P in Q's slice
    //printf(" %d ", x);

    //if (x) printf("BING\n");
    //return x;
}

static int directly_controls(tPdgVertex Q, tPdgVertex P)
{
    tPdgVertex *edges = Q->outgoing_intra_edges;
    if (edges != NULL) 
        for (;strip_edge_kind(*edges) != NULL; edges++)    // Q has only outgoing control edges so nee need to check
            if ((strip_edge_kind(*edges))->name == P->name)
                return TRUE;

    return FALSE;
}




static void find_second_if(tPdgVertex Q, tPdgVertex P, tHashTable sdg, int graph_size)
{
    if ((pdg_vertex_flags(Q) & FROM_LIBC) || (pdg_vertex_flags(Q) & NO_SOURCE))
        return;
             
    if (!control_point(Q->kind)) // was is_if(...
        return;

    if (Q->name == P->name)
        return;

    if (Q->name > P->name)   // process each pair only once
        return;

    transitively_control tc;
    transitively_controls(P, Q, &tc); 
    printf("%d %d ", tc.intraprocedurally, tc.interprocedurally);
    transitively_controls(Q, P, &tc); 
    int sp = same_procedure(P, Q);
    printf("%d %d %d ", tc.intraprocedurally, tc.interprocedurally, sp);

// do formals, globals, both, intra slice, inter slice

    long junk = 0;
    long intersection_size = 0;
    b_slice(P, &junk, OVERLAP_IS_FORMALS);
    b_slice(Q, &intersection_size, OVERLAP_IS_FORMALS);
    printf(" %ld ", intersection_size);

    intersection_size = 0;
    b_slice(P, &junk, OVERLAP_IS_GLOBALS);
    b_slice(Q, &intersection_size, OVERLAP_IS_GLOBALS);
    printf(" %ld ", intersection_size);

    intersection_size = 0;
    b_slice(P, &junk, OVERLAP_IS_FIN_N_GIN);
    b_slice(Q, &intersection_size, OVERLAP_IS_FIN_N_GIN);
    printf(" %ld ", intersection_size);

    intersection_size = 0;
    b_slice(P, &junk, OVERLAP_IS_INTRA_SLICE);
    b_slice(Q, &intersection_size, OVERLAP_IS_INTRA_SLICE);
    printf(" %ld ", intersection_size);

    intersection_size = 0;
    b_slice(P, &junk, OVERLAP_IS_INTER_SLICE);
    b_slice(Q, &intersection_size, OVERLAP_IS_INTER_SLICE);
    printf(" %ld %5.2f ", intersection_size, 100.0*((double)intersection_size/(double)graph_size));

    pdg_vertex_print_short(P);
    pdg_vertex_print_short(Q);
    printf("\n");
}

static void find_first_if(tPdgVertex from, tHashTable sdg, int graph_size)
{
    //tPdgVertex *edges;
    // int Q_controls_an_a;
    
    tPdgVertex P;
    // ignore non-source vertices
    if ((pdg_vertex_flags(from) & FROM_LIBC) || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    if (DEBUG) pdg_vertex_print(from);

    // Node P [directly] controls Q and Q does not [transitively] control P.
    // (1) P ->c Q
    // (2) not (Q ->c* P)    [ implies P != Q ... ]
    P = from;

    if (!control_point(P->kind)) // was is_if(...
        return;

    ht_foreach3(sdg, (FN4ARG)&find_second_if, P, sdg, (void *)graph_size); 
    return;
}

static void find_path_start(tPdgVertex from, tHashTable sdg, int graph_size)
{
    //tPdgVertex *edges;
    // int Q_controls_an_a;
    
    tPdgVertex P;
    // ignore non-source vertices
    if ((pdg_vertex_flags(from) & FROM_LIBC) || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    if (DEBUG) pdg_vertex_print(from);

    // Node P [directly] controls Q and Q does not [transitively] control P.
    // (1) P ->c Q
    // (2) not (Q ->c* P)    [ implies P != Q ... ]
    P = from;

    if (!control_point(P->kind)) // was is_if(...
        return;

    process_paths(from, sdg, graph_size);
    return;
}


static void print_times(char *title, clock_t start, struct tms tms_start,
                        clock_t stop, struct tms tms_stop)
{
    long x = tms_stop.tms_utime - tms_start.tms_utime;
    long sum = x;

    printf("%s", title);
    printf("times %2.2ld.%2.2ldu ", x/100, x%100);

    x = tms_stop.tms_stime - tms_start.tms_stime;
    sum += x;
    printf("%2.2ld.%2.2lds ", x/100, x%100);

    x = stop - start;
    if (x != 0)
        printf("%2.2ld:%2.2ld.%2.2ld %ld%%\n", x/6000, (x/100)%60, x%100, sum*100/x);
    else
        printf("%2.2ld:%2.2ld.%2.2ld x=0\n", x/6000, (x/100)%60, x%100);
}

static void fill_in_my_entry(tPdgVertex v, tPdgVertex my_entry)
{
    if (v->my_entry == NULL)
    {
        v->my_entry = my_entry;

        tPdgVertex *edges = v->outgoing_intra_edges;

        if (edges != NULL)
        {
            while (strip_edge_kind(*edges) != NULL)
            {
                if (control_edge(*edges))
                    fill_in_my_entry(strip_edge_kind(*edges), my_entry);
                edges++;
            }
        }
    }
}



static void init(tPdgVertex v)
{
    pdg_vertex_clear_mark(v);
    if (!((pdg_vertex_flags(v) & FROM_LIBC) || (pdg_vertex_flags(v) & NO_SOURCE)))
        slice_size++;
    if (entry(v->kind))
        fill_in_my_entry(v, v);
}

static void lint()
{
  lint(); directly_controls(NULL, NULL); dump_slice(NULL);
}

static void print_pair_heading()
{
    printf("# for predicate pair <P,Q>\n");
    printf("# control relations (columns 1-5)                                      overlap measured using (columns 6-11)\n");
    printf("# P-tc-Q-intra P-tc-Q-inter Q-tc-P-intra Q-tc-P-inter same-procedure   formals globals both  intra-slice inter-slice inter-slice-as-percent   P Q\n");
}

int main(int argc, char *argv[])
{
    clock_t start, end_init, stop;
    struct tms tms_start, tms_end_init, tms_stop;
    tHashTable sdg = NULL; 
    int graph_size = 0;
    printf("*** consider charpos subsumption ***\n");

    srandom(getpid());

    if (argc < 2)
    {
        printf("usage %s [-path|-pair]\n", argv[0]);
        exit(0);
    }

    start = times(&tms_start);
    sdg = sdg_read(); 
    if (!feof(stdin))
        read_fails("ARGH read_sdg returned before EOF!\n", 0);

    // DEBUG
    ht_foreach(sdg, (FN1ARG)pdg_vertex_print);

    slice_size = 0;
    ht_foreach(sdg, (FN1ARG)&init); 
    graph_size = slice_size;

    // ht_foreach(sdg, (FN1ARG)&pdg_vertex_print); 
    end_init = times(&tms_end_init);
    if (strcmp(argv[1], "-path") == 0)
    {
        printf("# path length for control-only, data-only both-edge types [loop sourced edges count]\n");
        printf("# average given <count, length-sum> average\n");

	// get bounds on sizes of SCCs to process -- if a vertex induces
	// a graph with an SCC outside this range then it will not be
	// fully processed
	
	min_scc_to_process = 0;
	max_scc_to_process = INT_MAX;

	scc_processing_timeout = 10000000; // default max time to 1e+7 s = 116d
	scc_break_depth = -1; // default to no limit
	estimation_trials = 100000; // 100K trials when estimating

	if (argc >= 4 && strcmp(argv[2], "-min_scc") == 0)
	  {
	    min_scc_to_process = atoi(argv[3]);
	  }
	if (argc >= 4 && strcmp(argv[2], "-scc_timeout") == 0)
	  {
	    scc_processing_timeout = atoi(argv[3]);
	  }
	if (argc >= 4 && strcmp(argv[2], "-scc_break_depth") == 0)
	  {
	    scc_break_depth = atoi(argv[3]);
	  }
	if (argc >= 4 && strcmp(argv[4], "-estimation_trials") == 0)
	  {
	    estimation_trials = atoi(argv[5]);
	  }
	if (argc >= 6 && strcmp(argv[4], "-max_scc") == 0)
	  {
	    max_scc_to_process = atoi(argv[5]);
	  }

	printf("Building nice C++ graph\n");
	process_pd_graph(sdg);
	printf("Done building C++ graph\n");
        // ht_foreach2(sdg, (FN3ARG)&find_path_start, sdg, (void *)graph_size); 
    }
    else if (strcmp(argv[1], "-pair") == 0)
    {
        print_pair_heading();
        ht_foreach2(sdg, (FN3ARG)&find_first_if, sdg, (void *)graph_size); 
    }
    else
        printf("expected -path or -pair option\n");

    stop = times(&tms_stop);

    printf("\n");

    printf("() %d\n", random());

    exit(0);
    printf("\n");
    print_times("@@ Read     ", start, tms_start, end_init, tms_end_init);
    print_times("@@ process  ", end_init, tms_end_init, stop, tms_stop);
    print_times("@@ Total    ", start, tms_start, stop, tms_stop);
    printf("graphs contains %d vertices\n", graph_size);
    printf("\n");
    return 0;
}
