// paths.c - process the set of parts to a vertex producing the min, max, and
// ave length.   for control only, data, only, and all dependence edges.  
// report also the number of look sources edges (those that would have a back
// edge in the cfg

#include "sccs.h"
#include "paths.h"
#include "pdg.h"
#include "sdg.h"
#include "stdio.h"
#include "list.h"
#include "misc.h"

#include <stdlib.h>
//#include <time.h>
//#include <sys/times.h>


typedef struct sPair
{
    int x, y;
} tPair;


void reset_counts(tPdgVertex entry, int values[9]);
void print_bests(tPdgVertex v);
void print_best_averages(tPdgVertex v);

static void path_print(tPdgVertex *p);
static int control_self(tPdgVertex v);


#define ave(a,b)  (((double)(a))/((double)(b)))

#define CO 0   // control only
#define DO 1   // data only
#define BOTH 2


static int occurs_check(tPdgVertex *path, tPdgVertex v)
{
    tPdgVertex *t;
    for(t = path; *t != NULL; t++)    // cycle check
    {
        if (*t == v)
            return TRUE;
    }
    return FALSE;
}


// used for occurs (cycle) check during dfs (not needed by min)
static tPdgVertex current_path[2048];
static int current_path_head = 0;

static long long dwb = 1;
static int dwb_bail_message = FALSE;
static int loop_head_count = 0;

// edge_type is BOTH, CO, DO
// return 1 if the random sample is in order
// return 0 if exchustive completed serach before reaching limit
static int walk_ave(tPdgVertex from, int edge_type)
{
    int ret = 0;
    //printf("\n average walker to "); pdg_vertex_print_short(from);
    //printf("  both <%d,%d>   ", from->counts[edge_type], from->counts[edge_type+3]);
    //printf("  current_path "); path_print(current_path);

    //if (dwb >= 100000000LL) 
    if (dwb >= 10000) 
    {
	    if (!dwb_bail_message)
		{
            //fprintf(stderr, "reached (iteration count %lld) BAILING! \n", dwb); 
            printf("reached (iteration count %lld) BAILING! on", dwb); pdg_vertex_print_short(from);
			printf("\n");
		}
	    dwb_bail_message = TRUE;
        return 1;
	}
    //if (dwb >= 100000) return;

    if (formal_in(from->kind) || entry(from->kind))
    {
        from->counts[edge_type]++; // found a path
        // add on path's length (including "+1" includes the current 
        // vertex thus counting vertices others edges are counted.)
        from->counts[edge_type+3] += current_path_head;  
        from->counts[edge_type+6] += loop_head_count;
        //printf("path length %d   loop-head-count %d\n", current_path_head, loop_head_count);
        // printf("current_path_head %d ", current_path_head);
        // path_print(current_path);
        if ((dwb++ % 1000000000) == 0)
        {
            printf("reached (iteration count %lld) ", dwb-1); pdg_vertex_print_short(from);
            printf(" both <%5.0lf,%5.0lf> %5.2f\n", from->counts[edge_type], from->counts[edge_type+3]
                   ,((double)from->counts[edge_type+3])/((double)(from->counts[edge_type])));
        }
    }
    else
    {
        current_path[current_path_head++] = from;
        current_path[current_path_head] = NULL;
        if (current_path_head > 2000) fprintf(stderr, "cycle check needs more space!\n");
        from->on_path = TRUE;

        tPdgVertex *edges = from->incoming_intra_edges;
        if (edges != NULL) 
        {
            for(; strip_edge_kind(*edges) != NULL; edges++)
            {
                if (edge_type == BOTH ||
                    (edge_type == DO && data_edge(*edges)) ||
                    (edge_type == CO && control_edge(*edges)))
                {
                    tPdgVertex v = strip_edge_kind(*edges);

                    if (v->on_path)     // old - if (occurs_check(current_path, v))
                        continue;

                    int loop_head = control_self(v);
                    loop_head_count += loop_head;

                    ret = walk_ave(v, edge_type);
                    //printf("back from walking %x at %x\n", v->name, from->name);
                    loop_head_count -= loop_head;
    
                    // if (control_self(v)) p->loop_sourced_edges++; FIX ME 
                }
            }
        }
        current_path[--current_path_head] = NULL;  
        from->on_path = FALSE;
    }

    return ret;
}

static int consecutive_small_count = 0;
static int last_small = FALSE;

// edge_type is BOTH, CO, DO
// JRG: currently ignoring edge_type
// JRG: all work is done in functions in sccs.c
static void walk_sampled_ave(tPdgVertex from, int edge_type)
{
  printf("Creating SCC graph\n");
  tSccGraph g = create_scc_graph(from, selector_no_decls);
  double average_path_length = timed_estimate_average_path_length(g, selector_no_decls, scc_processing_timeout);
  // double average_path_length = estimate_average_path_length(g, selector_no_decls);
printf("AVERAGE-PATH-LENGTH BO ENTRY %x %6.3lf @@@ %s\n", from->name, average_path_length, from->src);
  destroy_scc_graph(g);
}

// \bot + 1 = \bot else increment
#define inc(x) ( (x == -1) ? x : x+1 )

static int min_meet(a,b)
{
    if (a == -1)    // \bot \cap n = n
        return b;
    if (b == -1)    // n \cap \bot = n
        return a;

    return a < b ? a : b;
}


static int max_meet(a,b)
{
    if (a == -1)    // \bot \cap n = n
        return b;
    if (b == -1)    // n \cap \bot = n
        return a;

    return a > b ? a : b;
}



static void walk(tPdgVertex from, int (*meet)(int a, int b))
{
    // printf("\nwalking to "); pdg_vertex_print_short(from);
    // printf("  current_path ");  path_print(current_path);

    if (formal_in(from->kind) || entry(from->kind))
    {
        //printf("reached "); pdg_vertex_print_short(from);
        //printf("CO %2d DO %2d both %2d \n", from->counts[CO], from->counts[DO], from->counts[BOTH]);
    }
    else
    {
        current_path[current_path_head++] = from;
        current_path[current_path_head] = NULL;
        if (current_path_head > 2000) fprintf(stderr, "cycle check needs more space!\n");

        tPdgVertex *edges = from->incoming_intra_edges;
        if (edges != NULL) 
        {
            for(; strip_edge_kind(*edges) != NULL; edges++)
            {
                tPdgVertex v = strip_edge_kind(*edges);
                if (occurs_check(current_path, v))
                    continue;
                if (declaration(v->kind))
                    continue;

                int loop_head = control_self(v);
                //if (loop_head) printf("for %x  %x controls itself\n", from->name, v->name);

                int change = FALSE;
                int m = (*meet)(v->counts[BOTH], inc(from->counts[BOTH]));
                if (v->counts[BOTH] != m)
                {
                    change = TRUE;
                    v->counts[BOTH] = m;
                    v->counts[BOTH+3] = from->counts[BOTH+3] + loop_head;
                }

                if (control_edge(*edges))
                {
                    m = (*meet)(v->counts[CO], inc(from->counts[CO]));
                    if (v->counts[CO] != m)
                    {
                        change = TRUE;
                        v->counts[CO] = m;
                        v->counts[CO+3] = from->counts[CO+3] + loop_head;
                    }
                }
                else  // data_edge(*edges)
                {
                    m = (*meet)(v->counts[DO], inc(from->counts[DO]));
                    if (v->counts[DO] != m)
                    {
                        change = TRUE;
                        v->counts[DO] = m;
                        v->counts[DO+3] = from->counts[DO+3] + loop_head;
                    }
                }

                if (change)
                    walk(v, meet);
            }
        }
        current_path[--current_path_head] = NULL;  // NULL assignment a bonus :) FIX ME CHECK
    }
}


 void process_paths(tPdgVertex from, tHashTable sdg, int graph_size)
{
    int minmax[9] = {-1, -1, -1, 0, 0, 0, 0, 0, 0}; // = [ \bot \bot \bot 3*loop headed = 0, 3*unused]   
    int ave[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};        // for ave these are strait counts thus all = 0
    reset_counts(from->my_entry, minmax);    // \forall n len(n) = [ \bot \bot \bot 3*loop headed = 0]   
    from->counts[CO] = 0;   // 111 counts vertices 000 counts edges ...
    from->counts[DO] = 0;
    from->counts[BOTH] = 0;
    current_path_head = 0;
    if (current_path_head != 0) 
        fprintf(stderr, "ouch, current_path_head = %d which is not zero\n", current_path_head);
    printf("\nmin walk "); pdg_vertex_print_short(from); printf("\n");
    walk(from, min_meet);  
    print_bests(from);

    reset_counts(from->my_entry, minmax);    // \forall n len(n) = [ \bot \bot \bot ]   
    from->counts[CO] = 0;   // 111 counts vertices 000 counts edges ...
    from->counts[DO] = 0;
    from->counts[BOTH] = 0;
    current_path_head = 0;
    printf("\nmax walk "); pdg_vertex_print_short(from); printf("\n");
    // JRG commented out walk(from, max_meet);  
    print_bests(from);

    from->counts[CO] = 1;   
    from->counts[DO] = 1;
    from->counts[BOTH] = 1;
    from->counts[CO+3] = 0;   
    from->counts[DO+3] = 0;
    from->counts[BOTH+3] = 0;
    printf("\nave walk "); pdg_vertex_print_short(from); printf("\n");

    dwb = 1;
	dwb_bail_message = FALSE;
    current_path_head = 0;
    reset_counts(from->my_entry, ave);    
    loop_head_count = 0;
    // int bailed = walk_ave(from, BOTH);  // works with triples and i'm to lazy to do the proper abstraction
    int bailed = TRUE;

    dwb = 1;
	dwb_bail_message = FALSE;
    current_path_head = 0;
    loop_head_count = 0;
    // walk_ave(from, DO);  

    dwb = 1;
	dwb_bail_message = FALSE;
    current_path_head = 0;
    loop_head_count = 0;
    // walk_ave(from, CO);  

    //print_best_averages(from);
    //printf("\n");

    if (bailed)
    {
        reset_counts(from->my_entry, ave);    
		 consecutive_small_count = 0;
         last_small = FALSE;
        printf("sample walk begins\n");
        walk_sampled_ave(from, BOTH);
        print_best_averages(from);
    }


    if (current_path_head != 0) fprintf(stderr, "ouch, current_path_head = %d which is not zero\n", current_path_head);
}



static int control_self(tPdgVertex v)
{
    tPdgVertex *edges = v->incoming_intra_edges;
    if (edges != NULL) 
    {
        while (strip_edge_kind(*edges) != NULL) 
        {
            tPdgVertex target = strip_edge_kind(*edges++);
            if (v == target)
                return 1;    // usage in walk requires this to be a 1  
        }
    }

    return 0;
}


static void path_print(tPdgVertex *p)
{
    tPdgVertex *v;
    for(v = p; *v != NULL; v++)
        printf("  %x ", (*v)->name);
    printf("\n"); 
}


static void lint()
{
    lint(); path_print(NULL); control_self(NULL);
}
