#ifndef __GRAPH_CPP__
#define __GRAPH_CPP__

#include "graph.h"

#include <algorithm>
#include <iostream>

namespace std
{
  template< class Key >
  DirectedGraph< Key >::DirectedGraph()
  {
  }
  

  template< class Key >
  void DirectedGraph< Key >::copyExceptIncoming(const DirectedGraph< Key >& g, const Key& v)
  {
    for (typename vector< Key >::const_iterator i = g.vertexList().begin(); i != g.vertexList().end(); i++)
      {
	addVertex(*i);
	vector< Key > adj = g.adjacentFrom(*i);
	for (typename vector< Key >::iterator j = adj.begin(); j != adj.end(); j++)
	  {
	    if (*j != v)
	      {
		addEdge(*i, *j);
	      }
	  }
      }
  }

  template< class Key >
  bool DirectedGraph< Key >::addVertex(Key v)
  {
    if (keys.count(v) == 0)
      {
	// vertex was not already present -- add it
	vertices.push_back(v);
	
	// create empty adjacency lists for it
	inAdjLists.push_back(vector< int >());
	outAdjLists.push_back(vector< int >());
	
	// keep track of its index
	keys[v] = vertices.size() - 1;
	
	return true;
      }
    else
      {
	return false;
      }
  }
  
  template< class Key >
  void DirectedGraph< Key >::addEdge(Key from, Key to)
  {
    // ensure that both vertices are present
    addVertex(from);
    addVertex(to);
    
    // get indices of vertices
    int fromIndex = keys[from];
    int toIndex = keys[to];
    
    // add vertices to adjacency lists
    outAdjLists[fromIndex].push_back(toIndex);
    inAdjLists[toIndex].push_back(fromIndex);
  }

  template< class Key >
  const vector< Key >& DirectedGraph< Key >::vertexList() const
  {
    return vertices;
  }
  
  template< class Key >
  vector< Key > DirectedGraph< Key >::adjacentFrom(const Key& v) const
  {
    if (keys.count(v) > 0)
      {
	int index = (*(keys.find(v))).second;
	return makeKeyList(outAdjLists[index]);
      }
    else
      {
	return vector< Key >();
      }
  }
  
  template< class Key >
  vector< Key > DirectedGraph< Key >::adjacentTo(const Key& v) const
  {
    if (keys.count(v) > 0)
      {
	int index = (*(keys.find(v))).second;
	return makeKeyList(inAdjLists[index]);
      }
    else
      {
	return vector< Key >();
      }
  }
  
  template< class Key >
  vector< Key > DirectedGraph< Key >::makeKeyList(const vector< int >& indices) const
  {
    vector< Key > result;
    
    for (unsigned int i = 0; i < indices.size(); i++)
      {
	result.push_back(vertices[indices[i]]);
      }
    
    return result;
  }

  template< class Key >
  bool DirectedGraph< Key >::existsIncomingEdge(const Key& v, const Predicate< Key >& pred) const
  {
    if (keys.count(v) == 0)
      {
	return false;
      }
    else
      {
	int index = (*(keys.find(v))).second;
	return existsEdge(index, pred, inAdjLists);
      }
  }

  template< class Key >
  bool DirectedGraph< Key >::existsOutgoingEdge(const Key& v, const Predicate< Key >& pred) const
  {
    if (keys.count(v) == 0)
      {
	return false;
      }
    else
      {
	int index = (*(keys.find(v))).second;

	return existsEdge(index, pred, outAdjLists);
      }
  }
  
  template< class Key >
  bool DirectedGraph< Key >::existsEdge(int v, const Predicate< Key >& pred, const vector< vector< int > >& adjList) const
  {
    unsigned int i = 0;
    while (i < adjList[v].size() && !pred(vertices[adjList[v][i]]))
      {
	i++;
      }
    return (i < adjList[v].size());
  }

  template< class Key >
  void DirectedGraph< Key >::dfs(Key v, DFSVisitor< Key >& visitor) const
  {
    // make a compound visitor with user-supplied and one that will
    // keep track of which vertices have already been visited
    
    DFSColorizer< Key > colorizer;
    CompoundDFSVisitor< Key > compoundVisitor(colorizer, visitor);
    dfsTraverse(v, compoundVisitor, colorizer);
  }

  
  template< class Key >
  void DirectedGraph< Key >::fullDFS(DFSVisitor< Key >& visitor) const
  {
    // make a compound visitor with user-supplied and one that will
    // keep track of which vertices have already been visited
    
    DFSColorizer< Key > colorizer;
    CompoundDFSVisitor< Key > compoundVisitor(colorizer, visitor);

    for (unsigned int i = 0; i < vertices.size(); i++)
      {
	if (colorizer.getColor(vertices[i]) == DFSColorizer< Key >::WHITE)
	  {
	    startDFS(i, compoundVisitor, colorizer);
	  }
	else
	  {
	  }
      }
  }

  template< class Key >
  void DirectedGraph< Key >::dfsTraverse(Key v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const
  {
    if (keys.count(v) > 0)
      {
	int index = (*(keys.find(v))).second;
	
	startDFS(index, visitor, acceptor);
      }
  }

  template< class Key >
  void DirectedGraph< Key >::startDFS(int v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const
  {
    visitor.startDFS(vertices[v]);
    dfs(v, visitor, acceptor);
    visitor.endDFS(vertices[v]);
  }

  /**
   * The internal dfs method.
   *
   * @param v the index of the vertex to visit
   * @param visitor the visitor object that processes vertices as they are visited
   * @param visted the set of already visited vertices 
   */
  template< class Key >
  void DirectedGraph< Key >::dfs(int v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const
  {
    for (unsigned int i = 0; i < outAdjLists[v].size(); i++)
      {
	int u = outAdjLists[v][i];
	if (acceptor.shouldFollow(vertices[v], vertices[u]))
	  {
	    // visit u from v
	    visitor.preVisit(vertices[u], vertices[v]);
	    dfs(u, visitor, acceptor);
	    visitor.postVisit(vertices[u], vertices[v]);
	  }
      }
  }

  template < class Key >
  DirectedGraph< Key > DirectedGraph< Key >::reverse() const
  {
    return DirectedGraph< Key >(vertices, keys, outAdjLists, inAdjLists);
  }

  template < class Key >
  DirectedGraph< Key >::DirectedGraph(const vector< Key >& verts, const tr1::unordered_map< Key, int >& k, const vector< vector< int > >& in, const vector< vector< int > >& out)
    : vertices(verts),
      keys(k),
      inAdjLists(in),
      outAdjLists(out)
  {
  }

  template < class Key >
  std::vector< Key > DirectedGraph< Key >::topologicalSort() const
  {
    TopologicalSorter< Key > visit;

    fullDFS(visit);
    return visit.getOrder();
  }

  template < class Key >
  tr1::unordered_map< Key, int > DirectedGraph< Key >::findSCCs() const
  {
    // standard DFS with finishing times
    DFSColorizer< Key > colorizer;
    DFSTimeCounter< Key > dfsTimes;
    CompoundDFSVisitor< Key > compoundVisitor(colorizer, dfsTimes);
    
    for (unsigned int i = 0; i < vertices.size(); i++)
      {
	if (colorizer.getColor(vertices[i]) == DFSColorizer< Key >::WHITE)
	  {
	    startDFS(i, compoundVisitor, colorizer);
	  }
      }

    // reverse the graph
    DirectedGraph< Key > rev = reverse();

    // sort the vertices by decreasing finish time -- we know that finish times
    // are from 0 to 2*|V|, so we will place the indices of the vertices
    // into a vector in the place determined by their finish times; we can
    // then skip the -1's that are left in the positions of the start times

    vector< int > order;
    for (unsigned int i = 0; i < 2 * vertices.size(); i++)
      {
	order.push_back(-1);
      }
    for (unsigned int i = 0; i < vertices.size(); i++)
      {
	order[dfsTimes.getFinishTime(vertices[i])] = i;
      }
    
    // do DFS on reversed graph in order of decreasing finish time
    DFSColorizer< Key > rColors;
    SCCBuilder< Key > sccs;
    CompoundDFSVisitor< Key > revCompoundVisitor(rColors, sccs);

    for (int i = 2 * vertices.size() - 1; i >= 0; i--)
      {
	if (order[i] != -1 && rColors.getColor(vertices[order[i]]) == DFSColorizer< Key >::WHITE)
	  {
	    rev.startDFS(order[i], revCompoundVisitor, rColors);
	  }
      }

    // retrieve and return SCC map
    return sccs.getSCCMap();
  }
  
  /* Destructors */
  template < class Key >
  Predicate< Key >::~Predicate()
  {
  }

  template < class Key >
  DFSEdgeAcceptor< Key >::~DFSEdgeAcceptor()
  {
  }
  
  template < class Key >
  DFSVisitor< Key >::~DFSVisitor()
  {
  }

  /*---------------------------CompoundDFSVisitor--------------------*/
  template < class Key >
  CompoundDFSVisitor< Key >::~CompoundDFSVisitor()
  {
  }

  template < class Key >
  CompoundDFSVisitor< Key >::CompoundDFSVisitor(DFSVisitor< Key >& v1, DFSVisitor< Key >& v2)
    : visitor1(v1),
      visitor2(v2)
  {
  }

  template < class Key >
  void CompoundDFSVisitor< Key >::startDFS(const Key& v)
  {
    visitor1.startDFS(v);
    visitor2.startDFS(v);
  }

  template < class Key >
  void CompoundDFSVisitor< Key >::endDFS(const Key& v)
  {
    visitor1.endDFS(v);
    visitor2.endDFS(v);
  }

  template < class Key >
  void CompoundDFSVisitor< Key >::preVisit(const Key& v, const Key& parent)
  {
    visitor1.preVisit(v, parent);
    visitor2.preVisit(v, parent);
  }

  template < class Key >
  void CompoundDFSVisitor< Key >::postVisit(const Key& v, const Key& parent)
  {
    visitor1.postVisit(v, parent);
    visitor2.postVisit(v, parent);
  }

  /*----------------------------DFSColorizer-------------------------*/
  template < class Key >
  DFSColorizer< Key >::~DFSColorizer()
  {
  }

  template < class Key >
  void DFSColorizer< Key >::startDFS(const Key& v)
  {
    colors[v] = GRAY;
  }

  template < class Key >
  void DFSColorizer< Key >::endDFS(const Key& v)
  {
    colors[v] = BLACK;
  }
    
  template < class Key >
  void DFSColorizer< Key >::preVisit(const Key& v, const Key& parent)
  {
    colors[v] = GRAY;
  }

  template < class Key >
  void DFSColorizer< Key >::postVisit(const Key& v, const Key& parent)
  {
    colors[v] = BLACK;
  }

  template < class Key >
  bool DFSColorizer< Key >::shouldFollow(const Key& from, const Key& to) const
  {
    return (getColor(to) == WHITE);
  }

  template < class Key >
  int DFSColorizer< Key >::getColor(const Key& v) const
  {
    if (colors.count(v) == 0)
      {
	return WHITE;
      }
    else
      {
	return (*(colors.find(v))).second;
      }
  }

  /*----------------------------DFSTimeCounter-----------------------*/
  template < class Key >
  DFSTimeCounter< Key >::~DFSTimeCounter()
  {
  }
  
  template < class Key >
  DFSTimeCounter< Key >::DFSTimeCounter()
  {
    time = 0;
  }

  template < class Key >
  void DFSTimeCounter< Key >::startDFS(const Key& v)
  {
    startTimes[v] = time++;
  }

  template < class Key >
  void DFSTimeCounter< Key >::endDFS(const Key& v)
  {
    finishTimes[v] = time++;
  }

  template < class Key >
  void DFSTimeCounter< Key >::preVisit(const Key& v, const Key& parent)
  {
    startTimes[v] = time++;
  }

  template < class Key >
  void DFSTimeCounter< Key >::postVisit(const Key& v, const Key& parent)
  {
    finishTimes[v] = time++;
  }

  template< class Key >
  int DFSTimeCounter< Key >::getStartTime(const Key& v) const
  {
    if (startTimes.count(v) > 0)
      {
	return (*(startTimes.find(v))).second;
      }
    else
      {
	return -1;
      }
  }

  template< class Key >
  int DFSTimeCounter< Key >::getFinishTime(const Key& v) const
  {
    if (finishTimes.count(v) > 0)
      {
	return (*(finishTimes.find(v))).second;
      }
    else
      {
	return -1;
      }
  }

  /*-----------------------TopologicalSorter----------------*/
  template < class Key >
  TopologicalSorter< Key >::~TopologicalSorter()
  {
  }

  template < class Key >
  void TopologicalSorter< Key >::startDFS(const Key& v)
  {
  }

  template < class Key >
  void TopologicalSorter< Key >::endDFS(const Key& v)
  {
    order.push_back(v);
  }

  template < class Key >
  void TopologicalSorter< Key >::preVisit(const Key& v, const Key& parent)
  {
  }

  template < class Key >
  void TopologicalSorter< Key >::postVisit(const Key& v, const Key& parent)
  {
    order.push_back(v);
  }

  template < class Key >
  const vector< Key >& TopologicalSorter< Key >::getOrder() const
  {
    return order;
  }

  /*-----------------------SCCBuilder-----------------------*/
  template < class Key >
  SCCBuilder< Key >::~SCCBuilder()
  {
  }

  template < class Key >
  SCCBuilder< Key >::SCCBuilder()
  {
    currentComponent = 0;
  }

  template < class Key >
  void SCCBuilder< Key >::startDFS(const Key& v)
  {
    components[v] = currentComponent;
  }

  template < class Key >
  void SCCBuilder< Key >::endDFS(const Key& v)
  {
    currentComponent++;
  }

  template < class Key >
  void SCCBuilder< Key >::preVisit(const Key& v, const Key& parent)
  {
    components[v] = currentComponent;
  }

  template < class Key >
  void SCCBuilder< Key >::postVisit(const Key& v, const Key& parent)
  {
  }

  template< class Key >
  tr1::unordered_map< Key, int> SCCBuilder< Key >::getSCCMap() const
  {
    return components;
  }

  /*---------------------------PathProcessor---------------------*/
  template < class Key >
  PathProcessor< Key >::~PathProcessor()
  {
  }

  template < class Key >
  void PathProcessor< Key >::startDFS(const Key& v)
  {
    addToPath(v);
  }

  template < class Key >
  void PathProcessor< Key >::endDFS(const Key& v)
  {
    removeLastOnPath();
  }

  template < class Key >
  void PathProcessor< Key >::preVisit(const Key& v, const Key& parent)
  {
    addToPath(v);
  }

  template < class Key >
  void PathProcessor< Key >::postVisit(const Key& v, const Key& parent)
  {
    removeLastOnPath();
  }

  template < class Key >
  bool PathProcessor< Key >::shouldFollow(const Key& from, const Key& to) const
  {
    return (onPath.count(to) == 0);
  }

  template < class Key >
  void PathProcessor< Key >::addToPath(const Key& v)
  {
    onPath.insert(v);
    path.push_back(v);

    /*
    cout << "<";
    for (unsigned int i = 0; i < path.size(); i++)
      {
	cout << " " << path[i];
      }
    cout << " >" << endl;
    */
  }

  template < class Key >
  void PathProcessor< Key >::removeLastOnPath()
  {
    onPath.erase(path[path.size() - 1]);
    path.pop_back();
  }

};

#endif
