/* sdg.c */

#define DEBUG 0

#include "stdio.h"
#include "stdlib.h"
#include "sdg.h"
#include "pdg.h"
#include "ht.h"
#include "list.h"
#include "misc.h"


static void rebuild_list(tPdgVertex *l, tHashTable sdg)
{
    int i;

    if (l == 0)
        return;

    for(i=0; l[i] != 0; i++)
    {
	    int data = data_edge(l[i]);
        tPdgVertex u = pdg_vertex_lookup((long)strip_edge_kind(l[i]), sdg);
	// printf("REBUILD LIST: %x %x\n", (long)strip_edge_kind(l[i]), (long)(u ? u->name : 0));
        if (u == NULL)
        {
           fprintf(stderr,"can't find vertex %x\n", (long)l[i]);
           //if ((int)l[i] != 0)exit(-5);
        }
        l[i] = (tPdgVertex) ((long)u | data);
    }
}


// replace name of source of edge with pointer to actual vertex.
// this avoids future lookups
static void rebuild_incoming_edge_lists(tPdgVertex v, tHashTable sdg)
{
  // printf("REBUILD LIST: %x\n", (int)(v->name));
    rebuild_list(v->incoming_intra_edges, sdg);
    rebuild_list(v->incoming_inter_edges, sdg);
    rebuild_list(v->outgoing_intra_edges, sdg);
    rebuild_list(v->outgoing_inter_edges, sdg);
}


tHashTable sdg_read()
{
    tHashTable sdg = ht_initialize(10000, pdg_vertex_compare, pdg_vertex_hash);
    tPdgVertex v;

    for(v = pdg_vertex_read(sdg); 
        v != NULL;
        v = pdg_vertex_read(sdg))
    {
        if (DEBUG) pdg_vertex_print(v); 
        ht_insert(sdg, v);
    }

    ht_foreach1(sdg, (FN2ARG)&rebuild_incoming_edge_lists, sdg);
    return sdg;
}
