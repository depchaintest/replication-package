#ifndef __SCC_PATH_COUNTER_CPP
#define __SCC_PATH_COUNTER_CPP

#include "scc_path_counter.h"

extern int scc_processing_timeout;

namespace std
{
  template < class Key >
  SCCPathCounter< Key >::SCCPathCounter(const DirectedGraph< Key >& graph, const Key& s, const tr1::unordered_set< Key >& exits, const tr1::unordered_map< Key, int >& sccs, int time)
    : TimeLimitedComputation< tr1::unordered_map< Key, PathStatistics< Key > > >(time),
      g(graph),
      source(s),
      exitPoints(exits),
      sccMap(sccs)
  {
    component = (*(sccMap.find(source))).second;
  }

  template< class Key>
  tr1::unordered_map< Key, PathStatistics< Key > > SCCPathCounter< Key >::compute()
  {
    g.dfsTraverse(source, *this, *this);

    return getPathCounts();
  }

  template< class Key>
  tr1::unordered_map< Key, PathStatistics< Key > > SCCPathCounter< Key >::timeoutValue()
  {
    // return a map (exit -> infinity) for every exit point in the same
    // SCC as the source

    tr1::unordered_map< Key, PathStatistics< Key > > result;
    for (typename tr1::unordered_set< Key >::const_iterator i = exitPoints.begin(); i != exitPoints.end(); i++)
      {
	if ((*(sccMap.find(*i))).second == component)
	  {
	    result[*i] = PathStatistics< Key >::infinity();
	  }
      }

    return result;
  }

  template < class Key >
  const tr1::unordered_map< Key, PathStatistics< Key > >& SCCPathCounter< Key >::getPathCounts() const
  {
    return pathCounts;
  }

  template < class Key >
  void SCCPathCounter< Key >::addToPath(const Key& v)
  {
    if (this->timeExpired())
      {
	throw TimeLimitExceededException();
      }

    PathProcessor< Key >::addToPath(v);

    if (exitPoints.count(v) > 0)
      {
	// create entry with empty statistics if entry doesn't already exist
	if (pathCounts.count(v) == 0)
	  {
	    pathCounts[v] = PathStatistics< Key >();
	  }

	// update statistics for the current path
	pathCounts[v] = pathCounts[v] + PathProcessor< Key >::path;
      }
  }

  template < class Key >
  bool SCCPathCounter< Key >::shouldFollow(const Key& from, const Key& to) const
  {
    // traverse edge if destination isn't already on current path and
    // is in the same component
    return (PathProcessor< Key >::shouldFollow(from, to)
	    && sccMap.count(to) > 0
	    && (*(sccMap.find(to))).second == component);
  }
};

#endif
