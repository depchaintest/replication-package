#ifndef __GRAPH_UTILS_CPP__
#define __GRAPH_UTILS_CPP__

#include "graph_utils.h"

#include "scc_path_counter.cpp"

#include <vector>

namespace std
{

// from http://stackoverflow.com/questions/20590656/error-for-hash-function-of-pair-of-ints
struct pairhash {
public:
  template <typename T, typename U>
  size_t operator()(const std::pair<T, U> &x) const
  {
    /*
    std::size_t seed = 0;
    tr1::hash_combine(seed, tr1::hash<T>()(x.first));
    tr1::hash_combine(seed, tr1::hash<T>()(x.second));
    return seed;
    */
    return tr1::hash<T>()(x.first) ^ (tr1::hash<U>()(x.second) << 16);
  }
};

/*----------------------HalfVertex------------------------*/
// a type for (vertex, <entry, exit>) pairs (instead of pair so I can supply a specialization
// of hash

template < class Key >
class HalfVertex
{
public:
  HalfVertex(const Key& v, int h);
  
  Key getVertex() const;
  int getHalf() const;
  bool isEntry() const;
  bool isExit() const;

  bool operator==(const HalfVertex& other) const;

private:
  Key vertex;
  int half;
};

template < class Key >
HalfVertex< Key >::HalfVertex(const Key & v, int h)
{
  vertex = v;
  half = h;
}

template < class Key >
Key HalfVertex< Key >::getVertex() const
{
  return vertex;
}

template < class Key >
int HalfVertex< Key >::getHalf() const
{
  return half;
}

template < class Key >
bool HalfVertex< Key >::isEntry() const
{
  return half == 0;
}

template < class Key >
bool HalfVertex< Key >::isExit() const
{
  return half == 1;
}

template < class Key >
bool HalfVertex< Key >::operator==(const HalfVertex& other) const
{
  return vertex == other.vertex && half == other.half;
}

  template < class Key >
  ostream& operator<<(ostream& os, const HalfVertex< Key >& v)
  {
    os << "(" << v.getVertex() << ", " << (v.isEntry() ? "ENTRY" : "EXIT") << ")";

    return os;
  }

namespace tr1
{
template < class Key >
struct hash< HalfVertex< Key > >
{
  size_t operator()(const HalfVertex< Key >& v) const
  {
    // shift the bits of the vertex's hash over by 1 and add the entry/exit bit
    return (hash< Key >()(v.getVertex()) << 1) + v.getHalf();
  }
};
}

/*----------------------DifferentSCC------------------------*/

template < class Key >
DifferentSCC< Key >::DifferentSCC(const Key& v, const tr1::unordered_map< Key, int >& s)
  : sccs(s)
{
  component = (*(sccs.find(v))).second;
}

template < class Key >
DifferentSCC< Key >::~DifferentSCC()
{
}

template < class Key >
bool DifferentSCC< Key >::operator()(const Key& u) const
{
  int otherComponent = (*(sccs.find(u))).second;
  return (component != otherComponent);
}

/**
 * Adds the given vertex to the given set if it is an entry point for
 * its SCC.
 */
template< class Key >
void addIfSCCEntry(const Key& v, tr1::unordered_set< Key >& s, const DirectedGraph< Key >& g, const tr1::unordered_map< Key, int >& sccs)
{
  DifferentSCC< Key > isEntry(v, sccs);

  if (g.existsIncomingEdge(v, isEntry))
    {
      s.insert(v);
    }
}

/**
 * Adds the given vertex to the given set if it is an exit point for its SCC.
 */
template< class Key >
void addIfSCCExit(const Key& v, tr1::unordered_set< Key >& s, const DirectedGraph< Key >& g, const tr1::unordered_map< Key, int >& sccs)
{
  DifferentSCC< Key > isExit(v, sccs);

  if (g.existsOutgoingEdge(v, isExit))
    {
      s.insert(v);
    }
}

template< class Key >
void printDAG(const DirectedGraph< Key >& g, tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash >& labels)
{
  cout << "DAG" << endl;
  for (typename vector< Key >::const_iterator i = g.vertexList().begin();
       i != g.vertexList().end();
       i++)
    {
      vector< Key > adj = g.adjacentFrom(*i);
      for (typename vector< Key >::iterator j = adj.begin(); j != adj.end(); j++)
	{
	  cout << *i << "->" << *j << ": " << labels[pair< Key, Key >(*i, *j)] << endl;
	}
    }
}

template < class Key >
tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash > countPaths(const DirectedGraph< Key >& g, const tr1::unordered_set< Key >& starts, const tr1::unordered_set< Key >& ends, vector< long >& trace, int depth, int maxDepth, int& deepest);

/**
 * Computes path statistics for the given starting points in the given graph.
 */
template < class Key >
tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash > countPaths(const DirectedGraph< Key >& g, const tr1::unordered_set< Key >& starts, const tr1::unordered_set< Key >& ends, int maxDepth, int& deepest)
{
  vector< long > trace;
  trace.push_back(0L);
  
  return countPaths(g, starts, ends, trace, 0, maxDepth, deepest);
}

template < class Key >
PathStatistics< Key > estimatePaths(const DirectedGraph< Key >& g, Key start, Key end, int numTrials)
{
  // make a vector of counts for each possible path length
  std::vector< double > counts;
  for (size_t i = 0; i < g.vertexList().size() + 1; i++)
    {
      counts.push_back(0.0);
    }
  double totalImport = 0.0;
  double totalPathImport = 0.0;
  double totalLength = 0.0;
  int min = g.vertexList().size();
  int max = -1;

  // do some random walks
  for (int i = 0; i < numTrials; i++)
    {
      Key curr = start;
      bool dead = false;
      tr1::unordered_set< Key > visited;
      visited.insert(start);
      double importance = 1.0;
      while (curr != end && !dead)
	{
	  std::vector< Key > cand;
	  const std::vector< Key > neighbors = g.adjacentFrom(curr);
	  for (typename std::vector< Key >::const_iterator i = neighbors.begin();
	       i != neighbors.end();
	       i++)
	    {
	      if (visited.count(*i) == 0)
		{
		  cand.push_back(*i);
		}
	    }

	  // randomly choose one
	  if (cand.size() == 0)
	    {
	      dead = true;
	    }
	  else
	    {
	      importance = importance * cand.size();
	      curr = cand[random() % cand.size()];
	      visited.insert(curr);
	    }
	}

      // end of random walk -- count
      totalImport += importance;
      if (!dead)
	{
	  int len = visited.size() - 1;

	  counts[len] += importance;
	  totalPathImport += importance;
	  totalLength += importance * len;

	  if (len > max) max = len;
	  if (len < min) min = len;
	}
    }
  
  PathStatistics< Key > result = PathStatistics< Key >::estimate(min, max, totalPathImport / numTrials, totalLength / numTrials);

  //std::cout << result << std::endl;

  return result;
}

template < class Key >
PathStatistics< Key > estimatePaths(const DirectedGraph< Key >& g, Key start, Key end)
{
  // change back to false to find a good graph for students to play with
  static bool outputted = true;

  size_t size = g.vertexList().size();

  if (size >= 30 && size <= 50 && !outputted)
    {
      outputted = true;

      // check if this doesn't break up quickly
      int deepest = 0;
      tr1::unordered_set< Key > starts;
      starts.insert(start);
      tr1::unordered_set< Key > ends;
      ends.insert(end);
      countPaths(g, starts, ends, 5, deepest);

      if (deepest == 5)
	{
	  // the graph held together!
	  tr1::unordered_map< Key, size_t > indices;
	  for (typename std::vector< Key >::const_iterator i = g.vertexList().begin(); i != g.vertexList().end(); i++)
	    {
	      indices[*i] = indices.size();
	    }
	  
	  for (typename std::vector< Key >::const_iterator i = g.vertexList().begin(); i != g.vertexList().end(); i++)
	    {
	      std::vector< Key > adj = g.adjacentFrom(*i);
	      for (typename std::vector< Key >::iterator j = adj.begin(); j != adj.end(); j++)
		{
		  if (indices[*i] != indices[*j])
		    {
		      std::cout << "GRAPH-EDGE: " << indices[*i] << " " << indices[*j] << std::endl;
		    }
		}
	    }
	  std::cout << "GRAPH-ENDS: " << indices[start] << " " << indices[end] << std::endl;
	}
      else
	{
	  // didn't hold together -- try for another one
	  outputted = false;
	}
    }
      
  if (estimation_trials <= 0)
    {
      return PathStatistics<Key>::maxForGraph(size);
    }
  else
    {
      return estimatePaths(g, start, end, estimation_trials);
    }
}

template < class Key >
tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash > countPaths(const DirectedGraph< Key >& g, const tr1::unordered_set< Key >& starts, const tr1::unordered_set< Key >& ends, vector< long >& trace, int depth, int maxDepth, int& deepest)
{
  if (depth > deepest) deepest = depth;

  tr1::unordered_map< pair< Key, Key >, PathStatistics< Key >, pairhash > results;
  
  if (g.vertexList().size() == 1)
    {
      return results;
    }
 
  if (depth == maxDepth)
    {
      // std::cout << "Estimating for graph with " << g.vertexList().size() << " vertices" << std::endl;
      // for every (start, end) pair, just report average path length is
      // size of graph
      for (typename tr1::unordered_set< Key >::const_iterator i = starts.begin();
	   i != starts.end();
	   i++)
	{
	  for (typename tr1::unordered_set< Key >::const_iterator j = ends.begin();
	       j != ends.end();
	       j++)
	    {
	      results[std::pair< Key, Key >(*i, *j)] = estimatePaths(g, *i, *j);
	    }
	}

      return results;
    }
 
  // find the SCCs in g
  //cout << "finding SCCs" << endl;
  tr1::unordered_map< Key, int > sccs = g.findSCCs();

  // count the SCCs
  int sccCount = 0;
  for ( typename tr1::unordered_map< Key, int >::const_iterator i = sccs.begin();
	i != sccs.end();
	i++)
    {
      if ((*i).second >= sccCount)
	{
	  sccCount = (*i).second + 1;
	}
    }
  
  /*
  cout << "COUNT-PATHS: " << g.vertexList().size() << " vertices, "
       << sccCount << " sccs ";

  int j = 0;
  for (typename vector< long >::iterator i = trace.begin(); i != trace.end(); i++, j++)
    {
      if (j == 0)
	{
	  cout << dec << (*i);
	}
      else
	{
	  cout << "-" << (*i);
	}
    }
  cout << endl;
  */

  // initalize a new graph in which paths between entries and exits will be represented
  // by a single edge labelled with path counts and edges between SCCs copied with a label
  // of <1, 1, 1, 1>; this new graph will be a DAG
  DirectedGraph< HalfVertex< Key > > dag;
  tr1::unordered_map< pair< HalfVertex< Key >, HalfVertex< Key > >, PathStatistics< HalfVertex< Key > >, pairhash > labels;

  // build the subgraph for each SCC with entry points and exit points noted
  DirectedGraph< Key > subgraphs[sccCount];
  tr1::unordered_set< Key > entries[sccCount];
  tr1::unordered_set< Key > exits[sccCount];
  
  for (typename vector< Key >::const_iterator i = g.vertexList().begin();
       i != g.vertexList().end();
       i++)
    {
      int currSCC = sccs[*i];
      subgraphs[currSCC].addVertex(*i);
      HalfVertex< Key > entryHalf(*i, 0);
      HalfVertex< Key > exitHalf(*i, 1);
      dag.addEdge(entryHalf, exitHalf);
      labels[pair< HalfVertex< Key >, HalfVertex< Key > >(entryHalf, exitHalf)] = PathStatistics< HalfVertex< Key > >::empty();

      vector< Key > adj = g.adjacentFrom(*i);
      for (typename vector< Key >::iterator j = adj.begin();
	   j != adj.end();
	   j++)
	{
	  int otherSCC = sccs[*j];
	  if (otherSCC == currSCC)
	    {
	      // same SCC, so add the edge
	      subgraphs[currSCC].addEdge(*i, *j);
	    }
	  else
	    {
	      // different SCCs, so add the edge to the DAG with label <1, 1, 1, 1>
	      dag.addEdge(HalfVertex< Key >(*i, 1), HalfVertex< Key >(*j, 0));
	      labels[pair< HalfVertex< Key >, HalfVertex< Key > >(HalfVertex< Key >(*i, 1), HalfVertex< Key >(*j, 0))] = PathStatistics< HalfVertex< Key > >::one();

	      exits[currSCC].insert(*i);
	      entries[otherSCC].insert(*j);
	      
	    }
	}
    }

  // add starts and ends to SCC entry and exit points
  for (typename tr1::unordered_set< Key >::const_iterator i = starts.begin();
       i != starts.end();
       i++)
    {
      entries[sccs[*i]].insert(*i);
    }

  for (typename tr1::unordered_set< Key >::const_iterator i = ends.begin();
       i != ends.end();
       i++)
    {
      exits[sccs[*i]].insert(*i);
    }
  

  // find path counts between entry and exit points in SCCs
  for (int c = 0; c < sccCount; c++)
    {
      trace.push_back(sccCount - 1 - c);

      int x = entries[c].size() - 1;
      for (typename tr1::unordered_set< Key >::iterator i = entries[c].begin(); i != entries[c].end(); i++)
	{
	  // make a copy of the current SCC with edges into the current start point removed
	  DirectedGraph< Key > temp;
	  temp.copyExceptIncoming(subgraphs[c], *i);
	  tr1::unordered_set< Key > tempStarts;
	  tempStarts.insert(*i);
	  
	  trace.push_back(x);
	  tr1::unordered_map< pair< Key, Key>, PathStatistics<Key>, pairhash > counts = countPaths(temp, tempStarts, exits[c], trace, depth + 1, maxDepth, deepest);
	  trace.pop_back();
	  x = x - 1;
	  
	  for (typename tr1::unordered_map< pair< Key, Key>, PathStatistics<Key>, pairhash >::iterator j = counts.begin();
	       j != counts.end();
	       j++)
	    {
	      HalfVertex< Key > from((*j).first.first, 0);
	      HalfVertex< Key > to((*j).first.second, 1);
	      dag.addEdge(from, to);
	      labels[pair< HalfVertex< Key >, HalfVertex< Key > >(from, to)] = PathStatistics< HalfVertex< Key > >::copy((*j).second);
	    }
	}
      trace.pop_back();
    }

  // printDAG(dag, labels);
  
  // compute results for each starting point in the DAG 
  vector< HalfVertex< Key > > order = dag.topologicalSort();
  /*
  cout << "TOPO SORT";
  for (typename vector< Key >::iterator i = order.begin(); i != order.end(); i++)
    {
      cout << " " << *i;
    }
  cout << endl;

  cout << "RESULTS" << endl;
  */

  tr1::unordered_map< Key, long double > maxCounts;
  for (typename tr1::unordered_set< Key >::const_iterator i = starts.begin(); i != starts.end(); i++)
    {
      // start a new map of statistics starting from the current starting point
      tr1::unordered_map< HalfVertex< Key >, PathStatistics< HalfVertex< Key > > > vCounts;

      Key v = *i;
      vCounts[HalfVertex< Key >(v, 0)] = PathStatistics< HalfVertex< Key > >::empty();

      // go through vertices in order of topological sort
      for (typename vector< HalfVertex< Key > >::reverse_iterator j = order.rbegin(); j != order.rend(); j++)
	{
	  HalfVertex< Key > u = *j;

	  if (vCounts.count(u) > 0)
	    {
	      // update count for vertex at end of each edge coming out of u

	      vector< HalfVertex< Key > > adj = dag.adjacentFrom(u);
	      for (typename vector< HalfVertex< Key > >::iterator k = adj.begin(); k != adj.end(); k++)
		{
		  PathStatistics< HalfVertex< Key > > oldCount;
		  if (vCounts.count(*k) > 0)
		    {
		      oldCount = vCounts[*k];
		    }

		  vCounts[*k] = oldCount + vCounts[u] * labels[pair< HalfVertex< Key >, HalfVertex< Key > >(u, (*k))];

		}
	    }
	}

      // copy results for v into results for all exit points
      for (typename tr1::unordered_map< HalfVertex< Key >, PathStatistics< HalfVertex< Key > > >::iterator k = vCounts.begin();
	   k != vCounts.end();
	   k++)
	{
	  if ((*k).first.isExit() &&                   // is this an exit half?
	      ends.count((*k).first.getVertex()) > 0   // that is one we're interested in?
	      && (*k).first.getVertex() != v)          // and not the same as the current start?
	    {
	      PathStatistics< HalfVertex< Key > > c = (*k).second;
	      results[pair< Key, Key >(v, (*k).first.getVertex())] = PathStatistics< Key >::copy((*k).second);

	      if (maxCounts.count(v) == 0 || (*k).second.getCount() > maxCounts[v])
		{
		  maxCounts[v] = (*k).second.getCount();
		}
	    }

	  //cout << hex << v << "->" << (*k).first << ": " << dec << (*k).second << endl;
	}
    }

#ifdef __PD_GRAPH_H__
  /*
  // fake sccs for brute-force path counting
  tr1::unordered_map< Key, int > fakeSCCs;
  for (typename vector< Key >::const_iterator z = g.vertexList().begin();
       z != g.vertexList().end();
       z++)
    {
      fakeSCCs[*z] = 0;
    }

  for (typename tr1::unordered_set< Key >::const_iterator i = starts.begin();
       i != starts.end();
       i++)
    {
      Key source = *i;

      if (maxCounts[source] < 10000000)
	{
	  SCCPathCounter< Key > counter(g, source, ends, fakeSCCs, 0);
	  // call compute instead of run to avoid time limit
	  const tr1::unordered_map< Key, PathStatistics< Key > >& stats = counter.compute();
	  
	  for (typename tr1::unordered_map< Key, PathStatistics< Key > >::const_iterator j = stats.begin();
	       j != stats.end();
	       j++)
	    {
	      Key dest = (*j).first;
	      PathStatistics< Key > brute = (*j).second;
	      PathStatistics< Key > clever = results[pair< Key, Key >(source, dest)];
	      
	      if (!(brute == clever))
		{
		  cout << "MISCOUNT:  ";
		}
	      else
		{
		  cout << "VERIFIED: ";
		}
	      cout
		       << hex << source->name << dec
		       << (isEntry(dest) ? " ENTRY " : "")
		       << (isFormalIn(dest) ? " FORML " : "")
		       << (isBody(dest) ? " BODY  " : "")
		       << brute
		       << " @@@ "
		       << source->src
		       << " @@@ "
		       << hex << dest->name << dec
		       << " " << dest->src
		       << endl;
	    }
	}
      else
	{
	  cout << "UNCHECKABLE: " << hex << source->name << dec << "(" << maxCounts[source] << ")" << endl;
	}
    }
  */
#endif

  return results;
}
}
#endif
