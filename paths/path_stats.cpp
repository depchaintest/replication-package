#ifndef __PATH_STATS_CPP__
#define __PATH_STATS_CPP__

#include <iomanip>
#include <limits>
#include <vector>

#include "path_stats.h"

namespace std
{
  template < class T >
  PathStatistics< T >::PathStatistics()
  {
    min = numeric_limits< int >::max();;
    max = numeric_limits< int >::min();;
    count = 0.0;
    totalLength = 0.0;
    isEstimate = false;
  }

  template < class T >
  PathStatistics< T >::PathStatistics(int n, int x, long double c, long double len, bool est)
  {
    min = n;
    max = x;
    count = c;
    totalLength = len;
    isEstimate = est;
  }


  template < class T >
  PathStatistics< T > PathStatistics< T >::estimate(int mn, int mx, long double cnt, long double totLen)
  {
    return PathStatistics< T >(mn, mx, cnt, totLen, true);
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::maxForGraph(int n)
  {
    return PathStatistics< T >(1, n - 1, std::numeric_limits< long double >::infinity(), n - 1, true);
  }

  template < class T >
  bool PathStatistics< T >::operator ==(const PathStatistics< T >& other) const
  {
    return min == other.min && max == other.max && count == other.count && totalLength == other.totalLength;
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::operator +(const vector< T >& path) const
  {
    int pathLen = path.size() - 1;

    return PathStatistics< T >((pathLen >= min ? min : pathLen),
			       (pathLen <= max ? max : pathLen),
			       count + 1,
			       totalLength + pathLen,
			       isEstimate);
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::operator +(const PathStatistics< T >& stats) const
  {
    return PathStatistics< T >((stats.min >= min ? min : stats.min),
			       (stats.max <= max ? max : stats.max),
			       stats.count + count,
			       stats.totalLength + totalLength,
			       isEstimate || stats.isEstimate);
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::operator *(const PathStatistics< T >& stats) const
  {
    return PathStatistics< T >(min + stats.min,
			       max + stats.max,
			       count * stats.count,
			       count * stats.totalLength + stats.count * totalLength,
			       isEstimate || stats.isEstimate);

  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::infinity()
  {
    return PathStatistics< T >(numeric_limits< int >::min(),
			       numeric_limits< int >::max(),
			       numeric_limits< long double >::infinity(),
			       numeric_limits< long double >::infinity());
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::one()
  {
    return PathStatistics< T >(1, 1, 1.0, 1.0);
  }

  template < class T >
  PathStatistics< T > PathStatistics< T >::empty()
  {
    return PathStatistics< T >(0, 0, 1.0, 0.0);
  }

  template < class T >
  int PathStatistics< T >::getMinimum() const
  {
    return min;
  }

  template < class T >
  int PathStatistics< T >::getMaximum() const
  {
    return max;
  }

  template < class T >
  long double PathStatistics< T >::getCount() const
  {
    return count;
  }

  template < class T >
  bool PathStatistics< T >::isEstimated() const
  {
    return isEstimate;
  }

  template < class T >
  double PathStatistics< T >::getAverage() const
  {
    return (double)(totalLength / count);
  }

  template < class T >
  long double PathStatistics< T >::getTotalLength() const
  {
    return totalLength;
  }

  template< class T >
  template< class U >
  PathStatistics< T > PathStatistics< T >::copy(const PathStatistics< U >& other)
  {
    return PathStatistics< T >(other.getMinimum(), other.getMaximum(), other.getCount(), other.getTotalLength(), other.isEstimated());
  }

  template < class T >
  ostream& operator<<(ostream& os, const PathStatistics< T >& stats)
  {
    streamsize oldPrecision = os.precision();
    ios_base::fmtflags oldFlags = os.flags();

    if (stats.getCount() == numeric_limits< long double >::infinity())
      {
	os << "<min,max,count,avg> " << (stats.isEstimated() ? "[" : "<")
	   << "---,---,"
	   << setw(8) << stats.getCount()
	   << "," << setw(7) << fixed << setprecision(3) << stats.getAverage()
	   << (stats.isEstimated() ? "]" : ">");
      }
    else
      {
	os << "<min,max,count,avg> " << (stats.isEstimated() ? "[" : "<")
	   << setw(3) << stats.getMinimum()
	   << "," << setw(3) << stats.getMaximum();
	if (stats.getCount() < 10000000)
	  {
	    os << "," << setw(8) << fixed << setprecision(0) << stats.getCount();
	  }
	else
	  {
	    os << "," << setw(8) << scientific << setprecision(2) << stats.getCount();
	  }
	os << "," << setw(7) << fixed << setprecision(3) << stats.getAverage()
	   << (stats.isEstimated() ? "]" : ">");
      }

    os.precision(oldPrecision);
    os.flags(oldFlags);

    return os;
  }
};

#endif
