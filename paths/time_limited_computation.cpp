#include <unistd.h>
#include <cxxabi.h>

#include <iostream>


#include "time_limited_computation.h"

namespace std
{
  template < class T >
  pthread_mutex_t TimeLimitedComputation< T >::DEFAULT_MUTEX = PTHREAD_MUTEX_INITIALIZER;

  template < class T >
  pthread_cond_t TimeLimitedComputation< T >::DEFAULT_COND = PTHREAD_COND_INITIALIZER;

  template < class T >
  TimeLimitedComputation< T >::TimeLimitedComputation(int time)
    : lock(DEFAULT_MUTEX),
      closedCond(DEFAULT_COND)
  {
    timeLimit = time;
    expired = false;
    finished = false;
    closed = false;
  }

  template < class T >
  T TimeLimitedComputation< T >::run()
  {
    // start timer thread
    pthread_create(&timerThread, NULL, startTimer, this);
    pthread_detach(timerThread);

    try
      {
	// attempt computation and return result if it completes in time
	T result = compute();

	finished = true;

	return result;
      }
    catch (TimeLimitExceededException& e)
      {
	// computation didn't finish in time -- return flag value
	return timeoutValue();
      }
  }

  template < class T >
  void * TimeLimitedComputation< T >::startTimer(void *a)
  {
    TimeLimitedComputation< T >* tl = (TimeLimitedComputation< T > *)a;

    // poll for compute thread finished (cancellation by compute thread would
    // be better but cancellation and C++ don't mix)

    // first poll after .1ms, .2ms, ... 1/2 sec (total ~1 sec)
    // (so computations that finish quickly aren't held up by the
    // main thread making sure the timer finishes)
    for (int wait = 100; wait < 500000; wait *= 2)
      {
	usleep(wait);

	if (tl->finished)
	  {
	    tl->close();
	    return NULL;
	  }
      }

    for (int i = 0; i < tl->timeLimit - 1; i++)
      {
	sleep(1);

	if (tl->finished)
	  {
	    tl->close();
	    return NULL;
	  }
      }

    tl->expired = true;
    tl->close();

    return NULL;
  }

  template < class T >
  void TimeLimitedComputation< T >::close()
  {
    pthread_mutex_lock(&lock);

    closed = true;
    pthread_cond_signal(&closedCond);

    pthread_mutex_unlock(&lock);
  }

  template < class T >
  void TimeLimitedComputation< T >::waitClosed() const
  {
    pthread_mutex_lock(&lock);

      if (!closed)
	{
	  pthread_cond_wait(&closedCond, &lock);
	}

    pthread_mutex_unlock(&lock);
  }

  template < class T >
  bool TimeLimitedComputation< T >::timeExpired() const
  {
    return expired;
  }

  template < class T >
  bool TimeLimitedComputation< T >::checkFinished() const
  {
    return finished;
  }
};
