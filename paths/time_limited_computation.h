#ifndef __TIME_LIMITED_COMPUTATION_H__
#define __TIME_LIMITED_COMPUTATION_H__

#include <pthread.h>

namespace std
{
  class TimeLimitExceededException
  {
  };

  /**
   * A computation with a time limit.  If the computation finishes
   * within the time limit then the result is valid.  Otherwise,
   * the result is set to some flag determined by the subclass.
   *
   * @param T the result type; requires zero-argument- and copy- constructors
   */

  template < class T >
    class TimeLimitedComputation
    {
    public:
      TimeLimitedComputation(int time);
      virtual ~TimeLimitedComputation() {}
      T run();
      virtual T compute() = 0;
      virtual T timeoutValue() = 0;
      bool timeExpired() const;
      void finish();
      bool checkFinished() const;
      void waitClosed() const;

    protected:
      void close();
      static void *startTimer(void *a);

    private:
      bool expired;
      bool finished;
      bool closed;
      int timeLimit;
      T result;
      pthread_t timerThread;
      pthread_t computeThread;
      mutable pthread_mutex_t lock;
      mutable pthread_cond_t closedCond;

      static pthread_mutex_t DEFAULT_MUTEX;
      static pthread_cond_t DEFAULT_COND;
    };

};
#endif
