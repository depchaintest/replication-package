/* misc.h */

/* copyright (c) The Authors 1993 */

#define printd      if (DEBUG) printf

#ifndef _MISC
#define _MISC

#define TRUE 1
#define FALSE 0
#define BOOLEAN int
#define EQUAL 0

#ifndef NULL
#define NULL 0
#endif

#define abort abort_was_renamed_abort1

#include <stdio.h>

#include "pdg.h"

void abort1(char *);
void *check_malloc(int);
void *check_realloc(void *, int);
void readln();
void warning(const char *, const char *);
void print_memory_used();
int __isoc99_fscanf(FILE *stream, const char *format, ...);
vertex_kind string_to_kind(char *s);
void read_fails(char *format, int cnt);

#endif

