#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <map>
#include <set>
#include <string>

#include "pd_graph.h"

extern "C"
{
#include "pdg.h"
#include "ht.h"

extern int scc_break_depth;
extern int estimation_trials;
}

#include "graph.cpp"
#include "scc_path_counter.cpp"
#include "graph_utils.cpp"

namespace std
{

#define edge_type_to_cstring(i) ((i->useControl && i->useData) ? "BO" : (i->useControl ? "CO" : (i->useData ? "DO" : "NO")))

#define CHARPOS_OUTPUT_WIDTH 8

typedef struct
{
  int min_mark;
  bool useControl;
  bool useData;
} findReachableInfo;

  /**
   * Outputs a PDG vertex to the given output stream.
   *
   * @param os an output stream
   * @param v a pointer to a vertex
   * @return os
   */
  
  ostream& operator<<(ostream& os, const tPdgVertex v)
  {
    return os << hex << v->name << dec
	      << setw(CHARPOS_OUTPUT_WIDTH) << v->charpos_min
	      << setw(CHARPOS_OUTPUT_WIDTH) << v->charpos_max
	      << " " << setw(32) << left << v->file 
	      << " " << setw(32) << (v->my_entry ? v->my_entry->src : "???") << right;
    
  }

  void output_csv(ostream& os, const tPdgVertex v)
  {
    os << hex << v->name << dec << ","
       << v->charpos_min << ","
       << v->charpos_max << ","
       << "\"" << v->file << "\","
       << "\"" << (v->my_entry ? v->my_entry->src : "???") << "\",";
  }

/**
 * Performs a DFS starting from the given vertex in a PDG graph.
 * The behavior of the search is determined by the functions passed to it.
 * The mechanism by which vertices are marked as visited must be implemented
 * in those functions.
 *
 * @param getEdgeList a function that returns the first element of the
 * array of edges to follow from the starting vertex
 * @param shouldFollow a function that determines whether a particular edge
 * should be followed in the DFS
 * @param visit the function that performs a visit of vertices
 */
static void pdgDFS(tPdgVertex v, tPdgVertex *(*getEdgeList)(tPdgVertex v), bool (*shouldFollow)(tPdgVertex v, tPdgVertex *edge, void* args), void (*visit)(tPdgVertex v, void *args), void *args);

/**
 * Returns the first edge in the given vertex's incoming edge list.
 * Intended for use in conjunction with pdgDFS.
 */
static tPdgVertex *getIncomingEdgeList(tPdgVertex v);

/**
 * Determines if the given edge from the given vertex is to a vertex
 * that has not been marked yet and is not a declaration.  Intended for
 * use in conjunction with pdgDFS.
 *
 * @param v the source of the edge
 * @param edge the edge to consider following
 * @param a a pointer to a findReachableStruct that contains the lowest
 * mark of visited vertices
 * @return true iff the edge should be followed
 */
static bool notDeclaration(tPdgVertex v, tPdgVertex *edge, void *a);

/**
 * Marks the given vertex as visited.  The marked value is determined by
 * the global current_mark, which is incremented as a side effect.
 *
 * @param v the vertex to mark
 * @param a ignored
 */
static void markVertex(tPdgVertex v, void *a);

/**
 * Builds (and currently analyzes) a C++ graph out of the given sdg.
 * Edges are selected according to the useData and useControl fields
 * in the given info struct.  The info struct also contains a field
 * that can be used to keep track of the minimum mark on reachable
 * vertices.
 */
static void buildPDGraph(tHashTable sdg, findReachableInfo *info);

static void writeSCCDOTFiles(const DirectedGraph< tPdgVertex >& g, const tr1::unordered_map< tPdgVertex, int >& sccs, int sccCount, const string& filePrefix);

/**
 * Finds unmarked the source vertices that are reachable by following edges
 * backwards from the given vertex.  If the vertex is not a source vertex
 * or is a declaration vertex or has already been marked then there is
 * no effect.
 */
void findReachable(tPdgVertex from, void *a)
{
  findReachableInfo *args = (findReachableInfo *)a;

  // from find_path_start -- ignore non-source vertices
  if ((pdg_vertex_flags(from) & FROM_LIBC) || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

  if (from->kind == DECLARATION)
    {
      return;
    }

  if (!control_point(from->kind))
    {
      return;
    }

  // find vertices reachable from this one if it hasn't been visited yet
  if (from->mark <= args->min_mark)
    {
      //std::cout << "DFS START: " << hex << from->name << std::endl;
      pdgDFS(from, getIncomingEdgeList, notDeclaration, markVertex, args);
    }
}

/**
 * Adds incoming edges from marked vertices to the given vertex to the
 * given graph.  If the given vertex is not marked itself then no edges
 * are added.  The edges are reveresed as they are added to the new
 * graph: if u->v is marked in the PDG graph then the edge added to
 * the new graph is v->u.
 *
 * @param to the vertex whose incoming edges are to be added
 * @param g the graph to add edges to
 * @param args information about the DFS that marked the vertices; contains
 * the lowest mark of relevant vertices
 */
void addEdges(tPdgVertex to, DirectedGraph< tPdgVertex >* g, findReachableInfo *args)
{
  // check that from was visited when we marked all relevant vertices
  if (to->mark <= args->min_mark)
    {
      return;
    }

  // go over incoming edges and add those to other marked vertices
  tPdgVertex *edge = to->incoming_intra_edges;
  while (edge != NULL && *edge != NULL)
    {
      if ((control_edge(*edge) && args->useControl)
	  || (data_edge(*edge) && args->useData))
	{
	  tPdgVertex from = strip_edge_kind(*edge);
	  
	  if (from->mark > args->min_mark)
	    {
	      g->addEdge(to, from);
	    }
	}
      edge++;
    }
}

void pdgDFS(tPdgVertex v, tPdgVertex *(*getEdgeList)(tPdgVertex v), bool (*shouldFollow)(tPdgVertex v, tPdgVertex *edge, void* args), void (*visit)(tPdgVertex v, void *args), void *args)
{
  visit(v, args);
  
  tPdgVertex *edge = getEdgeList(v);
  while (edge != NULL && *edge != NULL)
    {
      if (shouldFollow(v, edge, args))
	{
	  tPdgVertex neighbor = strip_edge_kind(*edge);
	  pdgDFS(neighbor, getEdgeList, shouldFollow, visit, args);
	}
      edge++;
    }
}
	 
tPdgVertex *getIncomingEdgeList(tPdgVertex v)
{
  return v->incoming_intra_edges;
}

bool notDeclaration(tPdgVertex v, tPdgVertex *edge, void *a)
{
  findReachableInfo *args = (findReachableInfo *)a;

  // compare control/data to what we're supposed to follow
  if ((control_edge(*edge) && args->useControl)
      || (data_edge(*edge) && args->useData))
    {
      // determine if destination is a declaration or has been visited
      tPdgVertex dest = strip_edge_kind(*edge);
      bool follow = (dest->mark <= args->min_mark && dest->kind != DECLARATION);

      /*
      if (follow)
	{
	  std::cout << "DFS FOLLOW: " << hex << v->name << "->" << dest->name << std::endl;
	}
      */

      return follow;
    }
  else
    {
      return false;
    }
}

void markVertex(tPdgVertex v, void *a)
{
  // findReachableInfo *args = (findReachableInfo *)a;

  v->mark = current_mark++;
}

/**
 * Adds the given vertex to the given set if it is a predicate.
 */

void addIfKind(tPdgVertex v, tr1::unordered_set< tPdgVertex >* s, bool (*choose)(tPdgVertex))
{
  if (choose(v))
    {
      s->insert(v);
    }
}

extern "C"
{
  bool isPredicate(tPdgVertex v)
  {
    return control_point(v->kind);
  }
  
  bool isFormalIn(tPdgVertex v)
  {
    return formal_in(v->kind); // includes FORMAL_IN and GLOBAL_FORMAL_IN
  }
  
  bool isEntry(tPdgVertex v)
  {
    return entry(v->kind);
  }
  
  bool isBody(tPdgVertex v)
  {
    return ((v->kind & BODY) != 0);
  }
}


extern "C" void process_pd_graph(tHashTable sdg)
{
  // note that marks are cleared by calling foreach with init from main

  findReachableInfo *info = new findReachableInfo;

  // use both kinds of edges
  cout << "BOTH" << endl;
  info->useData = true;
  info->useControl = true;
  buildPDGraph(sdg, info);

  // use only control edges
  cout << "CONTROL ONLY" << endl;
  info->useData = false;
  buildPDGraph(sdg, info);
  
  // use only data edges
  cout << "DATA ONLY" << endl;
  info->useData = true;
  info->useControl = false;
  buildPDGraph(sdg, info);

  delete info;
}

template< class T >
void outputStatistics(findReachableInfo *info, tPdgVertex source, tPdgVertex exit, const char *type, const PathStatistics<T>& count)
{
  outputFixed(info, source, exit, type, count);
  outputCSV(info, source, exit, type, count);
}

template< class T >
void outputFixed(findReachableInfo *info, tPdgVertex source, tPdgVertex exit, const char *type, const PathStatistics<T>& count)
{
	  cout << "PATH-STATISTICS "
	       << edge_type_to_cstring(info) << " "
	       << source
	       << " "
	       << (exit != NULL && isEntry(exit) ? "ENTRY " : "")
	       << (exit != NULL && isFormalIn(exit) ? "FORML " : "")
	       << (exit != NULL && isBody(exit) ? "BODY  " : "")
	       << (type != NULL ? type : "")
	       << count
	       << " @@@ "
	       << source->src;
	  
	  if (exit != NULL && isFormalIn(exit))
	    {
	      cout << " %%% "
		   << hex << exit->name << dec
		   << " " << exit->src;
	    }
	  
	  cout << endl;
}
  
template< class T >
void outputCSV(findReachableInfo *info, tPdgVertex source, tPdgVertex exit, const char *type, const PathStatistics<T>& count)
{
  cout << "PATH-CSV,"
       << edge_type_to_cstring(info) << ",";
  output_csv(cout, source);
  cout << (exit != NULL && isEntry(exit) ? "ENTRY" : "")
       << (exit != NULL && isFormalIn(exit) ? "FORML" : "")
       << (exit != NULL && isBody(exit) ? "BODY" : "")
       << (type != NULL ? type : "")
       << ","
       << (count.isEstimated() ? "Y," : "N,")
       << count.getMinimum() << ","
       << count.getMaximum() << ","
       << (count.getCount() < 10000000 ? fixed : scientific)
       << (count.getCount() < 10000000 ? setprecision(0) : setprecision(2))
       << count.getCount() << ","
       << setprecision(3) << count.getAverage() << ","
       << "\"" << source->src << "\"";
  
  if (exit != NULL && isFormalIn(exit))
    {
      cout << "," << hex << exit->name << dec << ","
	   << "\"" << exit->src << "\"";
    }
  
  cout << endl;
}

void buildPDGraph(tHashTable sdg, findReachableInfo *info)
{
  // find which vertices are reachable from conditionals by marking
  // reachable vertices with a counter; vertices with a mark > this counter
  // were reachable
  info->min_mark = current_mark++;
  ht_foreach1(sdg, (FN2ARG)findReachable, info);

  // build the graph using edges between marked vertices
  DirectedGraph< tPdgVertex > g;

  ht_foreach2(sdg, (FN3ARG)addEdges, (void *)&g, (void *)info);

  // find the vertices we're interested in (predicates [control points])
  tr1::unordered_set< tPdgVertex > entryPoints;
  tr1::unordered_set< tPdgVertex > exitPoints;
  for (vector< tPdgVertex >::const_iterator i = g.vertexList().begin();
       i != g.vertexList().end();
       i++)
    {
      addIfKind(*i, &entryPoints, isPredicate);

      addIfKind(*i, &exitPoints, isEntry);
      addIfKind(*i, &exitPoints, isFormalIn);
      addIfKind(*i, &exitPoints, isBody);
    }

  cout << entryPoints.size() << " predicates" << endl;
  cout << entryPoints.size() << " entry/formal-in/body" << endl;

  // count paths

  int deepest = 0;
  tr1::unordered_map< pair< tPdgVertex, tPdgVertex >, PathStatistics< tPdgVertex >, pairhash > counts = countPaths(g, entryPoints, exitPoints, scc_break_depth, deepest);
  double max = 0.0;
  for (tr1::unordered_map< pair< tPdgVertex, tPdgVertex >, PathStatistics< tPdgVertex >, pairhash >::iterator i = counts.begin();
       i != counts.end();
       i++)
    {
      if ((*i).second.getCount() > max) max = (*i).second.getCount();
    }
  std::cout << "SCC-DEPTH: " << g.vertexList().size() << " " << deepest << " " << max << std::endl;

  // accumulators for each vertex
  tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > formalInTotals;
  tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > allVerticesTotals;

  for (tr1::unordered_map< pair< tPdgVertex, tPdgVertex >, PathStatistics< tPdgVertex >, pairhash >::iterator i = counts.begin(); i != counts.end(); i++)
    {
      tPdgVertex source = (*i).first.first;
      tPdgVertex exit = (*i).first.second;
      PathStatistics< tPdgVertex > count = (*i).second;

      if ((isEntry(exit) || isFormalIn(exit) || isBody(exit)))
	{
	  outputStatistics(info, source, exit, NULL, count);
	  
	  if (isFormalIn(exit) || isEntry(exit)) // _not_ BODY
	    {
	      if (allVerticesTotals.count(source) == 0)
		{
		  allVerticesTotals[source] = count;
		}
	      else
		{
		  allVerticesTotals[source] = allVerticesTotals[source] + count;
		}
	    }
	  
	  if (isFormalIn(exit))
	    {
	      if (formalInTotals.count(source) == 0)
		{
		  formalInTotals[source] = count;
		}
	      else
		{
		  formalInTotals[source] = formalInTotals[source] + count;
		}
	    }
	}
    }

  // output path statistics for all formal inputs
  for (tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > >::iterator i = formalInTotals.begin();
       i != formalInTotals.end();
       i++)
    {
      tPdgVertex source = (*i).first;

      outputStatistics(info, source, NULL, "ALLFI", formalInTotals[source]);
    }


  // output path statistics for all vertices
  for (tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > >::iterator i = allVerticesTotals.begin();
       i != allVerticesTotals.end();
       i++)
    {
      tPdgVertex source = (*i).first;

      outputStatistics(info, source, NULL, "ALLVX", allVerticesTotals[source]);
    }
}

/*
void buildPDGraph(tHashTable sdg, findReachableInfo *info)
{
  // find which vertices are reachable from conditionals by marking
  // reachable vertices with a counter; vertices with a mark > this counter
  // were reachable
  info->min_mark = current_mark++;
  ht_foreach1(sdg, (FN2ARG)findReachable, info);

  // build the graph using edges between marked vertices
  DirectedGraph< tPdgVertex > g;

  ht_foreach2(sdg, (FN3ARG)addEdges, (void *)&g, (void *)info);

  // count SCCs (and output vertex -> SCC map)
  tr1::unordered_map< tPdgVertex, int > sccs = g.findSCCs();
  int sccCount = 0;
  for (tr1::unordered_map< tPdgVertex, int >::iterator i = sccs.begin(); i != sccs.end(); i++)
    {
      // is vertex's SCC index larger than the largest SCC index seen so far?
      if ((*i).second >= sccCount)
	{
	  sccCount = (*i).second + 1;
	}

      cout << "SCC: "
	   << hex << ((*i).first)->name
	   << " " << setw(5) << dec << (*i).second
	   << " " << ((*i).first)->src
	   << endl;
    }

  cout << sccCount << " SCCs" << endl;

  if (1 == 2)
    {
      cout << "Writing DOT files" << endl;
      writeSCCDOTFiles(g, sccs, sccCount, string(edge_type_to_cstring(info)));
    }

  // find the SCC entry points we're interested in (predicates
  // [control points])
  tr1::unordered_set< tPdgVertex > entryPoints;
  tr1::unordered_set< tPdgVertex > exitPoints;
  for (vector< tPdgVertex >::const_iterator i = g.vertexList().begin();
       i != g.vertexList().end();
       i++)
    {
      addIfKind(*i, &entryPoints, isPredicate);
      addIfSCCEntry(*i, &entryPoints, &g, &sccs);

      addIfKind(*i, &exitPoints, isEntry);
      addIfKind(*i, &exitPoints, isFormalIn);
      addIfKind(*i, &exitPoints, isBody);
      addIfSCCExit(*i, &exitPoints, &g, &sccs);
    }

  cout << entryPoints.size() << " entry points" << endl;
  cout << entryPoints.size() << " exit points" << endl;

  // count paths in SCCs from each entry point
  tr1::unordered_map< tPdgVertex, tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > > sccStats;
  for (tr1::unordered_set< tPdgVertex >::iterator i = entryPoints.begin();
       i != entryPoints.end();
       i++)
    {
      cout << "SOURCE " << hex << (*i)->name << dec
	   << " " << (*i)->src
	   << endl;

      SCCPathCounter< tPdgVertex > counter(g,
					   *i,
					   exitPoints,
					   sccs,
					   scc_processing_timeout);

      const tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > >& stats = counter.run();
      counter.waitClosed();

      for (tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > >::const_iterator j = stats.begin();
	   j != stats.end();
	   j++)
	{
	  tPdgVertex dest = (*j).first;
	  PathStatistics< tPdgVertex > s = (*j).second;

	  cout << hex << dest->name << dec
	       << " " << dest->src
	       << " " << s
	       << endl;
	}

      sccStats[*i] = stats;
    }

  // invert scc map so we have sccEntries[i] = list of vertices in SCC i
  vector< vector< tPdgVertex > > sccEntryPoints;
  for (int c = 0; c < sccCount; c++)
    {
      sccEntryPoints.push_back(vector< tPdgVertex >());
    }
  
  // we now have empty vectors for each SCC; populate them
  for (tr1::unordered_set< tPdgVertex >::iterator i = entryPoints.begin();
       i != entryPoints.end();
       i++)
    {
      tr1::unordered_map< tPdgVertex, int >::iterator entry = sccs.find(*i);
      int component = (*entry).second;
      sccEntryPoints[component].push_back(*i);
    }

  // do the same for the exit points
  vector< vector< tPdgVertex > > sccExitPoints;
  for (int c = 0; c < sccCount; c++)
    {
      sccExitPoints.push_back(vector< tPdgVertex >());
    }
  
  for (tr1::unordered_set< tPdgVertex >::iterator i = exitPoints.begin();
       i != exitPoints.end();
       i++)
    {
      tr1::unordered_map< tPdgVertex, int >::iterator entry = sccs.find(*i);
      int component = (*entry).second;
      sccExitPoints[component].push_back(*i);
    }
  
  // output the reversed maps
  for (int c = 0; c < sccCount; c++)
    {
      cout << "COMPONENT " << c 
	   << endl
	   << " ENTRY POINTS" << endl;
      for (unsigned int i = 0; i < sccEntryPoints[c].size(); i++)
	{
	  cout << hex << sccEntryPoints[c][i]->name
	       << " " << dec << sccEntryPoints[c][i]->src
	       << endl;
	}

      cout << "EXIT POINTS" << endl;
      for (unsigned int i = 0; i < sccExitPoints[c].size(); i++)
	{
	  cout << hex << sccExitPoints[c][i]->name
	       << " " << dec << sccExitPoints[c][i]->src
	       << endl;
	}
    }

  // for each entry point that is a control point, compute path statistics 
  // starting with that point
  for (tr1::unordered_set< tPdgVertex >::iterator i = entryPoints.begin();
       i != entryPoints.end(); 
       i++)
    {
      tPdgVertex source = (*i);

      if (control_point(source->kind))
	{
	  //cout << "SOURCE " << hex << source->name << dec << " " << source->src
	  //     << " IN SCC " << (*(sccs.find(source))).second
	  //     << endl;
	  
	  
	  // start with all entry and exit points having 0 stats...
	  tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > entryStats;
	  tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > exitStats;

	  for (tr1::unordered_set< tPdgVertex >::iterator i = entryPoints.begin();
	       i != entryPoints.end();
	       i++)
	    {
	      entryStats[*i] = PathStatistics< tPdgVertex >();
	    }

	  for (tr1::unordered_set< tPdgVertex >::iterator i = exitPoints.begin();
	       i != exitPoints.end();
	       i++)
	    {
	      exitStats[*i] = PathStatistics< tPdgVertex >();
	    }
	  
	  // ...except for entry half of the place we're starting from
	  entryStats[source] = PathStatistics< tPdgVertex >::empty();
	  
	  for (int c = 0; c < sccCount; c++)
	    {
	      for (unsigned int i = 0; i < sccEntryPoints[c].size(); i++)
		{
		  tPdgVertex entry = sccEntryPoints[c][i];
		  
		  if (entryStats[entry].getCount() == 0)
		    continue;

		  // cout << "entry " << hex << entry->name << dec << " " << entry->src << endl;

		  tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > > outgoing
		    = sccStats[entry];
		  for (tr1::unordered_map< tPdgVertex, PathStatistics< tPdgVertex > >::iterator j = outgoing.begin();
		       j != outgoing.end();
		       j++)
		    {
		      tPdgVertex exit = (*j).first;
		      PathStatistics< tPdgVertex > w = (*j).second;

		      exitStats[exit] = exitStats[exit] + w * entryStats[entry];
		      // cout << "updated exit " << hex << exit->name << dec << " to " << exitStats[exit] << endl;
		    }
		}

	      for (unsigned int i = 0; i < sccExitPoints[c].size(); i++)
		{
		  tPdgVertex exit = sccExitPoints[c][i];
		  vector< tPdgVertex > edges = g.adjacentFrom(exit);

		  if (exitStats[exit].getCount() == 0)
		    continue;

		  // cout << "exit " << hex << exit->name << dec << " " << exit->src << endl;

		  for (vector< tPdgVertex >::iterator j = edges.begin();
		       j != edges.end();
		       j++)
		    {
		      tPdgVertex dest = *j;
		      int destSCC = (*(sccs.find(dest))).second;
		      if (destSCC != c)
			{
			  entryStats[dest] = entryStats[dest] + exitStats[exit] * PathStatistics< tPdgVertex >::one();
			  // cout << "updated entry " << hex << dest->name << dec << " to " << entryStats[dest] << endl;

			  }
			  }
			  }
			  }
			  
	  // output the results for exit points of interest
	  PathStatistics< tPdgVertex > formalInTotal;
	  PathStatistics< tPdgVertex > allVerticesTotal;
	  for (tr1::unordered_set< tPdgVertex >::iterator i = exitPoints.begin();
	       i != exitPoints.end();
	       i++)
	       {
	       tPdgVertex exit = *i;
	       if ((isEntry(exit) || isFormalIn(exit) || isBody(exit))
	       && exitStats[exit].getCount() > 0)
	       {
	       
	       outputStatistics(info, source, exit, NULL, exitStats[exit]);
	       
	      if (isFormalIn(exit) || isEntry(exit)) // _not_ BODY
	      {
	      allVerticesTotal = allVerticesTotal + exitStats[exit];
	      }
	      
	      if (isFormalIn(exit))
	      {
	      formalInTotal = formalInTotal + exitStats[exit];
	      }
	      }
	      }
	      
	  // output path statistics for all formal inputs
	  if (formalInTotal.getCount() > 0)
	    {
	    outputStatistics(info, source, NULL, "ALLFI", formalInTotal);
	    }

	  // output path statistics for all formal inputs
	  if (allVerticesTotal.getCount() > 0)
	  {
	  outputStatistics(info, source, NULL, "ALLVX", allVerticesTotal);
	  }
	}
    }
}
*/

void writeSCCDOTFiles(const DirectedGraph< tPdgVertex >& g, const tr1::unordered_map< tPdgVertex, int >& sccs, int sccCount, const string& filePrefix)
{
  // invert the SCC map so that scc[i] is list of vertices in SCC i
  vector< vector< tPdgVertex > > sccList;
  for (int c = 0; c < sccCount; c++)
    {
      sccList.push_back(vector< tPdgVertex >());
    }
  for (tr1::unordered_map< tPdgVertex, int >::const_iterator i = sccs.begin();
       i != sccs.end();
       i++)
    {
      sccList[(*i).second].push_back((*i).first);
    }
  
  for (int c = 0; c < sccCount; c++)
    {
      stringstream filename;
      filename << filePrefix << "_scc_" << c << ".gv" << ends;
      ofstream out(filename.str().c_str());

      out << "digraph scc" << c << endl
	  << "{" << endl;

      for (vector< tPdgVertex >::iterator i = sccList[c].begin();
	   i != sccList[c].end();
	   i++)
	{
	  tPdgVertex from = *i;
	  int fromComponent = c;

	  vector< tPdgVertex > neighbors = g.adjacentFrom(from);
	  for (vector< tPdgVertex >::iterator j = neighbors.begin();
	       j != neighbors.end();
	       j++)
	    {
	      tPdgVertex to = *j;
	      int toComponent = (*(sccs.find(to))).second;
	      
	      if (fromComponent == toComponent)
		{
		  out << "\t" << hex << "\"" << from->name << "\""
		      << " -> \"" << to->name << "\"" << dec
		      << ";" << endl;
		}
	    }
	}
      
      out << "}" << endl;
      out.close();
    }
}

}
