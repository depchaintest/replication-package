#ifndef __SCCS_H__
#define __SCCS_H__

#include "pdg.h"

#ifndef BOOL
#define BOOL int
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

extern int current_mark;

extern int min_scc_to_process;
extern int max_scc_to_process;
extern int scc_processing_timeout;

typedef struct sSccGraph
{
  tPdgVertex source;

  int min_mark; // current_mark when 1st DFS (to find reachable) was started
  int min_scc_mark; // current_mark when 2nd DFS (to find SCCs) was started

  tPdgVertex *reachable;
  int reachable_size;
  int reachable_capacity;

  tPdgVertex **sccs;
  int *scc_size;
  int *scc_capacity;
  int sccs_size;
  int sccs_capacity;

  tPdgVertex **entry_points;
  int *entry_size;
  int *entry_capacity;

  tPdgVertex **exit_points;
  int *exit_size;
  int *exit_capacity;

} *tSccGraph;

tSccGraph create_scc_graph(tPdgVertex s, BOOL (*selector)(tPdgVertex v));
void destroy_scc_graph(tSccGraph g);
double timed_estimate_average_path_length(tSccGraph g, BOOL (*selector)(tPdgVertex v), int timeout_sec);
double estimate_average_path_length(tSccGraph g, BOOL (*selector)(tPdgVertex v));
BOOL selector_all(tPdgVertex v);
BOOL selector_no_decls(tPdgVertex v);

#endif
