static int debug = 0;

#define DEBUG 0

#include "stdio.h"
#include "path.h"
#include "ht.h"
#include "misc.h"


int current_mark = 0;
static int slice_overlap = 0;  // overlap of current slice with current_mark-1 slice
static tList aouts, ains; 
static int _count_what = 0;
// 0 = all vertices else if it's a kind overlaps with this mask

static int count_in_overlap(tPdgVertex v)
{
    return !_count_what || (v->kind & _count_what);
    // return !_count_what || formal_in(v->kind);     // lumps fin and gin together
}


static int pdg_size = 0;

static void _reset_counts(tPdgVertex v, int values[9])
{
    if (v->mark != current_mark)
    {
        v->mark = current_mark;
//pdg_size++; // vertex count
        int i;
        for(i=0; i<9; i++)
            v->counts[i] = values[i];

        tPdgVertex *edges = v->outgoing_intra_edges;

        if (edges != NULL)
        {
            while (strip_edge_kind(*edges) != NULL)
            {
//pdg_size++; // edge count
                if (control_edge(*edges))
                    _reset_counts(strip_edge_kind(*edges), values);
                edges++;
            }
        }
    }
}


void reset_counts(tPdgVertex entry, int values[9])
{
pdg_size = 0;
    current_mark++;
    _reset_counts(entry, values);
//printf("pdg size = %d\n", pdg_size);
}

#define ave(v,x) (v->counts[(x)] <= 0 ? -9.99 : (((double)(v->counts[(x)+3]))/((double)(v->counts[(x)]))))
// average for loopheaded vertices
#define avelh(v,x) (v->counts[(x)] <= 0 ? -9.99 : (((double)(v->counts[(x)+6]))/((double)(v->counts[(x)]))))

static void print_aves(tPdgVertex v)
{
    if (v->counts[CO] <= 0 && v->counts[BOTH] <= 0 && v->counts[DO] <= 0)
        return;

    pdg_vertex_print_short(v);
    printf("both <%1.0lf,%1.0lf> %3.2f   ", v->counts[BOTH], v->counts[BOTH+3], ave(v, BOTH));
    printf("do <%1.0lf,%1.0lf> %3.2f   ", v->counts[DO], v->counts[DO+3], ave(v, DO));
    printf("co <%1.0lf,%1.0lf> %3.2f", v->counts[CO], v->counts[CO+3], ave(v, CO));
    //printf("   [loop-headed-count: CO %1.0lf DO %1.0lf both %1.0lf]", v->counts[CO+6], v->counts[DO+6], v->counts[BOTH+6]);
    //printf("   [loop-headed: "); 
    printf("   ["); 
    printf("both <%1.0lf,%1.0lf> %3.2f   ", v->counts[BOTH], v->counts[BOTH+6], avelh(v, BOTH));
    printf("do <%1.0lf,%1.0lf> %3.2f   ", v->counts[DO], v->counts[DO+6], avelh(v, DO));
    printf("co <%1.0lf,%1.0lf> %3.2f", v->counts[CO], v->counts[CO+6], avelh(v, CO));
    printf("]");
    printf("\n"); 
}


// formals all reachable on single edges from entry
void print_best_averages(tPdgVertex it)
{
    int i;
    tPdgVertex entry = it->my_entry;
    //printf("bests for "); pdg_vertex_print_short(it); printf("\n"); 
    print_aves(entry);

    struct sPdgVertex all;
    all.name = 0x12345678;
    all.src = "all vertices";
    for(i=0; i<9; i++)
        all.counts[i] = entry->counts[i];

    struct sPdgVertex formals;
    formals.name = 0x87654321;
    formals.src = "all formals";
    for(i=0; i<9; i++)
        formals.counts[i] = 0;

    tPdgVertex *edges = entry->outgoing_intra_edges;
    if (edges != NULL)
    {
        for( ;strip_edge_kind(*edges) != NULL; edges++)
        {
            tPdgVertex v = strip_edge_kind(*edges);
            if (formal_in(v->kind))
                print_aves(v);

            for(i=0; i<9; i++)
            {
                formals.counts[i] += v->counts[i];
                all.counts[i] += v->counts[i];
            }
            v->last_average = 0;
        }
    }
    print_aves(&formals);
    print_aves(&all);
    it->last_average = 0;
    entry->last_average = 0;
}


// formals all reachable on single edges from entry
void print_bests(tPdgVertex it)
{
        tPdgVertex entry = it->my_entry;
        //printf("bests for "); pdg_vertex_print_short(it);
        pdg_vertex_print_short(entry);
        printf("CO %1.0lf  DO %1.0lf  both %1.0lf  ", entry->counts[CO], entry->counts[DO], entry->counts[BOTH]);
        printf("[CO %1.0lf  DO %1.0lf  both %1.0lf]\n", entry->counts[CO+3], entry->counts[DO+3], entry->counts[BOTH+3]);


        tPdgVertex *edges = entry->outgoing_intra_edges;
        if (edges != NULL)
        {
            while (strip_edge_kind(*edges) != NULL)
            {
                tPdgVertex v = strip_edge_kind(*edges);
                if (control_edge(*edges) && formal_in(v->kind)
                   && (v->counts[CO] != -1 || v->counts[DO] != -1 ||  v->counts[BOTH] != -1)) 
                {

                    pdg_vertex_print_short(v);
                    printf("CO %1.0lf  DO %1.0lf  both %1.0lf ", v->counts[CO], v->counts[DO], v->counts[BOTH]);
                    printf("[CO %1.0lf  DO %1.0lf  both %1.0lf]\n", v->counts[CO+3], v->counts[DO+3], v->counts[BOTH+3]);
                }

                edges++;
            }
        }
}



void f1_control_flood(tPdgVertex v)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->outgoing_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;

        v->mark = current_mark;

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                if (control_edge(*edges))
                    f1_control_flood(strip_edge_kind(*edges));
                edges++;
            }

        if (call_site(kind))
            list_insert_beginning(ains, v);
    }
}


void f2_control_flood(tPdgVertex v)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->outgoing_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;
        v->mark = current_mark;

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                if (control_edge(*edges))
                    f2_control_flood(strip_edge_kind(*edges));
                edges++;
            }

        if (call_site(kind))
        {
            edges = v->outgoing_inter_edges;
            if (edges != NULL) 
                while (strip_edge_kind(*edges) != NULL)
                {
                    if (control_edge(*edges))
                        f2_control_flood(strip_edge_kind(*edges));
                    edges++;
                }
        }
    }
}


void start_f2_control(tPdgVertex v)
{
    v->mark = 0;  // Force revisit
    //if (!((pdg_vertex_flags(v) & FROM_LIBC) || (pdg_vertex_flags(v) & NO_SOURCE)) && count_in_overlap(v))
        //slice_overlap--;
    f2_control_flood(v);
}


void f_control_slice(tPdgVertex from)
{
    if ((pdg_vertex_flags(from) & FROM_LIBC) ) // || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    ains = list_initialize();
    slice_overlap = 0;
    current_mark++;
    f1_control_flood(from);
    list_foreach(ains, (FN1ARG)start_f2_control); 
    list_free(ains); 
    //printf("size of slice on %x = %d\n", from->name, slice_overlap);
}
















void f1_flood(tPdgVertex v, int do_interprocedural)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->outgoing_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;
        v->mark = current_mark;
        if (DEBUG) printf("  f1 %x\n", v->name);

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                f1_flood(strip_edge_kind(*edges), do_interprocedural);
                edges++;
            }

        if (do_interprocedural && formal_out(kind))
        {
            edges = v-> outgoing_inter_edges;
            if (edges != NULL) 
                while (strip_edge_kind(*edges) != NULL)
                {
                    f1_flood(strip_edge_kind(*edges), do_interprocedural);
                    edges++;
                }
        }

        if (actual_in(kind) || call_site(kind))
            list_insert_beginning(ains, v);
    }
}


void f2_flood(tPdgVertex v)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->outgoing_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;
        v->mark = current_mark;
            //slice_overlap++;
        if (DEBUG) printf(" f2 %x\n", v->name);

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                f2_flood(strip_edge_kind(*edges));
                edges++;
            }

        if (actual_in(kind) || call_site(kind))
        {
            edges = v->outgoing_inter_edges;
            if (edges != NULL) 
                while (strip_edge_kind(*edges) != NULL)
                {
                    f2_flood(strip_edge_kind(*edges));
                    edges++;
                }
        }
    }
}


void start_f2(tPdgVertex v)
{
    v->mark = 0;  // Force revisit
    // FIX ME dropped NO_SOURCE test in other places   
    //if (!((pdg_vertex_flags(v) & FROM_LIBC) || (pdg_vertex_flags(v) & NO_SOURCE)) && count_in_overlap(v))
        //slice_overlap--;
    f2_flood(v);
}


void f_slice(tPdgVertex from, long *area, int do_interprocedural, int count_what)

{
    if ((pdg_vertex_flags(from) & FROM_LIBC) 
       || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    _count_what = count_what;
    ains = list_initialize();
    current_mark++;
    slice_overlap = 0;
    f1_flood(from, do_interprocedural);
    list_foreach(ains, (FN1ARG)start_f2); 
    list_free(ains); 
    //printf("size of slice on %x = %d\n", from->name, slice_overlap);
}


void b1_flood(tPdgVertex v, int do_interprocedural)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->incoming_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;
        v->mark = current_mark;
        //if (debug) printf(" b1 flood to %x slice size now %d\n", v->name, slice_overlap);
        if (DEBUG) printf(" %x\n", v->name);

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                b1_flood(strip_edge_kind(*edges), do_interprocedural);
                edges++;
            }

        if (do_interprocedural && (formal_in(kind) || entry(kind)))
        {
            edges = v->incoming_inter_edges;
            if (edges != NULL) 
                while (strip_edge_kind(*edges) != NULL)
                {
                    b1_flood(strip_edge_kind(*edges), do_interprocedural);
                    edges++;
                }
        }

        if (actual_out(kind))
            list_insert_beginning(aouts, v);
    }
}


void b2_flood(tPdgVertex v)
{
    if (v->mark != current_mark)
    {
        int kind = v->kind;// v->edge_length>>24 ; //pdg_vertex_kind(v);
        tPdgVertex *edges = v->incoming_intra_edges;
        if (!((pdg_vertex_flags(v) & FROM_LIBC) )) // || (pdg_vertex_flags(v) & NO_SOURCE)))
            if (v->mark == current_mark - 1 && count_in_overlap(v))
                slice_overlap++;
        v->mark = current_mark;
        // if (debug) printf(" b2 flood to %x slice size now %d\n", v->name, slice_overlap);
        if (DEBUG) printf(" %x\n", v->name);

        if (edges != NULL) 
            while (strip_edge_kind(*edges) != NULL)
            {
                b2_flood(strip_edge_kind(*edges));
                edges++;
            }

        if (actual_out(kind))
        {
            edges = v->incoming_inter_edges;
            if (edges != NULL) 
                while (strip_edge_kind(*edges) != NULL)
                {
                    b2_flood(strip_edge_kind(*edges));
                    edges++;
                }
        }
    }
}


void start_b2(tPdgVertex v)
{
    v->mark = 0;  // Force revisit
    //if (!((pdg_vertex_flags(v) & FROM_LIBC) || (pdg_vertex_flags(v) & NO_SOURCE)) && count_in_overlap(v))
      //slice_overlap--;
    b2_flood(v);
}


void dump_slice(tPdgVertex v)
{
    if (v->mark == current_mark)
        pdg_vertex_print_short(v);
}


void b_slice(tPdgVertex from, long *area, int do_interprocedural, int count_what)
{
    // ignore non-source vertices
    if ((pdg_vertex_flags(from) & FROM_LIBC) 
       ) // || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    if (DEBUG) pdg_vertex_print(from);
    _count_what = count_what;
    aouts = list_initialize();
    current_mark++;
    slice_overlap = 0;
    b1_flood(from, do_interprocedural);
    list_foreach(aouts, (FN1ARG)start_b2); 
    list_free(aouts);
    (*area) += slice_overlap;
    if (debug) printf("size of slice on %x = %d area now %ld\n\n", from->name, slice_overlap, *area);
}


void b2_slice(tPdgVertex from, long *area)
{
    // ignore non-source vertices
    if ((pdg_vertex_flags(from) & FROM_LIBC) 
       ) // || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    if (DEBUG) pdg_vertex_print(from);
    current_mark++;
    slice_overlap = 0;
    b2_flood(from);
    //printf("size of slice on %x = %d\n", from->name, slice_overlap);
    (*area) += slice_overlap;
}

void f2_slice(tPdgVertex from)
{
    // ignore non-source vertices
    if ((pdg_vertex_flags(from) & FROM_LIBC) 
       ) // || (pdg_vertex_flags(from) & NO_SOURCE))
        return;

    if (DEBUG) pdg_vertex_print(from);
    current_mark++;
    slice_overlap = 0;
    f2_flood(from);
    //printf("size of slice on %x = %d\n", from->name, slice_overlap);
}


