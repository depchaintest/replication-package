#ifndef __PD_GRAPH_H__
#define __PD_GRAPH_H__

#ifndef __cplusplus
typedef int bool;
#endif

#ifdef __cplusplus
extern "C"
{
#endif
#include "ht.h"
#include "pdg.h"

  bool isPredicate(tPdgVertex v);
  bool isFormalIn(tPdgVertex v);
  bool isEntry(tPdgVertex v);
  bool isBody(tPdgVertex v);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
extern "C"
#endif
  void process_pd_graph(tHashTable sdg);
 
extern int current_mark;


#endif

