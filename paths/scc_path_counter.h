#ifndef __SCC_PATH_COUNTER_H__
#define __SCC_PATH_COUNTER_H__

#include <tr1/unordered_map>
#include <tr1/unordered_set>

#include "graph.cpp"
#include "path_stats.cpp"
#include "time_limited_computation.cpp"

namespace std
{
  template < class Key >
    class SCCPathCounter : public PathProcessor< Key >, public TimeLimitedComputation< tr1::unordered_map< Key, PathStatistics< Key > > >
  {
  public:
    SCCPathCounter(const DirectedGraph< Key >& graph, const Key& s, const tr1::unordered_set< Key >& exits, const tr1::unordered_map< Key, int >& sccs, int time);
    virtual ~SCCPathCounter() {}

    bool shouldFollow(const Key& from, const Key& to) const;
    const tr1::unordered_map< Key, PathStatistics< Key > >& getPathCounts() const;
    tr1::unordered_map< Key, PathStatistics< Key > > compute();
    tr1::unordered_map< Key, PathStatistics< Key > > timeoutValue(); 

  protected:
    void addToPath(const Key& v);

  private:
    /**
     * The graph to work in.
     */
    const DirectedGraph< Key >& g;

    /**
     * The source vertex to search from.
     */
    Key source;

    /**
     * The component the DFS will stay in.
     */
    int component;

    /**
     * The set of destination vertices to collect statistics for.
     */
    const tr1::unordered_set< Key >& exitPoints;

    /**
     * The map from vertices to the index of the sccs they are in.
     */
    const tr1::unordered_map< Key, int >& sccMap;

    /**
     * A map from exit points to the statistics on paths to them.
     */
    tr1::unordered_map< Key, PathStatistics< Key > > pathCounts;
  };
};

#endif
