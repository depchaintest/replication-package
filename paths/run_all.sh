#!/bin/bash

if [ "$2" == "" ]; then
    echo "USAGE: run_all.sh timeout output-label"
    exit
fi

SDG_PATH=SDGs

TIMEOUT=$1

for SDG in $SDG_PATH/*.sdg.gz; do
    FILE=$(basename $SDG)
    UNCOMPRESSED=${FILE%.*}
    ROOT=${UNCOMPRESSED%.*}

    zcat $SDG | ./slice -path -scc_timeout $TIMEOUT > path_stats/${ROOT}_timeout_${TIMEOUT}_${2}.out

    echo $FILE $ROOT
done
