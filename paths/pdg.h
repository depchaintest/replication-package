/* pdg.h */

#ifndef __PDGH__
#define __PDGH__

#include "ht.h"
#include "list.h"
#include "set.h"

// packed in with kind
#define FROM_LIBC 0x01 << 24
#define NO_SOURCE 0x01 << 25


typedef enum {ENTRY = 0x01 << 0, BODY = 0x01 << 1, FORMAL_IN = 0x01 << 2, 
              FORMAL_OUT = 0x01 << 3, GLOBAL_FORMAL_IN = 0x01 << 4,
              GLOBAL_FORMAL_OUT = 0x01 << 5, CALL_SITE = 0x01 << 6, 
              INDIRECT_CALL = 0x01 << 7, ACTUAL_IN = 0x01 << 8,
              ACTUAL_OUT = 0x01 << 9, GLOBAL_ACTUAL_IN = 0x01 << 10, 
              GLOBAL_ACTUAL_OUT = 0x01 << 11, DECLARATION =  0x01 << 12,
              //EXECUTABLE_CODE, 
              VARIABLE_INITIALIZATION = 0x01 << 13, AUXILIARY = 0x01 << 14,
              CONTROL_POINT = 0x01 << 15, CONTROL_TARGET = 0x01 << 16, 
              EXPRESSION = 0x01 << 17, RETURN_KIND = 0x01 << 18, 
              OTHER = 0x01 << 19,
              UNKNOWN_VERTEX_KIND = 0x01 << 20, ERROR_KIND =  0x01 << 21,
              IF = 0x01 << 22, WHILE = 0x01 << 23
              }
        vertex_kind;

typedef struct sPdgVertex
{
    int  mark;
  // JRG added these
    int  entry_half;
    int  exit_half;
    int  on_path_in_scc; // can I use the existing on_path?
  // JRG's additions end here
    int  kind;
    struct sPdgVertex **incoming_intra_edges;
    struct sPdgVertex **incoming_inter_edges;
    short incoming_intra_edge_count;
    short last_average;
    struct sPdgVertex **outgoing_intra_edges;
    struct sPdgVertex **outgoing_inter_edges;
    int  name;
    char *src; // only for if vertices
  // JRG: changed these to unsigned short (was short)
    unsigned short  charpos_min;
    unsigned short  charpos_max;
    struct sPdgVertex *my_entry;
  char *file; // JRG added this
    double  counts[9];     // control only, data only, both for min and max
                        // pairs of <path count, length sum> for the three for averages
                        // last group of three is length sum filtered to lopp headed edges  
    int on_path;
} *tPdgVertex;
#define PDG_VERTEX_SIZE (sizeof (struct sPdgVertex))


tPdgVertex pdg_vertex_lookup(int name, tHashTable sdg);
void        pdg_vertex_dump_tokens(tPdgVertex v);
void        pdg_vertex_print_short(tPdgVertex v);
void        pdg_vertex_parse_text(tPdgVertex v);
void        pdg_vertex_print(tPdgVertex v);
tPdgVertex  pdg_vertex_read(tHashTable sdg);
int         pdg_vertex_hash(tPdgVertex v);
int         pdg_vertex_compare(tPdgVertex v1, tPdgVertex v2);
void        pdg_vertex_clear_mark(tPdgVertex v);
void        pdg_vertex_clear_all_mark(void *v);
void        pdg_vertex_set_mark(tPdgVertex v, int m);
void        pdg_vertex_no_incoming_edges(tPdgVertex v, tList l, tHashTable sdg);
void        pdg_vertex_set_deleted(tPdgVertex v);
void        pdg_vertex_clear_deleted(tPdgVertex v);
int         pdg_vertex_deleted(tPdgVertex v);
int         pdg_vertex_undeleted(tPdgVertex v);
void        pdg_vertex_print_if(void *x, int (*f)(tPdgVertex v));
void        pdg_vertex_print_short_if(void *x, int (*f)(tPdgVertex v));
int         pdg_vertex_edge_count(tPdgVertex v);
int         pdg_vertex_incoming_inter_edge_count(tPdgVertex v);


#define is_while(k)        ((k) & WHILE)
#define is_if(k)        ((k) & IF)
#define entry(k)        ((k) & ENTRY)
#define formal_in(k)  ((k) & FORMAL_IN || (k) & GLOBAL_FORMAL_IN)
#define formal_out(k) ((k) & FORMAL_OUT || (k) & GLOBAL_FORMAL_OUT)
#define call_site(k)  ((k) & CALL_SITE)
#define actual_in(k)  ((k) & ACTUAL_IN || (k) & GLOBAL_ACTUAL_IN)
#define actual_out(k) ((k) & ACTUAL_OUT || (k) & GLOBAL_ACTUAL_OUT)
#define declaration(k) ((k) & DECLARATION)
// #define executable_code(k) ((k) & EXECUTABLE_CODE)
#define executable_code(k) ((k) & CONTROL_POINT || (k) & CONTROL_TARGET ||\
        (k) & EXPRESSION || (k) & RETURN_KIND)
#define control_point(k) ((k) & CONTROL_POINT)
#define expression(k) ((k) & EXPRESSION)
#define data_source(k) ( (k) & FORMAL_IN || (k) & FORMAL_OUT || (k) & GLOBAL_FORMAL_IN ||  \
(k) & GLOBAL_FORMAL_OUT || (k) & ACTUAL_IN || (k) & ACTUAL_OUT ||  \
(k) & GLOBAL_ACTUAL_IN || (k) & GLOBAL_ACTUAL_OUT || (k) & VARIABLE_INITIALIZATION || \
(k) & EXPRESSION) 



//typedef void (*FN1ARG)(void*) ;
//typedef void (*FN2ARG)(void*, void*) ;
//typedef void (*FN3ARG)(void*, void*, void*) ;

#define pdg_vertex_flags(v) (v->kind)
#define pdg_vertex_kind(v) (v->kind)
#define pdg_vertex_marked(v) (v->mark)


#define data_edge(e) (((long int)e) & 0x01)
#define control_edge(e) (!data_edge(e))
#define strip_edge_kind(e) ((tPdgVertex) ((long int)e & ~0x01))

#endif 


