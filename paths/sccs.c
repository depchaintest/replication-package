#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <sys/time.h>
#include <time.h>
#include <limits.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>

#include "sccs.h"

/*
Structure:

walk_sampled_ave (path.c) -- called from each vertex of interest
  create_scc_graph -- creates the SCC graph for that vertex
    dfs -- used to find reachable vertices and to compute SCCs
    pull_source_and_function -- removes start and end vertices from SCCs
    find_entry_and_exit_points -- finds entry and exit points from SCCs
  estimate_average_path_length -- estimates average path length in that graph
    estimate_path_length_in_scc -- estimates average path length within an SCC
                                   for small SCCs it computes directly
  destroy_scc_graph -- disposes of the SCC graph and its accompanying resourses
*/

#define is_not_decl(v) (strip_edge_kind(v)->kind != DECLARATION)

static void add_reachable(tSccGraph g, tPdgVertex v);
static int is_visited(tSccGraph g, tPdgVertex v);
static int has_assigned_scc(tSccGraph g, tPdgVertex v);
static void add_to_list(tPdgVertex v, tPdgVertex **arr, int *size, int *capacity);
static void resize(tPdgVertex **arr, int *capacity);
static void dfs(tSccGraph g, tPdgVertex v, int mark, int (*is_visited)(tSccGraph, tPdgVertex), tPdgVertex *(*get_edge_list)(tPdgVertex v, BOOL (*selector)(tPdgVertex v), int *), BOOL (*selector)(tPdgVertex v), void (*visit)(tSccGraph, tPdgVertex));
static tPdgVertex *get_forward_edges(tPdgVertex v, BOOL (*selector)(tPdgVertex v), int *count);
static tPdgVertex *get_backward_edges(tPdgVertex v, BOOL (*selector)(tPdgVertex v), int *count);
static void add_empty_scc(tSccGraph g);
static void add_to_last_scc(tSccGraph g, tPdgVertex v);
static int get_scc_index(tSccGraph g, tPdgVertex v);
static void pull_source_and_function(tSccGraph g);
static void find_entry_and_exit_points(tSccGraph g, BOOL (*selector)(tPdgVertex v));
static void print_vertices(tSccGraph g);
static void print_sccs(tSccGraph g);
static void estimate_length_within_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex from, tPdgVertex to, long long *paths, long long *total_length);
static void estimate_path_length_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex), tPdgVertex from, int c, double *n_from_i, double *length_from_i, int half_vertex_count);
static void find_all_paths_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex v, int scc, double *n, double *total_length, int depth, int half_vertex_count);
static void sample_paths_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex source, int c, double *n_from_source, double *length_from_source, int half_vertex_count, int num_samples);
static BOOL sample_paths_between_vertices(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex source, tPdgVertex dest, int c, double *path_count, double *total_length, double recip_prob, int depth);
static long double factorial(int k);
static void random_walk_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex v, int scc, double *n, double *total_length, int depth, double recip_prob);

/**
 * Creates an empty SCC graph.
 *
 * @return a pointer to the new graph
 */

tSccGraph create_scc_graph(tPdgVertex s, BOOL (*selector)(tPdgVertex v))
{
  // create struct
  tSccGraph g = malloc(sizeof(struct sSccGraph));
  
  // initialize
  g->source = s;
  g->min_mark = ++current_mark;

  // create array of capacity 1 to hold reachable vertices
  g->reachable = malloc(sizeof(tSccGraph));
  g->reachable_size = 0;
  g->reachable_capacity = 1;

  // create array to hold 1 SCC of size 1
  g->sccs = malloc(sizeof(tPdgVertex *));
  g->scc_size = malloc(sizeof(int));
  g->scc_capacity = malloc(sizeof(int));
  g->sccs_size = 0;
  g->sccs_capacity = 1;

  // do DFS, calculating finishing times
  dfs(g, s, g->min_mark, is_visited, get_backward_edges, selector, add_reachable);

  // print_vertices(g);

  // increment by 2 b/c we may decrement later (when splitting source
  // out of whatever SCC it is in) and we may need
  // min_scc_mark - min_mark >= number of vertices; this is the
  // easy way to ensure that w/o thinking about whether we really need to :)
  g->min_scc_mark = (current_mark += 2);

  // do DFS in order of decreasing finishing time to find SCCs
  int j;
  for (j = g->reachable_size - 1; j >= 0; j--)
    {
      if (!has_assigned_scc(g, g->reachable[j]))
	{
	  add_empty_scc(g);
	  dfs(g, g->reachable[j], ++current_mark, has_assigned_scc, get_forward_edges, selector, add_to_last_scc);
	}
    }

  // pull out source and fxn entry from their SCCs
  pull_source_and_function(g);

  // find entry and exit points for each SCC
  find_entry_and_exit_points(g, selector);

  print_sccs(g);

  return g;
}

static void add_reachable(tSccGraph g, tPdgVertex v)
{
  v->mark = ++current_mark;
  v->on_path_in_scc = 0;

  if (g->reachable_size == g->reachable_capacity)
    {
      resize(&g->reachable, &g->reachable_capacity);
    }

  g->reachable[g->reachable_size++] = v;
}

static void add_empty_scc(tSccGraph g)
{
  if (g->sccs_size == g->sccs_capacity)
    {
      g->sccs_capacity *= 2;
      g->sccs = realloc(g->sccs, sizeof(g->sccs[0]) * g->sccs_capacity);

      g->scc_size = realloc(g->scc_size, sizeof(g->scc_size[0]) * g->sccs_capacity);
      g->scc_capacity = realloc(g->scc_capacity, sizeof(g->scc_capacity[0]) * g->sccs_capacity);
    }

  // create an empty length-1 array to hold the new SCC
  g->sccs[g->sccs_size] = malloc(sizeof(tPdgVertex));
  g->scc_size[g->sccs_size] = 0;
  g->scc_capacity[g->sccs_size] = 1;
  g->sccs_size++;
}

static void add_to_last_scc(tSccGraph g, tPdgVertex v)
{
  int last = g->sccs_size - 1;

  // make sure there's room
  if (g->scc_size[last] == g->scc_capacity[last])
    {
      resize(&g->sccs[last], &g->scc_capacity[last]);
    }

  // add to array and increment size
  g->sccs[last][g->scc_size[last]++] = v;
}

void destroy_scc_graph(tSccGraph g)
{
  int s;
  for (s = 0; s < g->sccs_size; s++)
    {
      free(g->sccs[s]);
    }
  free(g->sccs);

  free(g->reachable);
  free(g);
}

/**
 * Performs a DFS of the given graph starting from the given vertex.
 * Whether a given vertex should be processed will be determined by
 * the given function, and processing starts by marking vertices
 * with the given value when first seen.  Once all of a vertex's
 * neighbors (determined by get_edge_list) have been processed, it
 * will be visited by passing it to the given function.
 */ 

static void dfs(tSccGraph g, tPdgVertex v, int mark, int (*is_visited)(tSccGraph, tPdgVertex), tPdgVertex *(*get_edge_list)(tPdgVertex, BOOL (*selector)(tPdgVertex v), int *), BOOL (*selector)(tPdgVertex v), void (*visit)(tSccGraph, tPdgVertex))
{
  v->mark = mark;

  int edge_count;
  tPdgVertex *edge = get_edge_list(v, selector, &edge_count);
  if (edge != NULL)
    {
      int i = 0;
      while (i < edge_count && *edge != NULL)
	{
	  tPdgVertex neighbor = strip_edge_kind(*edge);
	  if (selector(neighbor) && !is_visited(g, neighbor))
	    {
	      dfs(g, neighbor, mark, is_visited, get_edge_list, selector, visit);
	      i++;
	    }

	  // advance to next neighbor
	  edge++;
	}
    }

  // visit the vertex
  visit(g, v);
}


static tPdgVertex *get_forward_edges(tPdgVertex v, BOOL (*selector)(tPdgVertex v), int *count)
{
  // count edges
  *count = 0;
  tPdgVertex* edge = v->outgoing_intra_edges;
  if (edge != NULL)
    {
      while (*edge != NULL)
	{
	  if (selector(strip_edge_kind(v)))
	    {
	      (*count)++;
	    }
	  edge++;
	}
    }

  return v->outgoing_intra_edges;
}
  
static tPdgVertex *get_backward_edges(tPdgVertex v, BOOL (*selector)(tPdgVertex v), int *count)
{
  // count edges
  *count = 0;
  tPdgVertex* edge = v->incoming_intra_edges;
  if (edge != NULL)
    {
      while (*edge != NULL)
	{
	  if (selector(strip_edge_kind(v)))
	    {
	      (*count)++;
	    }
	  edge++;
	}
    }

  return v->incoming_intra_edges;
}

/**
 * A neighbor selector that selects all neighbors
 */
int selector_all(tPdgVertex v)
{
  return TRUE;
}

/**
 * A neighbor selector that selects those neighbors that are not declarations.
 */
int selector_no_decls(tPdgVertex v)
{
  return (v->kind != DECLARATION);
}

/**
 * Determines if the given vertex has been visited by the DFS that
 * finds vertices reachable from the source of the given graph.
 */

static int is_visited(tSccGraph g, tPdgVertex v)
{
  return (v->mark >= g->min_mark);
}

/**
 * Determines if the given vertex has been assigned to an SCC in the
 * given SCC graph.  A vertex not in the SCC graph at all (one not
 * reachable from the vertex the graph's source) is considered to have
 * an SCC ("none" rather than "unknown") assigned.
 */

static int has_assigned_scc(tSccGraph g, tPdgVertex v)
{
  return (v->mark < g->min_mark || v->mark >= g->min_scc_mark);
}

/**
 * Returns the index of the SCC the given vertex belongs to in the
 * given SCC graph, or -1 if it is not in the SCC graph.
 */

static int get_scc_index(tSccGraph g, tPdgVertex v)
{
  if (v->mark < g->min_mark)
    {
      return -1;
    }

  else
    {
      return v->mark - (g->min_scc_mark + 1);
    }
}

static void add_to_list(tPdgVertex v, tPdgVertex **arr, int *size, int *capacity)
{
  if (*size == *capacity)
    {
      resize(arr, capacity);
    }
  (*arr)[*size] = v;
  (*size)++;
}

static void resize(tPdgVertex **arr, int *capacity)
{
  *arr = realloc(*arr, sizeof(tPdgVertex *) * *capacity * 2);
  *capacity *= 2;
}

/**
 * Moves the source vertex and the function entry point into their
 * own components in the given graph.
 */

static void pull_source_and_function(tSccGraph g)
{
  // move the function entry point (can this ever be in an SCC of size > 1?)

  int last = g->sccs_size - 1;
  if (g->scc_size[last] > 1)
    {
      // find where the function entry point lives
      int i = 0;
      while (i < g->scc_size[last] && g->sccs[last][i]->kind != ENTRY)
	{
	  i++;
	}

      assert(i < g->scc_size[last]); // should be in there somewhere!

      // add fxn entry point to new scc
      add_empty_scc(g);
      add_to_last_scc(g, g->sccs[last][i]);
      g->sccs[last][i]->mark++;

      // move everything else down one
      g->scc_size[last]--;
      int j;
      for (j = i; j < g->scc_size[last]; j++)
	{
	  g->sccs[j] = g->sccs[j + 1];
	}
      g->sccs[j] = NULL;
    }

  // split out the source vertex

  if (g->scc_size[0] > 1)
    {
      // find the source vertex in SCC 0
      int i = 0;
      while (i < g->scc_size[0] && g->sccs[0][i] != g->source)
	{
	  i++;
	}
      
      assert(i < g->scc_size[0]); // should be in there somewhere!

      // remove fxn entry point from SCC
      g->scc_size[0]--;
      int k;
      for (k = i; k < g->scc_size[0]; k++)
	{
	  g->sccs[0][k] = g->sccs[0][k + 1];
	}
      g->sccs[0][k] = NULL;

      // add new component to hold source
      add_empty_scc(g);
      add_to_last_scc(g, g->source);

      // move things around so the new one is the first one
      tPdgVertex *tempA = g->sccs[g->sccs_size - 1];
      int tempSz = g->scc_size[g->sccs_size - 1];
      int tempCap = g->scc_capacity[g->sccs_size - 1];
      int c;
      for (c = g->sccs_size - 1; c >= 1; c--)
	{
	  g->sccs[c] = g->sccs[c- 1];
	  g->scc_size[c] = g->scc_size[c - 1];
	  g->scc_capacity[c] = g->scc_capacity[c - 1];
	}
      g->sccs[0] = tempA;
      g->scc_size[0] = tempSz;
      g->scc_capacity[0] = tempCap;

      // update mark that corresponds to SCC 0 to account for all the changes
      g->source->mark = g->min_scc_mark;
      g->min_scc_mark--;
    }
}

/**
 * For each SCC in the given graph, creates the list of entry points
 * and exit points.
 */

static void find_entry_and_exit_points(tSccGraph g, BOOL (*selector)(tPdgVertex v))
{
  // create initial empty arrays for each component
  g->entry_points = malloc(sizeof(tPdgVertex *) * g->sccs_size);
  g->entry_size = malloc(sizeof(int) * g->sccs_size);
  g->entry_capacity = malloc(sizeof(int) * g->sccs_size);
  g->exit_points = malloc(sizeof(tPdgVertex *) * g->sccs_size);
  g->exit_size = malloc(sizeof(int) * g->sccs_size);
  g->exit_capacity = malloc(sizeof(int) * g->sccs_size);
  int c;
  for (c = 0; c < g->sccs_size; c++)
    {
      g->entry_points[c] = malloc(sizeof(tPdgVertex));
      g->entry_size[c] = 0;
      g->entry_capacity[c] = 1;
      g->exit_points[c] = malloc(sizeof(tPdgVertex));
      g->exit_size[c] = 0;
      g->exit_capacity[c] = 1;
    }

  // find entry and exit points for each SCC
  for (c = 0; c < g->sccs_size; c++)
    {
      if (g->scc_size[c] == 1)
	{
	  // only one vertex in this SCC -- it must be entry and exit
	  add_to_list(g->sccs[c][0], &g->entry_points[c], &g->entry_size[c], &g->entry_capacity[c]);
	  add_to_list(g->sccs[c][0], &g->exit_points[c], &g->exit_size[c], &g->exit_capacity[c]);
	}
      else
	{
	  int i;
	  for (i = 0; i < g->scc_size[c]; i++)
	    {
	      // look for edges from earlier components
	      // (remember, we're looking at things backwards)
	      int count;
	      tPdgVertex *edge = get_forward_edges(g->sccs[c][i], selector, &count);

	      if (edge != NULL)
		{
		  int backward_count = 0;
		  int e = 0;
		  while (backward_count == 0 && e < count && *edge != NULL)
		    {
		      tPdgVertex neighbor = strip_edge_kind(*edge);

		      if (selector(neighbor))
			{
			  if (is_visited(g, neighbor) && get_scc_index(g, neighbor) < get_scc_index(g, g->sccs[c][i]))
			    {
			      backward_count++;
			    }
			  e++;
			}
		      edge++;
		    }
		  
		  if (backward_count > 0)
		    {
		      add_to_list(g->sccs[c][i], &g->entry_points[c], &g->entry_size[c], &g->entry_capacity[c]);
		    }
		}
	      
	      // look for edges to later components
	      // (remember, we're looking at things backwards)
	      edge = get_backward_edges(g->sccs[c][i], selector_all, &count);

	      if (edge != NULL)
		{
		  int forward_count = 0;
		  int e = 0;
		  while (forward_count == 0 && e < count && *edge != NULL)
		    {
		      tPdgVertex neighbor = strip_edge_kind(*edge);
		      if (selector(neighbor))
			{
			  if (is_visited(g, neighbor) && get_scc_index(g, neighbor) > get_scc_index(g, g->sccs[c][i]))
			    {
			      forward_count++;
			    }
			  e++;
			}
		      edge++;
		    }
		  
		  if (forward_count > 0)
		    {
		      add_to_list(g->sccs[c][i], &g->exit_points[c], &g->exit_size[c], &g->exit_capacity[c]);
		    }
		}
	    }
	}
    }
}	  
  
static void print_vertices(tSccGraph g)
{
  printf("Found %d vertices\n", g->reachable_size);
  int i;
  for (i = 0; i < g->reachable_size; i++)
    {
      pdg_vertex_print(g->reachable[i]);
    }
  printf("\n");
}

static void print_sccs(tSccGraph g)
{
  printf("Found %d SCCs\n", g->sccs_size);
  int c;
  for (c = 0; c < g->sccs_size; c++)
    {
      printf("SCC %d: [%d]", c, g->scc_size[c]);
      int i;
      /*
      for (i = 0; i < g->scc_size[c]; i++)
	{
	  pdg_vertex_print_short(g->sccs[c][i]);
	  printf("[%d] ", get_scc_index(g, g->sccs[c][i]));
	}
      */
      printf("\n");

      printf("%d entry points: ", g->entry_size[c]);
      for (i = 0; i < g->entry_size[c]; i++)
	{
	  pdg_vertex_print_short(g->entry_points[c][i]);
	}
      printf("\n");

      printf("%d exit points: ", g->exit_size[c]);
      for (i = 0; i < g->exit_size[c]; i++)
	{
	  pdg_vertex_print_short(g->exit_points[c][i]);
	}
      printf("\n");
    }
}

typedef enum {SCC_ENTRY = 0x1, SCC_EXIT = 0x1 << 1} half_vertex_kind;

/**
 * A struct containing the arguments for estimate_average_path_length
 * and objects and data needed for synchronizing with a thread that
 * enforces the time limit
 */

typedef struct apl_args
{
  // arguments for estimate_average_path_length
  tSccGraph g;
  BOOL (*selector)(tPdgVertex v);  
  double result;

  // stuff for timeout and synchronization
  int time_limit;
  int timed_out;
  pthread_mutex_t *lock;
  pthread_cond_t *finished;
} apl_args_t;


void *timer_entry(void *a)
{
  // set thread to be cancellable only at cancellation points
  // (b/c I don't know what happens when you cancel a thread in the middle
  // of sleep)
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);

  // convert type of argument
  apl_args_t *args = a;

  // wait given number of seconds, checking for cancellation each second
  int i;
  for (i = 0; i < args->time_limit; i++)
    {
      sleep(1);
      pthread_testcancel();
    }
  
  // if we got here then time expired before we were cancelled, so presumably
  // the computation did not finish in time -- set timeout flag and notify
  // master thread
  pthread_mutex_lock(args->lock);
  args->timed_out = 1;
  pthread_cond_broadcast(args->finished);
  pthread_mutex_unlock(args->lock);

  pthread_exit(NULL);
}

void *estimate_average_path_length_entry(void *a)
{
  // set this thread to be immediately cancellable
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

  // convert type of argument
  apl_args_t *args = a;

  // start computation
  double result = estimate_average_path_length(args->g, args->selector);

  // computation finished -- check for timeout
  pthread_mutex_lock(args->lock);
  if (args->timed_out == 0)
    {
      // computation finished before timeout -- record result and notify master
      args->result = result;
      pthread_cond_broadcast(args->finished);
    }
  pthread_mutex_unlock(args->lock);
  
  pthread_exit(NULL);
}

/**
 * Estimates average path length in a SCC graph, with a limit on the 
 * computation time.  If the computation completed within the specified
 * time then the returned value is the actual average path length.  If
 * the computation takes too long then the return value is positive infinity.
 */

double timed_estimate_average_path_length(tSccGraph g, BOOL (*selector)(tPdgVertex v), int timeout_sec)
{
  // initialize mutex and condition variable to control sharing of args
  pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t finished = PTHREAD_COND_INITIALIZER;
  
  // set up args with graph, selector, and synchronization stuff
  apl_args_t *args = malloc(sizeof(apl_args_t));
  args->g = g;
  args->selector = selector;
  args->time_limit = timeout_sec;
  args->timed_out = 0;
  args->lock = &lock;
  args->finished = &finished;

  // ids of the two threads
  pthread_t apl_id;
  pthread_t timer_id;

  // lock the mutex to make sure new threads don't get in before this master
  pthread_mutex_lock(&lock);

  // create and detach the two threads
  pthread_create(&apl_id, NULL, estimate_average_path_length_entry, args);
  pthread_create(&timer_id, NULL, timer_entry, args);
  pthread_detach(apl_id);
  pthread_detach(timer_id);

  // wait for one thread to finish
  pthread_cond_wait(&finished, &lock);
  pthread_mutex_unlock(&lock);

  // figure out which thread finished and cancel the other one
  double result;
  if (args->timed_out == 0)
    {
      // computation thread finished -- kill timer
      pthread_cancel(timer_id);

      // wait for thread to really complete before continuing
      void *dummy;
      pthread_join(timer_id, &dummy);

      // get result from computation thread
      result = args->result;
    }
  else
    {
      // timer thread finished -- kill computation thread
      pthread_cancel(apl_id);

      // wait for thread to really complete before continuing
      void *dummy;
      pthread_join(apl_id, &dummy);

      // return infinity
      result = 1e+300;
      result *= result;
    }

  free(args);
  return result;
}

int validate_half_vertices(tSccGraph g, tPdgVertex *half_vertex, int total_halves)
{
  int i;
  for (i = 0; i < total_halves; i++)
    {
      int c = get_scc_index(g, half_vertex[i]);
      assert(c >= 0 && c < g->sccs_size);
    }
  return 1;
}

double estimate_average_path_length(tSccGraph g, BOOL (*selector)(tPdgVertex v))
{
  // check that SCCs are of the right size for this run -- if outside the

  // range then return 0 or +infinity

  int max_size = 0;
  int comp;
  for (comp = 0; comp < g->sccs_size; comp++)
    {
      if (g->scc_size[comp] > max_size)
	{
	  max_size = g->scc_size[comp];
	}
    }
  printf("Largest SCC has size %d\n", max_size);

  if (max_size >= max_scc_to_process)
    {
      // return INFINITY; can I have my C99 please?
      double result = 1e+300;
      return result * result;
    }
  else if (max_size < min_scc_to_process)
    {
      return 0.0;
    }

  // seed the random number generator for use when sampling paths
  struct timeval time;
  gettimeofday(&time, NULL);
  srand(time.tv_sec);

  // set some values for each vertex
  int v;
  for (v = 0; v < g->reachable_size; v++)
    {
      g->reachable[v]->entry_half = -1;
      g->reachable[v]->exit_half = -1;
      g->reachable[v]->on_path = 0;
    }

  // count total entry vertices and exit vertices
  int total_entry = 0;
  int total_exit = 0;
  int c;
  for (c = 0; c < g->sccs_size; c++)
    {
      total_entry += g->entry_size[c];
      total_exit += g->exit_size[c];
    }
  int total_halves = total_entry + total_exit;

  assert(total_halves > 0);

  // allocate array to hold all the half-vertices
  tPdgVertex *half_vertex = malloc(sizeof(tPdgVertex) * total_halves);
  int *half_kind = malloc(sizeof(int) * total_halves);

  // populate the half vertex array
  int i = 0;
  for (c = 0; c < g->sccs_size; c++)
    {
      // add the entry points for component c
      int j;
      for (j = 0; j < g->entry_size[c]; j++)
	{
	  half_vertex[i] = g->entry_points[c][j];
	  half_kind[i] = SCC_ENTRY;
	  g->entry_points[c][j]->entry_half = i;
	  i++;
	}

      // add the exit points for component c
      for (j = 0; j < g->exit_size[c]; j++)
	{
	  half_vertex[i] = g->exit_points[c][j];
	  half_kind[i] = SCC_EXIT;
	  g->exit_points[c][j]->exit_half = i;
	  i++;
	}
    }
  assert(i <= total_halves);
  assert(i == total_halves);

  // create n[c] = number of paths from source to half vertex x
  // and len[c] = average length of paths from source to half vertex x
  long long *total_len = malloc(sizeof(long long) * total_halves);
  long long *n = malloc(sizeof(long long) * total_halves);

  // initialize for source vertex (1 path of length 0: source)
  // and all other vertices (0 paths so far)
  total_len[0] = 0;
  n[0] = 1;
  for (i = 1; i < total_halves; i++)
    {
      total_len[i] = 0;
      n[i] = 0;
    }

  // validate half vertices
  // assert(validate_half_vertices(g, half_vertex, total_halves));

  // update lengths and counts going forward
  for (i = 0; i < total_halves - 1; i++)
    {
      // printf("HALF-VERTEX %x %s: %lld %lld\n", half_vertex[i]->name, half_vertex[i]->src, total_len[i], n[i]);

      // assert(validate_half_vertices(g, half_vertex, total_halves));

      if (half_kind[i] == SCC_EXIT)
	{
	  // go over edges to later SCCs
	  int count;
	  tPdgVertex *edge = get_backward_edges(half_vertex[i], selector, &count);
	  int e = 0;
	  while (e < count && *edge != NULL)
	    {
	      tPdgVertex neighbor = strip_edge_kind(*edge);

	      if (selector(neighbor))
		{
		  // check whether edge goes forward
		  if (has_assigned_scc(g, neighbor) && get_scc_index(g, neighbor) > get_scc_index(g, half_vertex[i]))
		    {
		      // update path count and average length at neighbor
		      int j = neighbor->entry_half;
		      assert(j < total_halves);
		      total_len[j] += total_len[i] + n[i];
		      n[j] += n[i];
		    }
		  
		  e++;
		}
	      edge++;
	    }
	}
      else
	{
	  // get index of SCC the vertex is in
	  c = get_scc_index(g, half_vertex[i]);

	  // make arrays for paths and total_length from half vertex i
	  double *n_from_i = malloc(sizeof(double) * total_halves);
	  double *length_from_i = malloc(sizeof(double) * total_halves);
	  int j;
	  for (j = 0; j < total_halves; j++)
	    {
	      n_from_i[j] = 0.0;
	      length_from_i[j] = 0.0;
	    }

	  // find all paths from the half vertex that don't leave the SCC
	  estimate_path_length_in_scc(g, selector, half_vertex[i], c, n_from_i, length_from_i, total_halves);

	  // update n and total_len for all exit points
	  int k;
	  for (k = 0; k < g->exit_size[c]; k++)
	    {
	      int j = g->exit_points[c][k]->exit_half;
	      assert(j < total_halves);

	      // total path length to j goes up by the number of paths we
	      // just found (n[i] ways to get to i and n_from_i[j] ways to get
	      // through the SCC) * the average length of those paths
	      // (the average length to i + the average length through the SCC)
	      // total_len[j] += (n[i]  * n_from_i[j]) * (total_len[i] / n[i] + length_from_i[j] / n_from_i[j]);
	      total_len[j] += n_from_i[j] * total_len[i] + n[i] * length_from_i[j];
	      n[j] += n[i] * n_from_i[j];

	      // make sure there is no overflow
	      assert(total_len[j] >= 0);
	      assert(n[j] >= 0);
	    }

	  free(n_from_i);
	  n_from_i = NULL;

	  free(length_from_i);
	  length_from_i = NULL;
	}
    }

  // save result and free arrays
  double result = (double)total_len[total_halves - 1] / n[total_halves - 1];
  assert(!isnan(result));

  free(total_len);
  total_len = NULL;

  free(n);
  n = NULL;

  free(half_vertex);
  half_vertex = NULL;

  free(half_kind);
  half_kind = NULL;

  return result;
}

#define DEFAULT_SAMPLES 1000000

static void estimate_path_length_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex from, int c, double *n_from_i, double *length_from_i, int half_vertex_count)
{
  if (1 || g->scc_size[c] <= 1000)
    {
      find_all_paths_in_scc(g, selector, from, c, n_from_i, length_from_i, 0, half_vertex_count);
    }
  else
    {
      /* For getting to a single target destination */
      /*
      sample_paths_in_scc(g, from, c, n_from_i, length_from_i, half_vertex_count, DEFAULT_SAMPLES);
      */

	/* For wandering around and updating all exit points we find */
      int i;
      int notification = DEFAULT_SAMPLES + 1; // 1 to turn on progress reports 
      for (i = 0; i < DEFAULT_SAMPLES; i++)
	{
	  random_walk_in_scc(g, selector, from, c, n_from_i, length_from_i, 0, 1);
	  if (i + 1 == notification)
	    {
	      printf("Doing sample %d\n", notification);
	      notification *= 2;

	      printf("Current estimates: ");
	      int j;
	      for (j = 0; j < g->exit_size[c]; j++)
		{
		  printf("%8.4f ", (double)length_from_i[g->exit_points[c][j]->exit_half] / n_from_i[g->exit_points[c][j]->exit_half]);
		}
	      printf("\n");
	    }
	}



      int v;
      for (v = 0; v < half_vertex_count; v++)
	{
	  n_from_i[v] /= DEFAULT_SAMPLES;
	  length_from_i[v] /= DEFAULT_SAMPLES;
	}
    }
}
  
static void estimate_length_within_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex from, tPdgVertex to, long long *paths, long long *total_length)
{
  if (from == to)
    {
      // if from = to then there is 1 path of length 0: [from]
      *paths = 1;
      *total_length = 0;
    }
  else
    {
      // pretend for now that there is one path of length 1
      *paths = 1;
      *total_length = 1;
    }
}

static void find_all_paths_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex v, int scc, double *n, double *total_length, int depth, int half_vertex_count)
{
  // mark vertex as in this path
  v->on_path_in_scc = 1;

  // update counts if v is an exit vertex
  if (v->exit_half != -1)
    {
      assert(v->exit_half >= 0 && v->exit_half < half_vertex_count);

      n[v->exit_half] += 1.0;
      total_length[v->exit_half] += depth;
    }

  // see where else we can go
  int count;
  tPdgVertex *edge = get_backward_edges(v, selector, &count);
  int e = 0;
  while (e < count && *edge != NULL)
    {
      tPdgVertex neighbor = strip_edge_kind(*edge);

      if (selector(neighbor))
	{
	  // go to neighbor if in same SCC and not on path yet
	  if (get_scc_index(g, neighbor) == scc && !neighbor->on_path_in_scc)
	    {
	      find_all_paths_in_scc(g, selector, neighbor, scc, n, total_length, depth + 1, half_vertex_count);
	      assert(g->source != NULL);
	    }
	  e++;
	}
      edge++;
    }

  // unmark vertex as we unwind so we can get to again in an different way
  v->on_path_in_scc = 0;
}

static void random_walk_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex v, int scc, double *n, double *total_length, int depth, double recip_prob)
{
  // suppose P_1, P_2, ... are the paths
  // X_n = 1 if path n found
  // E[X_n] = P(random walk finds P_n=u_1,...,u_k)
  // = 1/product(i=1..k-1,outdegree*(u_i)) (* adj for edges that make cycles)
  // call that denominator the "branchiness" of P_n
  // E[branchness(P_1) * X_1 + ...] = 1 + ... = # of paths
  // E[branchiness(P_1) * X_1 * k_1 + ...] = k_1 + ... = total lengths of paths

  // mark vertex as in this path
  v->on_path_in_scc = 1;

  // update counts if v is an exit vertex
  if (v->exit_half != -1)
    {
      n[v->exit_half] += recip_prob;
      total_length[v->exit_half] += depth * recip_prob;
    }
  
  // see where else we can go
  int count;
  tPdgVertex *edge = get_backward_edges(v, selector, &count);
  int e = 0;
  int num_choices = 0;
  while (e < count && *edge != NULL)
    {
      tPdgVertex neighbor = strip_edge_kind(*edge);

      if (selector(neighbor))
	{
	  // go to neighbor if in same SCC and not on path yet
	  if (get_scc_index(g, neighbor) == scc && !neighbor->on_path_in_scc)
	    {
	      num_choices++;
	    }
	  e++;
	}
      edge++;
    }

  if (num_choices > 0)
    {
      int choice = random() % num_choices;
      e = 0;
      edge = get_backward_edges(v, selector, &count);
      while (e < count && *edge != NULL)
	{
	  tPdgVertex neighbor = strip_edge_kind(*edge);

	  if (selector(neighbor))
	    {
	      if (get_scc_index(g, neighbor) == scc && !neighbor->on_path_in_scc)
		{
		  choice--;
		  if (choice == 0)
		    {
		      random_walk_in_scc(g, selector, neighbor, scc, n, total_length, depth + 1, recip_prob * num_choices);
		    }
		}
	      e++;
	    }
	  edge++;
	}
    }

  // unmark vertex as we unwind so we can get to again in an different way
  v->on_path_in_scc = 0;
}

static void lint()
{
  lint(); print_vertices(NULL); print_sccs(NULL);
  estimate_length_within_scc(NULL, NULL, NULL, NULL, NULL, NULL);
}

static void sample_paths_in_scc(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex source, int c, double *n_from_source, double *length_from_source, int half_vertex_count, int num_samples)
{
  int x;
  for (x = 0; x < g->exit_size[c]; x++)
    {
      printf("Sampling from ");
      pdg_vertex_print_short(source);
      printf(" to ");
      pdg_vertex_print_short(g->exit_points[c][x]);
      printf("\n");

      int half_vertex_index = g->exit_points[c][x]->exit_half;

      int i;
      for (i = 0; i < num_samples; i++)
	{
	  if (sample_paths_between_vertices(g, selector, source, g->exit_points[c][x], c, &n_from_source[half_vertex_index], &length_from_source[half_vertex_index], 1.0, 0))
	    {
	      //pdg_vertex_print_short(source);
	      //printf("\n");
	    }
	}

      printf("Estimate %g paths of avg. length %g\n",
	     n_from_source[half_vertex_index] / num_samples,
	     length_from_source[half_vertex_index] / n_from_source[half_vertex_index]);
    }

  int v;
  for (v = 0; v < half_vertex_count; v++)
    {
      n_from_source[v] /= num_samples;
      length_from_source[v] /= num_samples;
    }
}

static BOOL sample_paths_between_vertices(tSccGraph g, BOOL (*selector)(tPdgVertex v), tPdgVertex source, tPdgVertex dest, int c, double *path_count, double *total_length, double recip_prob, int depth)
{
  if (source == dest)
    {
      *path_count += recip_prob;
      *total_length += recip_prob * depth;

      // printf("Found path of length %d with 1/P(getting here by random)=%g\n", depth, recip_prob);

      // printf("FOUND PATH (backwards): ");
      // pdg_vertex_print_short(dest);

      return TRUE;
    }
  else
    {
      source->on_path_in_scc = TRUE;

      // see where else we can go
      int count;
      tPdgVertex *edge = get_backward_edges(source, selector, &count);
      int e = 0;
      int num_choices = 0;
      while (e < count && *edge != NULL)
	{
	  tPdgVertex neighbor = strip_edge_kind(*edge);
	  
	  if (selector(neighbor))
	    {
	      // go to neighbor if in same SCC and not on path yet
	      if (get_scc_index(g, neighbor) == c && !neighbor->on_path_in_scc)
		{
		  num_choices++;
		}
	      e++;
	    }
	  edge++;
	}

      if (num_choices > 0)
	{
	  int choice = (random() % num_choices) + 1;
	  e = 0;
	  edge = get_backward_edges(source, selector, &count);
	  while (e < count && *edge != NULL)
	    {
	      tPdgVertex neighbor = strip_edge_kind(*edge);

	      if (selector(neighbor))
		{
		  if (get_scc_index(g, neighbor) == c && !neighbor->on_path_in_scc)
		    {
		      choice--;
		      if (choice == 0)
			{
			  BOOL result = sample_paths_between_vertices(g, selector, neighbor, dest, c, path_count, total_length, recip_prob * num_choices, depth + 1);
			  
			  if (result)
			    {
			      // pdg_vertex_print_short(source);
			    }
			  
			  source->on_path_in_scc = FALSE;
			  
			  return result;
			}
		    }
		  e++;
		}
	      edge++;
	    }

	  assert(FALSE); // should always find our randomly chosen edge
	}
      else 
	{
	  source->on_path_in_scc = FALSE;

	  return FALSE;
	}
    }
}

#define MAX_FACTORIAL 30 //_really_ unlikely that we'll find a path of this length
static long double *factorial_values = NULL;

static long double factorial(int k)
{
  if (k > MAX_FACTORIAL)
    {
      k = MAX_FACTORIAL;
    }

  if (factorial_values == NULL)
    {
      factorial_values = malloc(sizeof(long double) * (MAX_FACTORIAL + 1));
      factorial_values[0] = 1;
      int i;
      for (i = 1; i <= MAX_FACTORIAL; i++)
	{
	  factorial_values[i] = i * factorial_values[i - 1];
	}
    }

  return factorial_values[k];
}
