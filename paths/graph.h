#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <vector>
#include <map>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <set>

namespace std
{
  template < class Key >
    class DFSVisitor
    {
    public:
      virtual ~DFSVisitor();

      /**
       * Invoked when DFS starts at the given vertex.
       */
      virtual void startDFS(const Key& v) = 0;

      /**
       * Invoked when a DFS sthat started at the given vertex ends.
       */
      virtual void endDFS(const Key& v) = 0; 

      /**
       * Invoked before the neighbors of v are visited.
       *
       * @param v the vertex to visit
       * @param parent the parent of v in the DFS tree
       */
      virtual void preVisit(const Key &v, const Key& parent) = 0;

      /**
       * Invoked after the neighbors of a vertex have been visited.
       *
       * @param v the vertex to visit
       * @param parent the parent of v in the DFS tree
       */
      virtual void postVisit(const Key& v, const Key& parent) = 0;
    };

  template < class Key >
    class DFSEdgeAcceptor
    {
    public:
      virtual ~DFSEdgeAcceptor();

      virtual bool shouldFollow(const Key& from, const Key& to) const = 0;
    };

  template < class Key >
    class CompoundDFSVisitor : public DFSVisitor< Key >
  {
  public:
    CompoundDFSVisitor(DFSVisitor< Key >& v1, DFSVisitor< Key >& v2);
    virtual ~CompoundDFSVisitor();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);

  private:
    DFSVisitor< Key >& visitor1;
    DFSVisitor< Key >& visitor2;
  };

  template < class Key >
    class DFSColorizer : public DFSVisitor< Key >, public DFSEdgeAcceptor< Key >
  {
  public:
    virtual ~DFSColorizer();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);
    bool shouldFollow(const Key& from, const Key& to) const;
    int getColor(const Key& v) const;

    static const int WHITE = 0;
    static const int GRAY = 1;
    static const int BLACK = 2;

  protected:
    tr1::unordered_map< Key, int > colors;
  };

  template < class Key >
    class DFSTimeCounter : public DFSVisitor< Key >
  {
  public:
    DFSTimeCounter();
    virtual ~DFSTimeCounter();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);
    int getStartTime(const Key& v) const;
    int getFinishTime(const Key& v) const;
    
  protected:
    int time;
    tr1::unordered_map< Key, int > startTimes;
    tr1::unordered_map< Key, int > finishTimes;
  };
  
  template < class Key >
    class SCCBuilder : public DFSVisitor< Key >
  {
  public:
    SCCBuilder();
    virtual ~SCCBuilder();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);
    tr1::unordered_map< Key, int > getSCCMap() const;
    
  protected:
    int currentComponent;
    tr1::unordered_map< Key, int > components;
  };

  template < class Key >
    class TopologicalSorter : public DFSVisitor< Key >
  {
  public:
    virtual ~TopologicalSorter();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);
    const std::vector< Key >& getOrder() const;
    
  protected:
    std::vector< Key > order;
  };

  template < class Key >
    class PathProcessor : public DFSVisitor< Key >, public DFSEdgeAcceptor< Key >
  {
  public:
    virtual ~PathProcessor();

    void startDFS(const Key& v);
    void endDFS(const Key& v);
    void preVisit(const Key &v, const Key& parent);
    void postVisit(const Key& v, const Key& parent);
    bool shouldFollow(const Key& from, const Key& to) const;
  
  protected:
    virtual void addToPath(const Key& v);
    virtual void removeLastOnPath();

    vector< Key > path;
    tr1::unordered_set< Key > onPath;
  };

  template < class Key >
    class Predicate
    {
    public:
      virtual ~Predicate();

      virtual bool operator()(const Key& v) const = 0;
    };
  
  /**
   * A directed graph.
   * @param Key the type of the immutable keys that are used to refer to
   * vertices; this type should have an equality operator ==
   * @param KeyHash has function object for vertex keys
   */
  
  template < class Key >
    class DirectedGraph
    {
    public:
      /**
       * Creates an empty graph.
       */
      DirectedGraph();

      /**
       * Copies all vertices and edges from the given graph into this graph,
       * except edges coming into the given vertex.
       */
      void copyExceptIncoming(const DirectedGraph< Key >& g, const Key& v);

      /**
       * Adds the given vertex to this graph.  If the vertex is already present
       * then there is no effect and the return value is false.
       *
       * @param v the key of the new vertex
       * @return true iff the vertex was added
       */
      bool addVertex(Key v);
      
      /**
       * Adds an edge from the given source vertex to the given destination
       * vertex.  If the vertices are not present then they are added.
       * If the edge is already present then it is added again -- vertices may
       * have multiple edges to the same destination vertex.
       *
       * @param from the key of the source vertex
       * @param to the key of the destination vertex
       */
      void addEdge(Key from, Key to);
      
      /**
       * Returns a list of the vertices in this graph.
       *
       * @return a list of the vertices in this graph
       */
      const vector< Key >& vertexList() const;

      /**
       * Returns a list of the keys of the vertices that have edges from
       * the given vertex.  If the vertex does not exist then the list is
       * empty.
       *
       * @param v the key of a vertex
       * @return a list of the keys of the vertices that have edges from v, or
       * an empty list if v is not a vertex
       */
      vector< Key > adjacentFrom(const Key& v) const;
      
      /**
       * Returns a list of the keys of the vertices that have edges to
       * the given vertex.  If the vertex does not exist then the list is
       * empty.
       *
       * @param v the key of a vertex
       * @return a list of the keys of the vertices that have edges to v, or
       * an empty list if v is not a vertex
       */
      vector< Key > adjacentTo(const Key& v) const;
      
      /**
       * Determines if there exists an incoming edge at the given vertex
       * thatsatisfies the given predicate.
       */
      bool existsIncomingEdge(const Key& v, const Predicate< Key >& pred) const;

      /**
       * Determines if there exists an outgoing edge at the given vertex
       * that satisfies the given predicate.
       */
      bool existsOutgoingEdge(const Key& v, const Predicate< Key >& pred) const;

      /**
       * Performs a depth-first search starting from the given vertex.
       * As each vertex is visited the preVisit method of the visitor object
       * is invoked before the neighbors are visited, and then the postVisit 
       * method is invoked.
       *
       * @param v the key of vertex in this graph
       * @param visitor an object to perform preVisit and postVisit on the
       * vertices as they are visited
       */
      void dfs(Key v, DFSVisitor< Key >& visitor) const;

      /**
       * Performs a depth-first search starting from an arbitrarily selected
       * vertex, and restarting at a new vertex until all vertices have
       * been visited.
       * As each vertex is visited the preVisit method of the visitor object
       * is invoked before the neighbors are visited, and then the postVisit 
       * method is invoked.
       *
       * @param v the key of vertex in this graph
       * @param visitor an object to perform preVisit and postVisit on the
       * vertices as they are visited
       */
      void fullDFS(DFSVisitor< Key >& visitor) const;

      /**
       * Performs a depth-first traversal of the graph.  Whether edges
       * are followed is solely determined by the given acceptor; the DFS
       * algorithm implemented by this method does not by itself keep track
       * of which vertices have already been visited.
       *
       * @param v the key of a vertex in this graph
       * @param visit a visitor object to process vertices as they
       * are visited
       * @param acceptor an edge acceptor used to determine which edges to
       * follow
       */
      void dfsTraverse(Key v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const;
      
      /**
       * Returns a new graph that is a copy of this one with the edges
       * reversed.
       */
      DirectedGraph< Key > reverse() const;

      /**
       * Finds the strongly connected components in this graph.  The
       * strongly connected components will be so that the lowest
       * index is 0 and if SCC V is reachable from SCC U then the
       * index of U is less than the index of V (so that the source
       * SCC has index 0 and listing the SCCs in order of increasing
       * index gives a topological sort).  The returned map maps vertices to the
       * indices of their components.
       */
      tr1::unordered_map< Key, int > findSCCs() const;

      /**
       * Topologically sorts this graph, assuming that it is acyclic.  If there is an edge
       * from v to u then u will be before v in the returned vector.  If there are cycles
       * in the graph then the behavior is undetermined.
       */
      std::vector< Key > topologicalSort() const;

    private:
      DirectedGraph(const vector< Key >& verts, const tr1::unordered_map< Key, int >& k, const vector< vector< int > >& in, const vector< vector< int > >& out);
      void startDFS(int v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const;
      void dfs(int v, DFSVisitor< Key >& visitor, const DFSEdgeAcceptor< Key >& acceptor) const;
      vector< Key > makeKeyList(const vector< int >& indices) const;
      bool existsEdge(int, const Predicate< Key >& pred, const vector< vector< int > >& adjList) const;
      
      vector< Key > vertices;
      tr1::unordered_map< Key, int > keys;
      vector< vector< int > > inAdjLists;
      vector< vector< int > > outAdjLists;
    };
};

#endif
