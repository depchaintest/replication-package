/* ht.h */

/* copyright (c) The Authors 2002 */

#ifndef _HT
#define _HT

#include "set.h"

typedef struct sHashTable  *tHashTable;

/* exported functions */

tHashTable ht_initialize(int, int (*compare_function)(),int(*hash_function)());
void ht_free(tHashTable);
void ht_insert(tHashTable, void*);
void ht_delete(tHashTable, void*);
int  ht_size(tHashTable);
void *ht_lookup(tHashTable, void*);
void ht_print(tHashTable, void (*print_function)());
void ht_foreach(tHashTable, void (*function)(void *));
void ht_foreach1(tHashTable, void (*function)(void *, void *), void*);
void ht_foreach2(tHashTable, void (*function)(void *, void *, void *), void*, void*);
void ht_foreach3(tHashTable, void (*function)(void *, void *, void *, void *), void*, void*, void* );
void *ht_any_member(tHashTable);

#endif
