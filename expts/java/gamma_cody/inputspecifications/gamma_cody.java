package org.iguanatool.casestudies.gamma_cody.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gamma_cody extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
