package org.iguanatool.casestudies.pnchisq.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class pnchisq_raw extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(5, -100.0, 100.0, 1);
        addInt(2, 0, 1);
	}
}
