package org.iguanatool.casestudies.i0.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class i0e extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
