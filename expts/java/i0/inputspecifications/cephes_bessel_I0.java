package org.iguanatool.casestudies.i0.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class cephes_bessel_I0 extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
