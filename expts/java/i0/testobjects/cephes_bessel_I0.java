package org.iguanatool.casestudies.i0.testobjects; 

import org.iguanatool.testobject.CaseStudy;
import org.iguanatool.testobject.InputSpecification;
import org.iguanatool.testobject.JNILibrary;
import org.iguanatool.testobject.TestObject;

public class cephes_bessel_I0 extends TestObject { 

	private static final CaseStudy CASE_STUDY    = new CaseStudy("i0");
	private static final String TEST_OBJECT_NAME = "cephes_bessel_I0";
	private static final int TEST_OBJECT_ID      = 0;
	
	static {
		System.load(new JNILibrary(CASE_STUDY, TEST_OBJECT_NAME).getLibraryFile().getPath());
	}

	protected void loadCFG() {
		loadCFG(CASE_STUDY.getCFGFile(TEST_OBJECT_NAME));
	}
	
	protected InputSpecification instantiateInputSpecification() {
		return new org.iguanatool.casestudies.i0.inputspecifications.cephes_bessel_I0();
	}
	
	public int getTestObjectID() {
		return TEST_OBJECT_ID;
	}
	
    protected native void call(double[] args);	
}
