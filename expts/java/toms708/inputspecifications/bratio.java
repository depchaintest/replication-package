package org.iguanatool.casestudies.toms708.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class bratio extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(6, -100.0, 100.0, 1);
		addInt(2, 0, 1);
	}
}
