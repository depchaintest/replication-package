package org.iguanatool.casestudies.toms708.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class psi extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
