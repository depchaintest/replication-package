package org.iguanatool.casestudies.ptukey.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class wprob extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(3, -100.0, 100.0, 1);
	}
}
