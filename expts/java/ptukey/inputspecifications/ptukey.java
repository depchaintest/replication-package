package org.iguanatool.casestudies.ptukey.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class ptukey extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(4, -100.0, 100.0, 1);
		addInt(2, 0, 1);
	}
}
