package org.iguanatool.casestudies.spice1.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class clip_to_circle extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addInt(7, -1000, 1000);
	}
}
