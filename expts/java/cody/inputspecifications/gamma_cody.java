package org.iguanatool.casestudies.cody.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gamma_cody extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.00, 100.00, 1);
	}
}
