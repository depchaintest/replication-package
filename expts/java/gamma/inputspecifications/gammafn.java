package org.iguanatool.casestudies.gamma.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gammafn extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
