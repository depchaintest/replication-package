package org.iguanatool.casestudies.g.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gradient_calc_spiral_factor extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(6, -100.0, 100.0, 1);
	}
}
