package org.iguanatool.casestudies.g.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gradient_calc_square_factor extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(4, -100.0, 100.0, 1);
	}
}
