package org.iguanatool.casestudies.g.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class gradient_calc_conical_sym_factor extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(5, -100.0, 100.0, 1);
	}
}
