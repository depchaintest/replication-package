package org.iguanatool.casestudies.pnorm.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class pnorm_both extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(3, -100.0, 100.0, 1);
		addInt(2, 0, 1);
	}
}
