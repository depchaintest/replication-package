package org.iguanatool.casestudies.k0.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class k0e extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
