package org.iguanatool.casestudies.k0.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class cephes_bessel_K0 extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
