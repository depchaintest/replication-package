package org.iguanatool.casestudies.rhyper.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class rhyper extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		//addDouble(3, -1.000000, 1.000000, 4);
		addDouble(3, -100.0, 100.0, 1);
	}
}
