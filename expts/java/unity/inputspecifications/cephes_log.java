package org.iguanatool.casestudies.unity.inputspecifications; 

import org.iguanatool.testobject.InputSpecification;

public class cephes_log extends InputSpecification {
	
	public void defaultSetup() {
		// input specification code goes here
		addDouble(1, -100.0, 100.0, 1);
	}
}
