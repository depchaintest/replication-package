package org.iguanatool.ext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.iguanatool.library.Array;
import org.iguanatool.testobject.TestObject;
import org.iguanatool.testobject.TestObjectFactory;
import org.iguanatool.testobject.structure.CFGNode;
import org.iguanatool.testobject.structure.condition.ComposedCondition;
import org.iguanatool.testobject.structure.condition.Condition;

public class TSVReader {

	private String file;
	private BufferedReader br = null;

	public TSVReader(String file) {
		this.file = file;
	}

	public void getBranchesInfoTestObject() {

		String line = "";
		String cvsSplitBy = "\t";

		try {
			// Getting Case Study (Test Object) Name from File name
			String testObjectNameFromTSVFile = file.split("\\--")[0];
			// System.out.println("Technique = " + file.split("\\--")[1]);
			// System.out.println("RandomSeed = " + file.split("\\--")[2]);
			// Getting List Test Objects (Methods) within a case study
			List<TestObject> testObjects = TestObjectFactory.instantiate(testObjectNameFromTSVFile);
			// Starting a list of rows for CSV
			List<String[]> rows = new ArrayList<String[]>();
			// Headings in CSV file
			String[] headings = { "Test Object", "Condition No", "node", "True Evaluations", "False Evaluations",
					"Code" };
			rows.add(headings);
			// System.out.println(testObjects);
			// Run through each instantiated test object
			for (TestObject testObjectMethod : testObjects) {
				// Read each Line in TSV file
				br = new BufferedReader(new FileReader(file));
				// Initilise a test object to get info of Condition and Code
				TestObject testObjectMethodReplacer = null;
				// Getting test object method path from TSV
				String testObjectPackageName = null;
				// Condition Counter
				int conditionCounter = 0;
				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] testobject = line.split(cvsSplitBy);
					// Get Test Object method form TSV if it is in first line
					if (testobject[0].contains("org.iguanatool.casestudies")) {
						testObjectPackageName = "org.iguanatool.casestudies." + testobject[0].split("\\.")[3]
								+ ".testobjects." + testobject[0].split("\\.")[5];
					}

					// Get Info of test object branches
					if (!testobject[0].contains("org.iguanatool.casestudies")) {
						// Check if test object Method instantiated is equal to the one in TSV
						if (testObjectMethod.getClass().getName().equals(testObjectPackageName)) {
							testObjectMethodReplacer = testObjectMethod;
							int node = Integer.parseInt(testobject[0].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[0]);
							// Get Node from IGUANA CFG
							CFGNode cfgNode = testObjectMethodReplacer.getCFGNode(node);

							if (cfgNode.getCondition() instanceof ComposedCondition) {

								for (Condition condition : (((ComposedCondition) cfgNode.getCondition()).getSubConditions())) {
									String conditionNo = ""+conditionCounter;
									String nodeInfo = testobject[0].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[0];
									String predicate = testobject[0].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[1];
									String txt = "\""+cfgNode.getCode()+"\"";
		
									String[] row = { testObjectMethod.getClass().getName().split("\\.")[5], conditionNo,
											nodeInfo, null, null, txt };
		
									if (predicate.equals("T")) {
										int eva = Integer.parseInt(testobject[2]);
										if (testobject[1].equals("FAIL") && eva == 100000) {
											row[3] = "-1";
										} else {
											row[3] = testobject[2];
										}
										rows.add(row);
										conditionCounter++;
									} else {
										// String[] rowFalse = ;
										int eva = Integer.parseInt(testobject[2]);
										for (String[] rowArray : rows) {
											if (rowArray[2].equals(nodeInfo)) {
												if (testobject[1].equals("FAIL") && eva == 100000) {
													rowArray[4] = "-1";
												} else {
													rowArray[4] = testobject[2];
												}
											}
										}
									}
								}
							} else {
								String conditionNo = ""+conditionCounter;
								String nodeInfo = testobject[0].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[0];
								String predicate = testobject[0].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)")[1];
								String txt = "\""+cfgNode.getCode()+"\"";
	
								String[] row = { testObjectMethod.getClass().getName().split("\\.")[5], conditionNo,
										nodeInfo, null, null, txt };
	
								if (predicate.equals("T")) {
									int eva = Integer.parseInt(testobject[2]);
									if (testobject[1].equals("FAIL") && eva == 100000) {
										row[3] = "-1";
									} else {
										row[3] = testobject[2];
									}
									rows.add(row);
									conditionCounter++;
								} else {
									// String[] rowFalse = ;
									int eva = Integer.parseInt(testobject[2]);
									if (testobject[1].equals("FAIL") && eva == 100000) {
										rows.get(rows.size() - 1)[4] = "-1";
									} else {
										rows.get(rows.size() - 1)[4] = testobject[2];
									}
								}

							}
						} else {
							// System.out.println(t);
							// System.out.println(testObject);
						}
					}
				}
			}
			String outputfile = file.split("\\.")[0];
			PrintWriter pw = new PrintWriter(new File("CSV/" + outputfile + ".csv"));
			for (String[] row : rows) {
				String printer = printArray(row);
				pw.write(printer);
			}

			pw.close();
			System.out.println("done!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	// Method that creates a comma sperated line from an String Array
	private String printArray(String[] anArray) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < anArray.length; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append(anArray[i]);
		}
		sb.append('\n');
		return sb.toString();
	}
}
