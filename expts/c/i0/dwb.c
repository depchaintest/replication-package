
#include "stdio.h"

// not sure why these are hard to find :(*
// gcc finds them but csurf does not :(
//  ... the following are for csurf's benifit :) 
// Functions not defined in project:
//    __builtin_va_arg_pack
//    __finite
//    __fprintf_chk
//    __finitef
//    __finitel
//    __isnanf
//    __isnan
//    __isnanl

int __finite(double x) {return x == 0;}
int __finitef(float x) {return x == 0;}
int __finitel(long double x) {return x == 0;}
// int __isinf(double x) {return x == 0;}
// int __isinff(float x) {return x == 0;}
// int __isinfl(long double x) {return x == 0;}
int __isnan(double x) {return x == 0;}
int __isnanf(float x) {return x == 0;}
int __isnanl(long double x) {return x == 0;}


int __fprintf_chk(FILE * stream, int flag, const char * format, ...) {return 1;}
int __builtin_va_arg_pack () {return 1;}

