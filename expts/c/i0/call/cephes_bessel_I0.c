void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 1; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for cephes_bessel_I0");
	}

	// test object calling code
	cephes_bessel_I0((double) args[0]);
}
