/* config.h.  Generated from config.h.in by configure.  */
/* Configuration header file.
   Copyright (C) 1995, 1996, 1997, 1998 Free Software Foundation, Inc.

This file is part of gretl.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifndef CONFIG_H
#define CONFIG_H

/* for NLS use */
#define PACKAGE "gretl"

/* Native language support? */
#define ENABLE_NLS 1

/* Location of shared gretl files */
#define GRETL_PREFIX "/usr/local"

/* Building a reloctable package (Windows, OS X)? */
/* #undef PKGBUILD */

/* Extra floating-point GMP routines? */
/* #undef HAVE_MPFR */

/* Are we using openmp? */
/* #undef OPENMP_BUILD */

/* X-12-ARIMA support? */
#define HAVE_X12A 1

/* TRAMO/SEATS support? */
#define HAVE_TRAMO 1

/* Define if you want GNU readline support */
#define HAVE_READLINE 1

/* Define for GNU readline version >= 4.2 */
#define NEW_READLINE 1

/* Define if zlib is available */
#define HAVE_ZLIB 1

/* Define if using libgsf (>= 1.14.29) */
/* #undef USE_GSF */

/* Is LaTeX available? */
#define HAVE_LATEX 1

/* using gtk+ 3 */
/* #undef USE_GTK3 */

/* Using gtksourceview-2.0? */
/* #undef USE_GTKSOURCEVIEW_2 */

/* Using gtksourceview-3.0? */
/* #undef USE_GTKSOURCEVIEW_3 */

/* Does unistd.h have getdomainname? */
/* #undef GETDOMAINNAME */

/* Do we have stdint.h */
#define HAVE_STDINT_H 1

/* Do we have unistd.h */
#define HAVE_UNISTD_H 1

/* Do we have dirent.h */
#define HAVE_DIRENT_H 1

/* Do we have fnmatch.h */
#define HAVE_FNMATCH_H 1

/* Do we have sys/wait.h */
#define HAVE_SYS_WAIT_H 1

/* Do we have sys/times.h */
#define HAVE_SYS_TIMES_H 1

/* Do we have byteswap.h */
#define HAVE_BYTESWAP_H 1

/* Do we have libproc.h */
/* #undef HAVE_LIBPROC_H */

/* Do we have sys/proc_info.h */
/* #undef HAVE_SYS_PROC_INFO_H */

/* Do we have mmap */
#define HAVE_MMAP 1

/* Do we have vasprintf */
#define HAVE_VASPRINTF 1

/* Do we have posix_memalign */
#define HAVE_POSIX_MEMALIGN 1

/* Do we have the stuff needed for SMTP? */
#define HAVE_SYS_SOCKET_H 1
#define HAVE_NETDB_H 1
#define ENABLE_MAILER 1
#define HAVE_SOCKADDR_IN 1
#define HAVE_IN_ADDR 1

/* Define if the 'long double' type works.  */
#define HAVE_LONG_DOUBLE 1

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if your processor stores words with the most significant
   byte first (like Motorola and SPARC, unlike Intel and VAX).  */
/* #undef WORDS_BIGENDIAN */

/* The number of bytes in a int.  */
#define SIZEOF_INT 4

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define to use SSE2 instrinsics. */
#define USE_SSE2 1

/* Define to use AVX instrinsics. */
/* #undef USE_AVX */

/* Do we have immintrin.h? */
#define HAVE_IMMINTRIN_H 1

/* Enable audio graphs */
/* #undef HAVE_AUDIO */

/* Use flite for speech synthesis */
/* #undef HAVE_FLITE */

/* Have Jacobi SVD in Lapack 3.2 or higher */
#define HAVE_LAPACK_3_2 1

/* Use the GNU R shared library */
#define USE_RLIB 1

/* Location of R shared library */
#define RLIBPATH "/usr/lib/R/lib/libR.so"

/* Building for Mac OS X? */
/* #undef OS_OSX */

/* On Mac, using quartz GTK? */
/* #undef MAC_NATIVE */

#endif /* CONFIG_H */
