void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 5; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for pnorm_both");
	}

    double x1 = args[1];
    double y1 = args[2];
	// test object calling code
	pnorm_both(
		args[0],
		&x1,
		&y1,
		(int) args[3],
		(int) args[4]
    );
}
