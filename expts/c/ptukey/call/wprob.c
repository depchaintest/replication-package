void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 3; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for wprob");
	}

	// test object calling code
	wprob(
		args[0],
		args[1],
		args[2]
    );
}
