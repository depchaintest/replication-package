void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 6; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for ptukey");
	}

	// test object calling code
	ptukey(
		args[0],
		args[1],
		args[2],
		args[3],
		(int) args[4],
		(int) args[5]
    );
}
