#include "cephes_exp.h"
#include "CTestObjectHandle.h"
JNIEXPORT void JNICALL Java_org_iguanatool_casestudies_unity_testobjects_cephes_1exp_call(JNIEnv *env, jobject jobj, jdoubleArray args)
{
	initialize_and_call(env, jobj, args);
}

/*							unity.c
 *
 * Relative error approximations for function arguments near
 * unity.
 *
 *    log1p(x) = log(1+x)
 *    expm1(x) = exp(x) - 1
 *    cosm1(x) = cos(x) - 1
 *
 */

#include "mconf.h"
// ADDED BY ABDULLAH 
#include <math.h>
#include "stdio.h"

// not sure why these are hard to find :(*
// gcc finds them but csurf does not :(
//  ... the following are for csurf's benifit :) 
// Functions not defined in project:
//    __builtin_va_arg_pack
//    __finite
//    __fprintf_chk
//    __finitef
//    __finitel
//    __isnanf
//    __isnan
//    __isnanl

int __finite(double x) {return x == 0;}
int __finitef(float x) {return x == 0;}
int __finitel(long double x) {return x == 0;}
int __isinf(double x) {return x == 0;}
int __isinff(float x) {return x == 0;}
int __isinfl(long double x) {return x == 0;}
int __isnan(double x) {return x == 0;}
int __isnanf(float x) {return x == 0;}
int __isnanl(long double x) {return x == 0;}


int __fprintf_chk(FILE * stream, int flag, const char * format, ...) {return 1;}
int __builtin_va_arg_pack () {return 1;}

/* values gathering by running Beebe floating-point test suite */
#define BEEBE

#ifdef BEEBE
double MACHEP      = 2.22044604925031308e-16;
double UFLOWTHRESH = 2.22507385850720188e-308;
double MAXNUM      = 1.79769313486231571e+308;
#else
double MACHEP      = 1.11022302462515654042E-16;  /* 2**-53 */
double UFLOWTHRESH = 2.22507385850720138309E-308; /* 2**-1022 */
double MAXNUM      = 1.79769313486231570815E308;  /* 2**1024*(1-MACHEP) */
#endif

double MAXLOG =  7.08396418532264106224E2;     /* log 2**1022 */
double MINLOG = -7.08396418532264106224E2;     /* log 2**-1022 */

double PI     =  3.14159265358979323846;       /* pi */
double PIO2   =  1.57079632679489661923;       /* pi/2 */
double PIO4   =  7.85398163397448309616E-1;    /* pi/4 */
double SQRT2  =  1.41421356237309504880;       /* sqrt(2) */
double SQRTH  =  7.07106781186547524401E-1;    /* sqrt(2)/2 */
double LOG2E  =  1.4426950408889634073599;     /* 1/log(2) */
double SQ2OPI =  7.9788456080286535587989E-1;  /* sqrt( 2/pi ) */
double LOGE2  =  6.93147180559945309417E-1;    /* log(2) */
double LOGSQ2 =  3.46573590279972654709E-1;    /* log(2)/2 */
double THPIO4 =  2.35619449019234492885;       /* 3*pi/4 */
double TWOOPI =  6.36619772367581343075535E-1; /* 2/pi */

#ifndef INFINITY
double INFINITY = 1.0/0.0;
#endif

#ifndef NAN
double NAN = 0.0/0.0;
#endif

#ifdef MINUSZERO
double NEGZERO = -0.0;
#else
double NEGZERO = 0.0;
#endif

// END ADDITION

/* log1p(x) = log(1 + x)  */

/* Coefficients for log(1+x) = x - x**2/2 + x**3 P(x)/Q(x)
 * 1/sqrt(2) <= x < sqrt(2)
 * Theoretical peak relative error = 2.32e-20
 */

static double LP[] = {
 4.5270000862445199635215E-5,
 4.9854102823193375972212E-1,
 6.5787325942061044846969E0,
 2.9911919328553073277375E1,
 6.0949667980987787057556E1,
 5.7112963590585538103336E1,
 2.0039553499201281259648E1,
};

static double LQ[] = {
/* 1.0000000000000000000000E0,*/
 1.5062909083469192043167E1,
 8.3047565967967209469434E1,
 2.2176239823732856465394E2,
 3.0909872225312059774938E2,
 2.1642788614495947685003E2,
 6.0118660497603843919306E1,
};

#define SQRTH 0.70710678118654752440
#define SQRT2 1.41421356237309504880

double cephes_log (double x)
{
    double z = 1.0 + x;

    if (z < SQRTH || z > SQRT2) {
	return log(z);
    }

    z = x*x;
    z = -0.5 * z + x * (z * polevl(x, LP, 6) / p1evl(x, LQ, 6));

    return x + z;
}

/* expm1(x) = exp(x) - 1  */

/*  e^x =  1 + 2x P(x^2)/( Q(x^2) - P(x^2) )
 * -0.5 <= x <= 0.5
 */

static double EP[3] = {
 1.2617719307481059087798E-4,
 3.0299440770744196129956E-2,
 9.9999999999999999991025E-1,
};

static double EQ[4] = {
 3.0019850513866445504159E-6,
 2.5244834034968410419224E-3,
 2.2726554820815502876593E-1,
 2.0000000000000000000897E0,
};

double cephes_exp (double x)
{
  printf("x value 1 = %Lf\n", x);
    double r, xx;

    if (node(2,  is_true(0, isnan(x)) )) {
	return x;
    }
  printf("x value 2 = %f\n", x);
    if (node(4,  is_true(0, !isfinite(x)) )) {
	return (x < 0)? -1.0 : x;
    }
  printf("x value 3 = %f\n", x);
    if (node(6,  less_than(0, x, -0.5)  || greater_than(1, x, 0.5) )) {
	return exp(x) - 1.0;
    }
  printf("x value 4 = %f\n", x);
    xx = x * x;
    r = x * polevl(xx, EP, 2);
    r = r/(polevl(xx, EQ, 3) - r);

    return r + r;
}

/* cosm1(x) = cos(x) - 1  */

static double coscof[7] = {
 4.7377507964246204691685E-14,
-1.1470284843425359765671E-11,
 2.0876754287081521758361E-9,
-2.7557319214999787979814E-7,
 2.4801587301570552304991E-5,
-1.3888888888888872993737E-3,
 4.1666666666666666609054E-2,
};

double cosm1 (double x)
{
    double xx;

    if (x < -PIO4 || x > PIO4) {
	return cos(x) - 1.0;
    }

    xx = x * x;
    xx = -0.5*xx + xx * xx * polevl(xx, coscof, 6);

    return xx;
}

double polevl (double x, double *coef, int N)
{
    double ans;
    int i;
    double *p;

    p = coef;
    ans = *p++;
    i = N;

    do {
	ans = ans * x + *p++;
    } while (--i);

    return ans;
}


void perform_call(double* args, int num_args)
{
  // printf("Args[0] value = %d, num_args = %d\n", args[0], num_args );
	// declarations
	const int NUM_ARGS = 1; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for cephes_exp");
	}

	// test object calling code
	cephes_exp(args[0]);
}

