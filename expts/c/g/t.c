
// stubs used with hampering experiment

int __finite(double x) {return x == 0;}
int __finitef(float x) {return x == 0;}
int __finitel(long double x) {return x == 0;}
int __isinf(double x) {return x == 0;}
int __isinff(float x) {return x == 0;}
int __isinfl(long double x) {return x == 0;}
int __isnan(double x) {return x == 0;}
int __isnanf(float x) {return x == 0;}
int __isnanl(long double x) {return x == 0;}
int utime(char *a, char *b) {return *a + *b;}
int getopt_long(int argc, char *const argv[], char *optstring, char *longopts, int *longindex)
    {return argc + **argv + *optstring + *longopts + *longindex;}
char *bindtextdomain (const char * domainname, const char * dirname)
    {return *domainname + *dirname;}
char * textdomain (char * domainname) {return domainname;}
int getpagesize(void) {return 42;}
char *gettext (char * msgid) {return msgid;}
void *mmap(char *start, int length, int prot, int flags, int fd, int offset)
    {return *start + length + prot + flags + fd + offset;}
void obstack_free (char *x, void *y) {}
void _obstack_newchunk(void* x, int y) {}
int _obstack_begin(char *o , int x, int y, void * x1, void *x2)
    {return *o + x + y;}

    


