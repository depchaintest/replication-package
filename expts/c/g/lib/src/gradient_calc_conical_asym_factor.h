/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor */

#ifndef _Included_org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor
#define _Included_org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor
#ifdef __cplusplus
extern "C" {
#endif
#undef org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor_TEST_OBJECT_ID
#define org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor_TEST_OBJECT_ID 1L
/*
 * Class:     org_iguanatool_casestudies_g_testobjects_gradient_calc_conical_asym_factor
 * Method:    call
 * Signature: ([D)V
 */
JNIEXPORT void JNICALL Java_org_iguanatool_casestudies_g_testobjects_gradient_1calc_1conical_1asym_1factor_call
  (JNIEnv *, jobject, jdoubleArray);

#ifdef __cplusplus
}
#endif
#endif
