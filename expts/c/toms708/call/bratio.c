void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 8; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for bratio");
	}
    int args6 = (int) args[6];
	// test object calling code
	bratio(
		(double) args[0],
		(double) args[1],
		(double) args[2],
		(double) args[3],
		&args[4],
		&args[5],
        &args6,
        (int) args[7]
    );
}
