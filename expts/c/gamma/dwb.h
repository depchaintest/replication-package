
// https://software.intel.com/en-us/articles/floating-point-operation-result-out-of-range-in-static-initialization/

double NA_REAL = { 0.0L / 0.0L };
double R_PosInf = 1.0L / 0.0L;
double R_NegInf = -1.0L / 0.0L;

// a guess
double R_NaN = { 0.0L / 0.0L };



// rather than sorting out the needs of ../../src/main/errors.c
void Rf_warning(const char *format, ...) { } 
void R_CheckUserInterrupt(void) {}


// not sure why these are hard to find :(*
// gcc finds them but csurf does not :(
//  ... the following are for csurf's benifit :) 
// Functions not defined in project:
//    __finitef
//    __finitel
//    __isnanf
//    __isnanl
//    __finite
//    gettext
//    __isnan

//char *gettext(char *s) {return s;}

int R_finite(double x) {return x == 0;}
int __finitef(float x) {return x == 0;}
int __finitel(long double x) {return x == 0;}
// int __isinf(double x) {return x == 0;}
// int __isinff(float x) {return x == 0;}
// int __isinfl(long double x) {return x == 0;}
int __isnan(double x) {return x == 0;}
int __isnanf(float x) {return x == 0;}
int __isnanl(long double x) {return x == 0;}

