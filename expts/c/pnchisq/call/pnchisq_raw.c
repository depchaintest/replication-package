void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 7; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for pnchisq_raw");
	}
    

	// test object calling code
	pnchisq_raw(
		args[0],
		args[1],
		args[2],
		args[3],
		args[4],
		(int) args[5],
		(int) args[6]
    );
}
