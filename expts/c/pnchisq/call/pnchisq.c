void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 5; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for pnchisq");
	}

	// test object calling code
	pnchisq(
		args[0],
		args[1],
		args[2],
		(int) args[3],
		(int) args[4]
    );
}
