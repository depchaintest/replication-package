void perform_call(double* args, int num_args)
{
	// declarations
	const int NUM_ARGS = 7; // replace -1 with correct value

	// check correct number of arguments
	if (num_args != NUM_ARGS) {
		native_error("Wrong number of generated inputs for clip_to_circle");
	}
    int x1 = (int) args[0];
    int y1 = (int) args[1];
    int x2 = (int) args[2];
    int y2 = (int) args[3];
	// test object calling code
	clip_to_circle(
        &x1,
		&y1,
		&x2,
		&y2,
		(int) args[4],
		(int) args[5],
        (int) args[6]);
}
