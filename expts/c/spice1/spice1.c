/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1986 Wayne A. Christopyher, U. C. Berkeley CAD Group
Author: 1982 Giles Billingsley
**********/

/*
 * Some routines to do clipping of polygons, etc to boxes.  Most of this code
 * was rescued from MFB:
 *  sccsid "@(#)mfbclip.c   1.2  12/21/83"
 */

#include "math.h"

#ifndef	M_PI
#  define M_PI		3.14159265358979323846
#endif

typedef int bool;

#define false 0
#define true 1

/* This routine will clip a line to a circle, returning true if the line
 * is entirely outside the circle.  Note that we have to be careful not
 * to switch the points around, since in grid.c we need to know which is
 * the outer point for putting the label on.
 */

bool
clip_to_circle(int *x1, int *y1, int *x2, int *y2, int cx, int cy, int rad)
{
    double perplen, a, b, c;
    double tx, ty, dt;
    double dtheta;
    double theta1, theta2, tt, alpha, beta, gamma;
    bool flip = false;
    int i;

    /* Get the angles between the origin and the endpoints. */
    if ((*x1-cx) || (*y1-cy))
	theta1 = atan2((double) *y1 - cy, (double) *x1 - cx);
    else
	theta1 = M_PI;
    if ((*x2-cx) || (*y2-cy))
	theta2 = atan2((double) *y2 - cy, (double) *x2 - cx);
    else
	theta2 = M_PI;

    if (theta1 < 0.0)
        theta1 = 2 * M_PI + theta1;
    if (theta2 < 0.0)
        theta2 = 2 * M_PI + theta2;

    dtheta = theta2 - theta1;
    if (dtheta > M_PI)
        dtheta = dtheta - 2 * M_PI;
    else if (dtheta < - M_PI)
        dtheta = 2 * M_PI - dtheta;

    /* Make sure that p1 is the first point */
    if (dtheta < 0) {
        tt = theta1;
        theta1 = theta2;
        theta2 = tt;
        i = *x1;
        *x1 = *x2;
        *x2 = i;
        i = *y1;
        *y1 = *y2;
        *y2 = i;
        flip = true;
        dtheta = -dtheta;
    }

    /* Figure out the distances between the points */
    a = sqrt((double) ((*x1 - cx) * (*x1 - cx) + (*y1 - cy) * (*y1 - cy)));
    b = sqrt((double) ((*x2 - cx) * (*x2 - cx) + (*y2 - cy) * (*y2 - cy)));
    c = sqrt((double) ((*x1 - *x2) * (*x1 - *x2) +
            (*y1 - *y2) * (*y1 - *y2)));

    /* We have three cases now -- either the midpoint of the line is
     * closest to the origon, or point 1 or point 2 is.  Actually the
     * midpoint won't in general be the closest, but if a point besides
     * one of the endpoints is closest, the midpoint will be closer than
     * both endpoints.
     */
    tx = (*x1 + *x2) / 2;
    ty = (*y1 + *y2) / 2;
    dt = sqrt((double) ((tx - cx) * (tx - cx) + (ty - cy) * (ty - cy)));
    if ((dt < a) && (dt < b)) {
        /* This is wierd -- round-off errors I guess. */
        tt = (a * a + c * c - b * b) / (2 * a * c);
        if (tt > 1.0)
            tt = 1.0;
        else if (tt < -1.0)
            tt = -1.0;
        alpha = acos(tt);
        perplen = a * sin(alpha);
    } else if (a < b) {
        perplen = a;
    } else {
        perplen = b;
    }

    /* Now we should see if the line is outside of the circle */
    if (perplen >= rad)
        return (true);

    /* It's at least partially inside */
    if (a > rad) {
        tt = (a * a + c * c - b * b) / (2 * a * c);
        if (tt > 1.0)
            tt = 1.0;
        else if (tt < -1.0)
            tt = -1.0;
        alpha = acos(tt);
        gamma = asin(sin(alpha) * a / rad);
        if (gamma < M_PI / 2)
            gamma = M_PI - gamma;
        beta = M_PI - alpha - gamma;
        *x1 = (int) (cx + rad * cos(theta1 + beta) + 0.5);
        *y1 = (int) (cy + rad * sin(theta1 + beta) + 0.5);
    }
    if (b > rad) {
        tt = (c * c + b * b - a * a) / (2 * b * c);
        if (tt > 1.0)
            tt = 1.0;
        else if (tt < -1.0)
            tt = -1.0;
        alpha = acos(tt);
        gamma = asin(sin(alpha) * b / rad);
        if (gamma < M_PI / 2)
            gamma = M_PI - gamma;
        beta = M_PI - alpha - gamma;
        *x2 = (int) (cx + rad * cos(theta2 - beta) + 0.5);
        *y2 = (int) (cy + rad * sin(theta2 - beta) + 0.5);
    }
    if (flip) {
        i = *x1;
        *x1 = *x2;
        *x2 = i;
        i = *y1;
        *y1 = *y2;
        *y2 = i;
    }
    return (false);
}

// double
// cliparc(double cx, double cy, double rad, double start, double end, int iclipx, int iclipy, int icliprad, int flag)
// {
//     double clipx, clipy, cliprad;
//     double x, y, tx, ty, dist;
//     double alpha, theta, phi, a1, a2, d, l;
//     double sclip = 0, eclip = 0;
//     bool in;

//     clipx = (double) iclipx;
//     clipy = (double) iclipy;
//     cliprad = (double) icliprad;
//     x = cx - clipx;
//     y = cy - clipy;
//     dist = sqrt((double) (x * x + y * y));

//     if (!rad || !cliprad)
//         return(-1);
//     if (dist + rad < cliprad) {
//         /* The arc is entirely in the boundary. */
//         //DrawArc((int)cx, (int)cy, (int)rad, start, end);
//         return(flag?start:end);
//     } else if ((dist - rad >= cliprad) || (rad - dist >= cliprad)) {
//         /* The arc is outside of the boundary. */
//         return(-1);
//     }
//     /* Now let's figure out the angles at which the arc crosses the
//      * circle. We know dist != 0.
//      */
//     if (x)
//         phi = atan2((double) y, (double) x);
//     else if (y > 0)
//         phi = M_PI * 1.5;
//     else
//         phi = M_PI / 2;
//     if (cx > clipx)
//         theta = M_PI + phi;
//     else
//         theta = phi;

//     alpha = (double) (dist * dist + rad * rad - cliprad * cliprad) /
//             (2 * dist * rad);

//     /* Sanity check */
//     if (alpha > 1.0)
// 	alpha = 0.0;
//     else if (alpha < -1.0)
// 	alpha = M_PI;
//     else
//         alpha = acos(alpha);

//     a1 = theta + alpha;
//     a2 = theta - alpha;
//     while (a1 < 0)
//         a1 += M_PI * 2;
//     while (a2 < 0)
//         a2 += M_PI * 2;
//     while (a1 >= M_PI * 2)
//         a1 -= M_PI * 2;
//     while (a2 >= M_PI * 2)
//         a2 -= M_PI * 2;

//     tx = cos(start) * rad + x;
//     ty = sin(start) * rad + y;
//     d = sqrt((double) tx * tx + ty * ty);
//     in = (d > cliprad) ? false : true;

//     /* Now begin with start.  If the point is in, draw to either end, a1,
//      * or a2, whichever comes first.
//      */
//     d = M_PI * 3;
//     if ((end < d) && (end > start))
//         d = end;
//     if ((a1 < d) && (a1 > start))
//         d = a1;
//     if ((a2 < d) && (a2 > start))
//         d = a2;
//     if (d == M_PI * 3) {
//         d = end;
//         if (a1 < d)
//             d = a1;
//         if (a2 < d)
//             d = a2;
//     }

//     if (in) {
// 	if (start > d) {
// 	    double tmp;
// 	    tmp = start;
// 	    start = d;
// 	    d = tmp;
// 	}
//         //DrawArc((int)cx, (int)cy, (int)rad, start, d);
// 	sclip = start;
// 	eclip = d;
//     }
//     if (d == end)
//         return(flag?sclip:eclip);
//     if (a1 != a2)
//         in = in ? false : true;

//     /* Now go from here to the next point. */
//     l = d;
//     d = M_PI * 3;
//     if ((end < d) && (end > l))
//         d = end;
//     if ((a1 < d) && (a1 > l))
//         d = a1;
//     if ((a2 < d) && (a2 > l))
//         d = a2;
//     if (d == M_PI * 3) {
//         d = end;
//         if (a1 < d)
//             d = a1;
//         if (a2 < d)
//             d = a2;
//     }

//     if (in) {
//         //DrawArc((int)cx, (int)cy, (int)rad, l, d);
// 	sclip = l;
// 	eclip = d;
//     }
//     if (d == end)
//         return(flag?sclip:eclip);
//     in = in ? false : true;
    
//     /* And from here to the end. */
//     if (in) {
//         //DrawArc((int)cx, (int)cy, (int)rad, d, end);
// 	/* special case */
// 	if (flag != 2) {
// 	  sclip = d;
// 	  eclip = end;
// 	}
//     }
//     return(flag%2?sclip:eclip);
// }
