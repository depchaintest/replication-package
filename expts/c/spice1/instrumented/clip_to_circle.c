/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1986 Wayne A. Christopyher, U. C. Berkeley CAD Group
Author: 1982 Giles Billingsley
**********/

/*
 * Some routines to do clipping of polygons, etc to boxes.  Most of this code
 * was rescued from MFB:
 *  sccsid "@(#)mfbclip.c   1.2  12/21/83"
 */

#include "math.h"

#ifndef	M_PI
#  define M_PI		3.14159265358979323846
#endif

typedef int bool;

#define false 0
#define true 1

/* This routine will clip a line to a circle, returning true if the line
 * is entirely outside the circle.  Note that we have to be careful not
 * to switch the points around, since in grid.c we need to know which is
 * the outer point for putting the label on.
 */

bool
clip_to_circle(int *x1, int *y1, int *x2, int *y2, int cx, int cy, int rad)
{
    double perplen, a, b, c;
    double tx, ty, dt;
    double dtheta;
    double theta1, theta2, tt, alpha, beta, gamma;
    bool flip = false;
    int i;

    /* Get the angles between the origin and the endpoints. */
    if (node(7,  is_true(0, (*x1-cx))  || is_true(1, (*y1-cy)) ))
	theta1 = atan2((double) *y1 - cy, (double) *x1 - cx);
    else
	theta1 = M_PI;
    if (node(10,  is_true(0, (*x2-cx))  || is_true(1, (*y2-cy)) ))
	theta2 = atan2((double) *y2 - cy, (double) *x2 - cx);
    else
	theta2 = M_PI;

    if (node(13,  less_than(0, theta1, 0.0) ))
        theta1 = 2 * M_PI + theta1;
    if (node(15,  less_than(0, theta2, 0.0) ))
        theta2 = 2 * M_PI + theta2;

    dtheta = theta2 - theta1;
    if (node(18,  greater_than(0, dtheta, M_PI) ))
        dtheta = dtheta - 2 * M_PI;
    else if (node(20,  less_than(0, dtheta, - M_PI) ))
        dtheta = 2 * M_PI - dtheta;

    /* Make sure that p1 is the first point */
    if (node(22,  less_than(0, dtheta, 0) )) {
        tt = theta1;
        theta1 = theta2;
        theta2 = tt;
        i = *x1;
        *x1 = *x2;
        *x2 = i;
        i = *y1;
        *y1 = *y2;
        *y2 = i;
        flip = true;
        dtheta = -dtheta;
    }

    /* Figure out the distances between the points */
    a = sqrt((double) ((*x1 - cx) * (*x1 - cx) + (*y1 - cy) * (*y1 - cy)));
    b = sqrt((double) ((*x2 - cx) * (*x2 - cx) + (*y2 - cy) * (*y2 - cy)));
    c = sqrt((double) ((*x1 - *x2) * (*x1 - *x2) +
            (*y1 - *y2) * (*y1 - *y2)));

    /* We have three cases now -- either the midpoint of the line is
     * closest to the origon, or point 1 or point 2 is.  Actually the
     * midpoint won't in general be the closest, but if a point besides
     * one of the endpoints is closest, the midpoint will be closer than
     * both endpoints.
     */
    tx = (*x1 + *x2) / 2;
    ty = (*y1 + *y2) / 2;
    dt = sqrt((double) ((tx - cx) * (tx - cx) + (ty - cy) * (ty - cy)));
    if (node(40,  is_true(0, (dt < a))  && is_true(1, (dt < b)) )) {
        /* This is wierd -- round-off errors I guess. */
        tt = (a * a + c * c - b * b) / (2 * a * c);
        if (node(42,  greater_than(0, tt, 1.0) ))
            tt = 1.0;
        else if (node(44,  less_than(0, tt, -1.0) ))
            tt = -1.0;
        alpha = acos(tt);
        perplen = a * sin(alpha);
    } else if (node(48,  less_than(0, a, b) )) {
        perplen = a;
    } else {
        perplen = b;
    }

    /* Now we should see if the line is outside of the circle */
    if (node(51,  greater_than_or_equal(0, perplen, rad) ))
        return (true);

    /* It's at least partially inside */
    if (node(53,  greater_than(0, a, rad) )) {
        tt = (a * a + c * c - b * b) / (2 * a * c);
        if (node(55,  greater_than(0, tt, 1.0) ))
            tt = 1.0;
        else if (node(57,  less_than(0, tt, -1.0) ))
            tt = -1.0;
        alpha = acos(tt);
        gamma = asin(sin(alpha) * a / rad);
        if (node(61,  less_than(0, gamma, M_PI / 2) ))
            gamma = M_PI - gamma;
        beta = M_PI - alpha - gamma;
        *x1 = (int) (cx + rad * cos(theta1 + beta) + 0.5);
        *y1 = (int) (cy + rad * sin(theta1 + beta) + 0.5);
    }
    if (node(66,  greater_than(0, b, rad) )) {
        tt = (c * c + b * b - a * a) / (2 * b * c);
        if (node(68,  greater_than(0, tt, 1.0) ))
            tt = 1.0;
        else if (node(70,  less_than(0, tt, -1.0) ))
            tt = -1.0;
        alpha = acos(tt);
        gamma = asin(sin(alpha) * b / rad);
        if (node(74,  less_than(0, gamma, M_PI / 2) ))
            gamma = M_PI - gamma;
        beta = M_PI - alpha - gamma;
        *x2 = (int) (cx + rad * cos(theta2 - beta) + 0.5);
        *y2 = (int) (cy + rad * sin(theta2 - beta) + 0.5);
    }
    if (node(79,  is_true(0, flip) )) {
        i = *x1;
        *x1 = *x2;
        *x2 = i;
        i = *y1;
        *y1 = *y2;
        *y2 = i;
    }
    return (false);
}