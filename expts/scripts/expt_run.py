import os
from subprocess import Popen, STDOUT, TimeoutExpired, PIPE, check_output
import time
from random import randint
from os import killpg
from signal import SIGKILL
from datetime import datetime

cmd1 = 'export CLASSPATH="$IGUANA_HOME/target/iguanatool-1.0-jar-with-dependencies.jar":$CLASSPATH'
check_output(cmd1, shell=True)
time.sleep(2)

# Case studies/subjects and test data generators
casestudies = ["spice1", "pnorm", "pnchisq", "i0", "k0", "g", "ptukey", "rhyper", "unity", "toms708", "gamma", "cody"]
techinques = ["dcEvolve", "nhc", "random"]

print("###################################################")

MAX_WAIT = 600
for case in casestudies:
    for technique in techinques:
        for seed in range(1, 31):
            cmd = "java org.iguanatool.Run "+ case + " " + technique + " -seed=" + str(seed)
            with Popen(cmd,
               shell=True,
               stdout=PIPE,
               stderr=STDOUT,
               close_fds=True,
               universal_newlines=True,
               start_new_session=True) as proc:
                    try:
                        stdout = proc.communicate(timeout=MAX_WAIT)[0]
                    except TimeoutExpired:
                        print("Timeout = " + cmd)
                        killpg(proc.pid, SIGKILL)
                        stdout = proc.communicate(timeout=1)[0]
                    except Exception as err:
                        print("Error = " + e.output)
                        killpg(proc.pid, SIGKILL)
                    else:
                        print("DONE = " + cmd)
                    print('# Return code: {}'.format(proc.returncode))
                    print(stdout)

            cmd = "java org.iguanatool.ext.Run "+ case + "--"+ technique + "--"+ str(seed) + "--results.tsv"
            with Popen(cmd,
               shell=True,
               stdout=PIPE,
               stderr=STDOUT,
               close_fds=True,
               universal_newlines=True,
               start_new_session=True) as proc:
                    try:
                        stdout = proc.communicate(timeout=15)[0]
                    except TimeoutExpired:
                        print("Timeout = " + cmd)
                        killpg(proc.pid, SIGKILL)
                        stdout = proc.communicate(timeout=1)[0]
                    except Exception as err:
                        print("Error = " + e.output)
                        killpg(proc.pid, SIGKILL)
                    else:
                        print("DONE = " + cmd)
                    print('# Return code: {}'.format(proc.returncode))
                    print(stdout)
