#!/bin/bash
#
# Aggregates testability data, filters path count data, and joins into one file per (object, method, edge-type) tuple.
./aggregate_test_results.sh ../raw-results/small/CSV --median
./filter_path_data.sh gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1
./join_csvs.sh
mv *--joined.csv Smaller/Median

./aggregate_test_results.sh ../raw-results/small/CSV
./filter_path_data.sh gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1
./join_csvs.sh
mv *--joined.csv Smaller/Mean

./aggregate_test_results.sh ../raw-results/large/CSV --median
./filter_path_data.sh gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1
./join_csvs.sh
mv *--joined.csv Larger/Median

./aggregate_test_results.sh ../raw-results/large/CSV
./filter_path_data.sh gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1
./join_csvs.sh
mv *--joined.csv Larger/Mean

# Only needed to count matched predicates
# ./filter_matched_functions.sh gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1

