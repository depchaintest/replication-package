#!/bin/bash
#
# Uses q (http://harelba.github.io/q/) to join CSV files containing
# path data and test generation data using an additional file containing
# the matching between rows in those files.

for FILE in gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1; do
    for EDGES in control data both; do
	for METHOD in dcEvolve nhc random; do
	    echo $FILE $EDGES $METHOD
	    PATHS_FILE="${FILE}_paths_${EDGES}.csv"
	    MATCH_FILE="${FILE}_match.csv"
	    TESTS_FILE="${FILE}--${METHOD}--all--results.csv"
	    OUTPUT_FILE="${FILE}--${METHOD}--${EDGES}--joined.csv"

	    if [ -e "$TESTS_FILE" ]; then
		q -d ',' -O -H "SELECT File,Function,Start,End,ConditionNo,node,EdgeType,Estimate,Min,Max,Count,Mean,TrueEvaluations,FalseEvaluations,CASE WHEN TrueEvaluations < 0 THEN FalseEvaluations WHEN FalseEvaluations < 0 THEN TrueEvaluations WHEN TrueEvaluations <= FalseEvaluations THEN TrueEvaluations WHEN TrueEvaluations > FalseEvaluations THEN FalseEvaluations END AS MinEvals,'$METHOD' AS Method FROM $PATHS_FILE paths JOIN $MATCH_FILE match ON (paths.Start=MStart AND paths.End=MEnd AND paths.File=MFile AND paths.Function=MFunction) JOIN $TESTS_FILE tests ON (tests.TestObject=MTestObject AND tests.ConditionNo=MConditionNo AND tests.node=Mnode)" > $OUTPUT_FILE
	    fi
	done
    done
done
