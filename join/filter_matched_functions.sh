#!/bin/bash
#
# Checks line counts for test generation data and path filtering.
# Uses test_inputs.txt to determine how to match and filter files.

INPUT_DIR="."
OUTPUT_DIR="."

for INPUT in $*; do
    PATH_FILE=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 2`
    NESTING=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 3`
    SOURCE_FILE=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 4`
    MATCH_FILE="${INPUT}_match.csv"

    echo "=== $INPUT ==="
    echo $PATH_FILE
    echo $NESTING
    echo $SOURCE_FILE

    q -d ',' -O -H "SELECT DISTINCT MFunction from $MATCH_FILE" > ${OUTPUT_DIR}/${INPUT}_functions.csv
    for TYPE in both control data; do
	q -d ',' -O -H "SELECT VertexID,Start,End,File,Function,Source FROM ${INPUT_DIR}/${INPUT}_paths_${TYPE}.csv WHERE File=\"${SOURCE_FILE}\" AND Function IN (SELECT MFunction FROM ${INPUT}_functions.csv)" > ${OUTPUT_DIR}/${INPUT}_paths_filtered_${TYPE}.csv

	q -d ',' -O -H "SELECT VertexID,Start,End,File,Function,Source FROM ${INPUT_DIR}/${INPUT}_paths_filtered_${TYPE}.csv WHERE File=\"${SOURCE_FILE}\" AND Start NOT IN (SELECT MStart FROM ${INPUT}_match.csv)" > ${OUTPUT_DIR}/${INPUT}_paths_unmatched_${TYPE}.csv
    done

    q -d ',' -O -H "SELECT VertexID,paths.Start,paths.End,paths.File,paths.Function,Source,TrueEvaluations,FalseEvaluations FROM ${OUTPUT_DIR}/${INPUT}_paths_filtered_both.csv paths JOIN ${INPUT_DIR}/${INPUT}--dcEvolve--both--joined.csv tests ON (paths.Start=tests.Start AND paths.End=tests.End AND paths.File=tests.File AND paths.Function=tests.Function) WHERE VertexID NOT IN (SELECT VertexID FROM ${OUTPUT_DIR}/${INPUT}_paths_filtered_data.csv) " > ${OUTPUT_DIR}/${INPUT}_no_data_paths.csv
done
