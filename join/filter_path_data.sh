#!/bin/bash
#
# Checks line counts for test generation data and path filtering.
# Uses test_inputs.txt to determine how to match and filter files.

PATH_DATA_DIR="Path_Data"
OUTPUT_DIR="."

for INPUT in $*; do
    PATH_FILE=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 2`
    NESTING=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 3`
    SOURCE_FILE=`grep -e "^${INPUT}," test_inputs.txt | cut -d',' -f 4`

    echo "=== $INPUT ==="
    echo $PATH_FILE
    echo $NESTING
    echo $SOURCE_FILE

    java -cp process_data.jar FilterPathResults BO ALLVX $NESTING $SOURCE_FILE < ${PATH_DATA_DIR}/${PATH_FILE}.csv > ${OUTPUT_DIR}/${INPUT}_paths_both.csv
    java -cp process_data.jar FilterPathResults CO ALLVX $NESTING $SOURCE_FILE < ${PATH_DATA_DIR}/${PATH_FILE}.csv > ${OUTPUT_DIR}/${INPUT}_paths_control.csv
    java -cp process_data.jar FilterPathResults DO ALLVX $NESTING $SOURCE_FILE < ${PATH_DATA_DIR}/${PATH_FILE}.csv > ${OUTPUT_DIR}/${INPUT}_paths_data.csv
done
