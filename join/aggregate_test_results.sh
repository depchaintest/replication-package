#!/bin/bash
# new script for more complete Sep 2017 data

# The directory where the test data is and where the aggregated test
# data will be written
TEST_DIR="$1"
MEDIAN="$2"

if [ "$TEST_DIR" == "" ]; then
    echo "USAGE: $0 testability-data-directory [--median]"
    exit 1
fi

OUTPUT_DIR="."

# process the files that have data for all three methods (which is all now)
for FILE in gamma cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1; do 
    for METHOD in dcEvolve nhc random; do
	java -cp process_data.jar AggregateTestingResults -X 1 30 ${TEST_DIR}/$FILE--$METHOD--X--results.csv $MEDIAN > ${OUTPUT_DIR}/$FILE--$METHOD--all--results.csv
    done
done

# rename cody to gamma_cody for consistency with path data
mv ${OUTPUT_DIR}/cody--dcEvolve--all--results.csv ${OUTPUT_DIR}/gamma_cody--dcEvolve--all--results.csv 
mv ${OUTPUT_DIR}/cody--nhc--all--results.csv ${OUTPUT_DIR}/gamma_cody--nhc--all--results.csv 
mv ${OUTPUT_DIR}/cody--random--all--results.csv ${OUTPUT_DIR}/gamma_cody--random--all--results.csv 

