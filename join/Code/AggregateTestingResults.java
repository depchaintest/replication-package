import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 * An aggregation (min and mean) of data that accounts for -1 as a flag for "invalid".
 */
class Aggregate
{
    private int validCount;
    private int totalCount;

    private double sum;
    private double min = Double.POSITIVE_INFINITY;
    private List<Double> data = new ArrayList<Double>();
    
    /**
     * Adds the item into this aggregation.
     *
     * @param datum an integer, with -1 meaning "invalid" or "infinity"
     */
    public void add(int datum)
    {
	totalCount++;

	if (datum != -1)
	    {
		validCount++;
		sum += datum;

		min = Math.min(min, datum);
		data.add((double)datum);
	    }
	else
	    {
		data.add(Double.POSITIVE_INFINITY);
	    }
    }

    /**
     * Returns the minimum of all the valid data, or infinity if there is no valid data.
     */
    public double getMin()
    {
	return min;
    }

    /**
     * Returns the mean of the valid data, or -1 if there is no valid data.
     */
    public double averageOfValid()
    {
	if (validCount > 0)
	    {
		return sum / validCount;
	    }
	else
	    {
		return -1;
	    }
    }

    /**
     * Returns the mean of the data, which is -1 if any data is invalid.
     */
    public double average()
    {
	if (allValid())
	    {
		return averageOfValid();
	    }
	else
	    {
		return -1;
	    }
    }

    /**
     * Returns the median of all the data.  Returns NaN if there is no data.
     */
    public double median()
    {
	if (totalCount == 0)
	    {
		return Double.NaN;
	    }
	else
	    {
		Collections.sort(data);
		double med = (data.get(totalCount / 2) + data.get((totalCount - 1) / 2)) / 2;
		if (med == Double.POSITIVE_INFINITY)
		    {
			return -1;
		    }
		else
		    {
			return med;
		    }
	    }
    }

    /**
     * Determines if all data is valid.
     */
    public boolean allValid()
    {
	return totalCount == validCount;
    }
}


/**
 * Aggregates values from several input files containing results of generating tests
 * for predicates.
 *
 * @version 0.3 2017-09-12 added output of min average
 * @version 0.2 2017-08-15 added option to control how to handle -1, changed to output averages
 * @version 0.1 2017-06-10 cloudy over the North Atlantic
 *
 * TO DO
 * do CSV parsing properly to account for commas and quotes in code strings
 */

public class AggregateTestingResults
{
    private enum Mode {ZERO, INFINITY}; 

    /**
     * Determines if any of the scanners in the given array have a next line
     * of input available.
     *
     * @param inputs an array of non-NULL Scanners, non-NULL
     * @return true if any Scanner has a next line, false otherwise
     */
    private static boolean anyNextLine(Scanner[] inputs)
    {
	int i = 0;
	while (i < inputs.length && !inputs[i].hasNextLine())
	    {
		i++;
	    }
	return i < inputs.length;
    }

    // arguments are a list of files or a placeholder string, a range of integers to replace
    // the placeholder with, and a template with the placeholder in it that will form a filename
    // when the palceholder is replaced with an integer from the range
    private static final String USAGE = "USAGE: java AggregateTestingResults {file [file...] | --placeHolder from to template} [--median]";

	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.err.println(USAGE);
			return;
		}

		boolean showMedian, showMean;

		if (args[args.length - 1].equals("--median"))
		    {
			showMedian = true;
			showMean = false;
		    }
		else
		    {
			showMedian = false;
			showMean = true;
		    }

		/*
		// Not used in current code
		Mode mode = Mode.ZERO;
		if (args[0].equals("-zero") || args[0].equals("-infinity"))
		    {
			if (args.length == 1)
			    {
				System.err.println(USAGE);
				return;
			    }

			mode = (args[0].equals("-zero") ? Mode.ZERO : Mode.INFINITY);

			// shift arguments
			String[] nargs = new String[args.length - 1];
			for (int i = 0; i < nargs.length; i++)
			    {
				nargs[i] = args[i + 1];
			    }
			args = nargs;
		    }
		*/
		
		// determine if all filenames are given on command line or if using a placeholder;
		// default to filenames are arguments
		String[] fnames = args;
		
		if (args[0].charAt(0) == '-')
		{
		    // first argument gave placeholder; second and third give range of integers
		    // to replace placeholder with; fourth gives string with placeholder
			String placeHolder = args[0].substring(1);
			int from = Integer.parseInt(args[1]);
			int to = Integer.parseInt(args[2]);
			String fname = args[3];
			
			fnames = new String[to - from + 1];
			for (int i = from; i <= to; i++)
			{
				int pos = fname.indexOf(placeHolder);
				
				if (pos == -1)
				{
					System.err.println("Placeholder " + placeHolder + " not found in template " + fname);
					System.exit(1);
				}
				
				fnames[i - from] = fname.substring(0, pos) + i + fname.substring(pos + placeHolder.length());
			}
		}
		
		Scanner[] inputs = new Scanner[fnames.length];
		try
		{
			for (int i = 0; i < fnames.length; i++)
			{
				inputs[i] = new Scanner(new File(fnames[i]));
			}
		}
		catch (FileNotFoundException ex)
		{
			System.out.println("Could not open input file");
			System.out.println(ex);
			System.exit(1);
		}
		
		// read header of each file
		System.out.println(TextTools.noSpace(inputs[0].nextLine()));
		for (int i = 1; i < inputs.length; i++)
		{
			inputs[i].nextLine();
		}
		
		while (anyNextLine(inputs))
		{
		    Aggregate totalTrue = new Aggregate();
		    Aggregate totalFalse = new Aggregate();
		    String testObject = null;
		    int condition = -1;
		    int node = -1;
		    String source = null;
		    
		    for (int i = 0; i < inputs.length; i++)
			{
			    if (inputs[i].hasNextLine())
				{
				    String line = inputs[i].nextLine();
				    Scanner tok = new Scanner(line);
				    tok.useDelimiter(",");
				    
				    String o = tok.next().trim();
				    if (testObject == null)
					{
					    testObject = o;
					}
				    else if (!testObject.equals(o))
					{
					    System.err.println("Test Object does not match: " + testObject + ", " + o);
					    System.exit(1);
					}
				    
				    int c = Integer.parseInt(tok.next().trim());
				    if (condition == -1)
					{
					    condition = c;
					}
				    else if (condition != c)
					{
					    System.err.println("Condition does not match: " + condition + ", " + c);
					}
				    
				    int n = Integer.parseInt(tok.next().trim());
				    if (node == -1)
					{
					    node = n;
					}
				    else if (node != n)
					{
					    System.err.println("Node does not match: " + node + ", " + n);
					}
				    
				    int t = Integer.parseInt(tok.next().trim());
				    totalTrue.add(t);

				    String falseEvals = tok.next().trim();
				    if (falseEvals.equals("null"))
					{
					    // fix null from timeout
					    falseEvals = "-1";
					}
				    int f = Integer.parseInt(falseEvals);
				    totalFalse.add(f);
				    
				    String s = line.substring(line.indexOf('"'));
				    if (source == null)
					{
					    source = s;
					}
				    else if (!source.equals(s))
					{
					    System.err.println("Code does not match: " + source + ", " + s);
					    System.exit(1);
					}
				}
			}

		    if (showMean)
			{
			    System.out.println(testObject + "," + condition + "," + node + ","
					       + totalTrue.average() + ","
					       + totalFalse.average() + ","
					       // + totalTrue.averageOfValid() + ","
					       // + totalFalse.averageOfValid() + ","
					       + TextTools.escape(source));
			    
			    if (totalTrue.average() != totalTrue.averageOfValid())
				{
				    System.err.println("Mixed success/failure for " + testObject + " node " + node + " cond " + condition + " TRUE min " + totalTrue.getMin());
				}
			    
			    if (totalFalse.average() != totalFalse.averageOfValid())
				{
				    System.err.println("Mixed success/failure for " + testObject + " node " + node + " cond " + condition + " FALSE min " + totalFalse.getMin());
				
				}
			}
		    else if (showMedian)
			{
			    System.out.println(testObject + "," + condition + "," + node + ","
					       + totalTrue.median() + ","
					       + totalFalse.median() + ","
					       + TextTools.escape(source));
			}
		}
	}

    /**
     * Returns the quotient of non-negative first arguments and the second argument, or the
     * first argument if it is negative.
     *
     * @param sum an integer
     * @param num a positive integer
     * @return the quotient, or the sum if the sum is negative
     */
    private static int aggregateWithFlag(int sum, int num)
    {
	if (sum < 0)
	    {
		return sum;
	    }
	else
	    {
		return (int)((double)sum / num);
	    }
    }
    
    /**
     * Returns the updated counter for the given increment.  The summing operation treats -1
     * values as 0's, except keeps -1 and 0 distinct to distinguish between tests that succeeded
     * (resulted in a positive increment) and those that did not (resulted in a 0 increment indicated
     * by a -1).  So x + -1 = x for this operation.
     *
     * @param delta a non-negative integer, or -1
     * @param total a non-negative integer, or -1
     * @param node an integer identifying the source of the increment
     * @param file a string identifying the source of the increment
     * @param id a string identifying the accumulator
     * @return the updated value of the accumulator
     */
    public static int updateCounterWithFlag(int delta, int total, int n, String file, String id, Mode mode, int count)
    {
	
	if (delta == -1)
	    {
		if (total != -1 && total != 0)
		    {
			System.err.printf("%s failed where others succeeded for node %d %s avg=%f\n", file, n, id, (double)total / count);
		    }

		return (mode == Mode.ZERO ? total : -1);
	    }
	else
	    {
		if (total == -1)
		    {
			System.err.printf("%s succeeded where others failed for node %d %s datum=%d\n", file, n, id, delta);
			return (mode == Mode.ZERO ? delta : -1);
		    }
		else
		    {
			return total + delta;
		    }
	    }
    }
}
