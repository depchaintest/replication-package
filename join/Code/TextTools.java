import java.util.List;
import java.util.ArrayList;

public class TextTools
{
    public static String[] splitCSV(String in)
    {
	List<String> fields = new ArrayList<>();

	int fieldStart = 0;
	boolean inQuote = false;
	
	for (int i = 0; i < in.length(); i++)
	    {
		if (!inQuote && in.charAt(i) == ',')
		    {
			// output the last field
			fields.add(in.substring(fieldStart, i));

			// mark start of next field
			fieldStart = i + 1;
		    }
		else if (in.charAt(i) == '"' && i + 1 < in.length() && in.charAt(i + 1) != '"')
		    {
			inQuote = !inQuote;
		    }
	    }
	fields.add(in.substring(fieldStart));
	return fields.toArray(new String[fields.size()]);
    }

    /**
     * Returns the given string with 
     * double-quotes replaced with an escaped double-quote; if the given string contains a comman
     * then the return string is quoted even if the given string was not.
     *
     * @param in a string, not null
     * @return the input string with internal double-quotes escaped
     */
    public static String escape(String in)
    {
	StringBuilder result = new StringBuilder();

	result.append(in.charAt(0));
	boolean shouldQuote = false;
	for (int i = 1; i < in.length() - 1; i++)
	    {
		if (in.charAt(i) == '"')
		    {
			result.append('"');
		    }
		else if (in.charAt(i) == ',')
		    {
			shouldQuote = true;
		    }
		result.append(in.charAt(i));
	    }
	result.append(in.charAt(in.length() - 1));
	
	if (shouldQuote && result.charAt(0) != '"')
	    {
		return '"' + result.toString() + '"';
	    }
	else
	    {
		return result.toString();
	    }
    }

    public static String noSpace(String s)
    {
	StringBuffer buf = new StringBuffer();
	for (int i = 0; i < s.length(); i++)
	    {
		if (s.charAt(i) != ' ')
		    {
			buf.append(s.charAt(i));
		    }
	    }
	return buf.toString();
    }
}
