import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Scanner;
import java.util.Comparator;

/**
 * Filters path counting results to remove entries for which the
 * source code position interval contains the interval of another
 * entry.
 *
 * @version 0.1 2017-06-10 less cloudy now; should be making landfall soon and maybe then some wifi?
 *
 * TO DO:
 * do CSV parsing properly (strings that contain " or ,)
 * 
 * output vertex ids in hex
 * 
 * think about the proper thing to do for interval tree (when a new
 * interval contains several others; but see below)
 *
 * go back and see what the real condition to filter out entries is [currently
 * all in the script that drives this program; this makes the above irrelevant,
 * so in the end the matching is done manually and this program amounts
 * to grep-ing for the vertex type, edge type, and file]
 */
 
 public class FilterPathResults
 {
     // TO DO: should use an enum
     private static final int INNERMOST = 0;
     private static final int OUTERMOST = 1;
     private static final int ALL = 2;

     private static int keepOnOverlap = INNERMOST;

	 public static void main(String[] args)
	 {
		 if (args.length < 3)
		 {
			 System.err.println("USAGE: java FilterPathResults {CO|DO|BO} {ALLVX|ALLFI|FORML|ENTRY|BODY} {INNER|OUTER|ALL}");
			 System.exit(1);
		 }
		 
		 String edgeFilter = args[0];
		 String sourceFilter = args[1];
		 String intervalComparator = args[2];

		 String fileFilter = null;
		 if (args.length >= 4)
		     {
			 fileFilter = args[3];
			 if (!fileFilter.startsWith("\""))
			     {
				 fileFilter = '"' + args[3] + '"';
			     }
		     }
		 
		 Scanner input = new Scanner(System.in);

		 SortedMap<SourceInterval, PathData> data;
		 if (intervalComparator.equals("INNER"))
		     {
			 data = new TreeMap<>(SourceInterval.containmentComparator());
			 keepOnOverlap = INNERMOST;
		     }
		 else if (intervalComparator.equals("OUTER"))
		     {
			 data = new TreeMap<>(SourceInterval.containmentComparator());
			 keepOnOverlap = OUTERMOST;
		     }
		 else
		     {
			 data = new TreeMap<>();
			 keepOnOverlap = ALL;
		     }

		 int filtered = 0;
		 
		 while (input.hasNextLine())
		 {
		     String[] fields = TextTools.splitCSV(input.nextLine());
			 
		     String edgeType = fields[1];
		     int id = Integer.parseInt(fields[2], 16);
		     int start = Integer.parseInt(fields[3]);
		     int end = Integer.parseInt(fields[4]);
		     String file = fields[5];
		     String fxn = fields[6];
		     String sourceType = fields[7];
		     boolean estimated = fields[8].equals("Y");
		     int min = Integer.parseInt(fields[9]);
		     int max = Integer.parseInt(fields[10]);
		     double count = Double.parseDouble(fields[11]);
		     double mean = Double.parseDouble(fields[12]);
		     String code = fields[13];

		     // parse info for formal parameters if present;
		     // otherwise default to 0 and null		     
		     int formalID = 0;
		     String formalCode = null;
		     if (fields.length > 14)
			 {
			     formalID = Integer.parseInt(fields[14], 16);
			     formalCode = fields[15];
			 }
		     
		     if (edgeType.equals(edgeFilter) && sourceType.equals(sourceFilter) && (fileFilter == null || fileFilter.equals(file)))
			 {
				 SourceInterval i = new SourceInterval(start, end);
				 if (!data.containsKey(i))
				 {
				     // new interval -- keep it
				     
				     data.put(i, new PathData(edgeType, id, start, end, file, fxn, sourceType, estimated, min, max, count, mean, code, formalID, formalCode));
				 }
				 else if (i.contains(data.get(i).getInterval()))
				 {
				     // new interval contains other entries

				     if (keepOnOverlap == INNERMOST)
					 {
					     filtered++;
					     //System.err.println("skipping " + i + " in favor of " + data.get(i).getInterval());
					 }
				     else
					 {
					     // keep only the outermost entry
					     while (data.containsKey(i))
						 {
						     //System.err.println("removing " + data.get(i).getInterval() + " in favor of " + i);
						     data.remove(i);
						     filtered++;
						 }
					     data.put(i, new PathData(edgeType, id, start, end, file, fxn, sourceType, estimated, min, max, count, mean, code, formalID, formalCode));
					 }
				 }
				 else
				 {
					 // this entry is already contained by some other entry
				     if (keepOnOverlap == INNERMOST)
					 {
					     //System.err.println("removing " + data.get(i).getInterval() + " in favor of " + i);
					     data.remove(i);
					     filtered++;
					 }
				     else
					 {
					     filtered++;
					     //System.err.println("skipping " + i + " in favor of " + data.get(i).getInterval());
					 }
				 }
			 }
		 }

		 System.out.println("Header," + PathData.getHeader());
		 for (Map.Entry<SourceInterval, PathData> e : data.entrySet())
		 {
			 System.out.println("PATH-CSV," + e.getValue());
		 }
		 
		 //System.err.println("Filtered " + filtered + " entries");
	 }

 }
 
 class SourceInterval implements Comparable<SourceInterval>
 {
	private int start;
	private int end;
	
	public SourceInterval(int start, int end)
	{
		this.start = start;
		this.end = end;
	}
	
	public int getStart()
	{
		return start;
	}
	
	public int getEnd()
	{
		return end;
	}
	
	public int compareTo(SourceInterval i)
	{
	    if (start != i.start)
		{
		    return start - i.start;		
		}
	    else
		{
		    return i.end - end;
		}
	}

	public static Comparator<SourceInterval> containmentComparator()
	{
	    return new Comparator<SourceInterval>()
		{
		    public int compare(SourceInterval a, SourceInterval b)
		    {
			if (a.start > b.end)
			    {
				return 1;
			    }
			else if (a.end < b.start)
			    {
				return -1;
			    }
			else if (a.contains(b) || b.contains(a))
			    {
				return 0;
			    }
			else
			    {
				throw new IllegalArgumentException("intervals are incomparable: " + a + " " + b);
			    }
		    }
		};
	}
	
	public boolean contains(SourceInterval i)
	{
		return (start <= i.start && i.end <= end);
	}
	
	public String toString()
	{
		return "(" + start + "," + end + ")";
	}

 }
 
class PathData
{
	private String edgeType;
	private int id;
	private int start;
	private int end;
	private String file;
	private String fxn;
	private String sourceType;
	private boolean estimated;
	private int min;
	private int max;
	private double count;
	private double mean;
	private String code;
	private int formalID;
	private String formalCode;
	
	public PathData(String edgeType, int id, int start, int end, String file, String fxn, String sourceType, boolean estimated, int min, int max, double count, double mean, String code, int formalID, String formalCode)
	{
		this.edgeType = edgeType;
		this.id = id;
		this.start = start;
		this.end = end;
		this.file = file;
		this.fxn = fxn;
		this.sourceType = sourceType;
		this.estimated = estimated;
		this.min = min;
		this.max = max;
		this.count = count;
		this.mean = mean;
		this.code = code;
		this.formalID = formalID;
		this.formalCode = formalCode;
	}
	
	public SourceInterval getInterval()
	{
		return new SourceInterval(start, end);
	}

    public static String getHeader()
    {
	return "EdgeType,VertexID,Start,End,File,Function,VertexType,Estimate,Min,Max,Count,Mean,Source,FormalID,FormalCode";
    }
	
	public String toString()
	{
	    return edgeType + "," + String.format("%x", id) + "," + start + "," + end + "," + file + "," + fxn + "," + sourceType + "," + (estimated ? "Y" : "N") + "," + min + "," + max + "," + count + "," + mean + "," + TextTools.escape(code) + (formalCode != null ? "," + String.format("%x", formalID) + "," + formalCode : ",,");
	}


}
