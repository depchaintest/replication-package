#!/bin/bash
#for SIZE in Larger Smaller; do echo $SIZE; for MIDDLE in Mean Median; do echo $MIDDLE; for EDGE in BO CO DO; do for SEARCH in dcEvolve nhc random; do grep -v ",Mean," ${SIZE}/${MIDDLE}/*${SEARCH}*csv | grep $EDGE | grep -v "\-1.0" | grep -v ",1.0,1.0,1.0," | wc -l ; done; done; done; done

echo "=== NO FILTER"
for SIZE in Larger Smaller; do echo $SIZE; for MIDDLE in Mean Median; do echo $MIDDLE; for EDGE in BO CO DO; do for SEARCH in dcEvolve nhc random; do grep -v ",Mean," ${SIZE}/${MIDDLE}/*${SEARCH}*csv | grep $EDGE | wc -l ; done; done; done; done

echo "=== FILTERING OUT -1 ==="
for SIZE in Larger Smaller; do echo $SIZE; for MIDDLE in Mean Median; do echo $MIDDLE; for EDGE in BO CO DO; do for SEARCH in dcEvolve nhc random; do grep -v ",Mean," ${SIZE}/${MIDDLE}/*${SEARCH}*csv | grep $EDGE | grep -v "\-1.0" | wc -l ; done; done; done; done
