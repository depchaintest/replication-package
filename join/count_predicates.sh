#!/bin/bash

for EDGE in both data; do
    echo "===== EDGE TYPE: $EDGE ====="
    for INPUT in gamma gamma_cody rhyper toms708 unity g i0 k0 pnchisq pnorm ptukey spice1; do
	echo "=== $INPUT (paths, matched, paths and matched) ==="
	q -d',' -O -H "SELECT Function,count(*) FROM ${INPUT}_paths_filtered_${EDGE}.csv GROUP BY Function"
	q -d',' -O -H "SELECT MFunction,count(*) FROM ${INPUT}_match.csv GROUP BY Mfunction"
	q -d',' -O -H "SELECT Function,count(*) FROM ${INPUT}_paths_filtered_${EDGE}.csv WHERE Start || End IN (SELECT MStart || MEnd From ${INPUT}_match.csv) GROUP BY Function"
    done
done
